import { connect } from "react-redux";
import {
  cartAdd,
  cartRemove,
  cartUpdate,
  cartRemoveBySeller,
} from "../actions/cartActions";
import { IProduct } from "../reducers/productReducer";
import { IApplicationStore } from "./rootReducer";

export interface ICartProps {
  isFetchingCart: boolean;
  isUpdatingCart: boolean;
  errorCart?: any;
  itemsCart: {
    product: IProduct;
    price: number;
    count: number;
    discount: number;
    total: number;
    selectedForPurchase?: boolean;
  }[];
  discountCart: number;
  totalCart: number;
  totalSelectedForPurchase: number;
  cartAdd?(item: IProduct, count: number): void;
  cartUpdate?(item: IProduct, count: number): void;
  cartRemove?(item: IProduct, count: number): void;
  cartRemoveBySeller?(seller: string): void;
}

const mapStateToProps = (state: IApplicationStore): ICartProps => ({
  isFetchingCart: state.cartReducer.isFetching,
  isUpdatingCart: state.cartReducer.isUpdating,
  errorCart: state.cartReducer.error,
  itemsCart: state.cartReducer.items,
  discountCart: state.cartReducer.discount,
  totalCart: state.cartReducer.total,
  totalSelectedForPurchase: state.cartReducer.totalSelectedForPurchase,
});

const mapDispatchToProps = (
  dispatch: any
): Pick<
  ICartProps,
  "cartAdd" | "cartUpdate" | "cartRemove" | "cartRemoveBySeller"
> => ({
  cartAdd: (item: IProduct, count: number) => dispatch(cartAdd(item, count)),
  cartUpdate: (item: IProduct, count: number) =>
    dispatch(cartUpdate(item, count)),
  cartRemove: (item: IProduct, count: number) =>
    dispatch(cartRemove(item, count)),
  cartRemoveBySeller: (seller: string) => dispatch(cartRemoveBySeller(seller)),
});

export default connect(mapStateToProps, mapDispatchToProps);
