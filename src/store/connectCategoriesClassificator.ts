import { connect } from "react-redux";
import { IApplicationStore } from "./rootReducer";
import { ICategoryClassificator } from "../reducers/categoryReducer";

const mapStateToProps = (state: IApplicationStore) => ({
  categoriesClassificator: state.categoryReducer.flat || []
})

export default connect<{ categoriesClassificator: ICategoryClassificator[] }>(mapStateToProps)