import { Card } from '../types/commonapi';
import { loadPaymentCardsList } from '../actions/paymentCardsActions';
import { IApplicationStore } from './rootReducer';
import { connect } from 'react-redux';

export interface ICardsProps {
  loading?: boolean,
  error?: any,
  cards: Card[],
  hasCards?: boolean,
  loadPaymentCardsList(): () => void,
}

const mapStateToProps = (state: IApplicationStore) => ({
  ...state.paymentCardsReducer,
  hasCards: state.paymentCardsReducer.cards && state.paymentCardsReducer.cards.length > 0
})

const mapDispatchToProps = (dispatch: any) => ({
  loadPaymentCardsList: () => dispatch(loadPaymentCardsList())
})

export default connect(mapStateToProps, mapDispatchToProps)