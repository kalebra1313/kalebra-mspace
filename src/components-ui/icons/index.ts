import IcWip from "./ic-wip";
import IcUp from "./ic-up";
import IcMarker from "./ic-marker";
import IcProno from "./ic-prono";
import IcCrown from "./ic-crown";
import IcFramecard from "./ic-framecard";
import IcMastercard from "./ic-mastercard";
import IcVisa from "./ic-visa";
import IcTransaction from "./ic-transaction";
import IcYoutube from "./ic-youtube";
import IcVk from "./ic-vk";
import IcVimeo from "./ic-vimeo";
import IcArrowLeft from "./ic-arrow-left";
import IcArrowRight from "./ic-arrow-right";
import IcSent from "./ic-sent";
import IcStore from "./ic-store";
import IcReceived from "./ic-received";
import IcCart from "./ic-cart";
import IcDelivery from "./ic-delivery";
import IcOrders from "./ic-orders";
import IcSellerArea from "./ic-seller-area";
import IcSearch from "./ic-search";
import IcFilter from "./ic-filter";
import IcNothingFound from "./ic-nothing-found";
import IcMessages from "./ic-messages";
import IcEdit from "./ic-edit";
import IcLocation from './ic-location';
import IcGlobe from './ic-globe';
import IcRemove from './ic-remove';
import IcCopy from './ic-copy';
import IcMessagesSmall from './ic-messages-small';
import IcEditSmall from './ic-edit-small';
import IcRefundSmall from './ic-refund-small';
import IcArrowBack from './ic-arrow-back';
import IcDelete from './ic-delete';
import IcAddPhoto from './ic-add-photo';
import IcClose from './ic-close';
import IcBankAccount from './ic-bank-account';
import IcBankCard from './ic-bank-card';
import IcStripe from './ic-stripe';
import IcSupport from './ic-support';
import IcPassport from './ic-passport';
import IcMarketspaceApp from './ic-marketspace-app';
import IcMarketspace from './ic-marketspace';
import IcPassportLarge from './ic-passport-large';
import IcShare from './ic-share';
import IcShareLarge from './ic-share-large';

import "./style.less";

export {
  IcWip,
  IcUp,
  IcMarker,
  IcProno,
  IcCrown,
  IcFramecard,
  IcMastercard,
  IcVisa,
  IcTransaction,
  IcYoutube,
  IcVk,
  IcVimeo,
  IcArrowLeft,
  IcArrowRight,
  IcSent,
  IcStore,
  IcReceived,
  IcCart,
  IcDelivery,
  IcOrders,
  IcSellerArea,
  IcSearch,
  IcFilter,
  IcNothingFound,
  IcMessages,
  IcEdit,
  IcLocation,
  IcGlobe,
  IcRemove,
  IcCopy,
  IcMessagesSmall,
  IcEditSmall,
  IcRefundSmall,
  IcArrowBack,
  IcDelete,
  IcAddPhoto,
  IcClose,
  IcBankAccount,
  IcBankCard,
  IcStripe,
  IcSupport,
  IcPassport,
  IcMarketspaceApp,
  IcMarketspace,
  IcPassportLarge,
  IcShare,
  IcShareLarge
};
