import React from "react";

// @ts-ignore
export default ({ slot }: { slot?: string }) => (
  <svg width="32" height="32" viewBox="0 0 32 32" fill="none" xmlns="http://www.w3.org/2000/svg">
    <rect y="2" width="32" height="20" rx="4" fill="#CCCCCC"/>
    <path fillRule="evenodd" clipRule="evenodd"
          d="M0.125977 24C0.570019 25.7252 2.13612 27 3.99996 27H28C29.8638 27 31.4299 25.7252 31.8739 24H0.125977Z"
          fill="#CCCCCC"/>
    <path fillRule="evenodd" clipRule="evenodd"
          d="M0.125977 29C0.570019 30.7252 2.13612 32 3.99996 32H28C29.8638 32 31.4299 30.7252 31.8739 29H0.125977Z"
          fill="#CCCCCC"/>
    <circle cx="16" cy="12" r="5" fill="#949494"/>
    <circle cx="5" cy="12" r="2" fill="#949494"/>
    <circle cx="27" cy="12" r="2" fill="#949494"/>
  </svg>
);
