import HomePage from "./pages/home";
import AllFiltresPage from "./pages/all-filtres-popup";
import CategoriesPage from "./pages/categories";
import SubcategoriesPage from "./pages/subcategories";
import LocationPage from "./pages/location";
import ProductDetailsPage from "./pages/product-details";
import ProfilePage from "./pages/profile";
import WishListPage from "./pages/wish-list";
import SeeAllPage from "./pages/see-all";
import MyGoodsPage from "./pages/my-goods";
import {
  ProductCreatePage,
  SelectCategorySubcategorySheetPage,
} from "./pages/product-create";
import ProductEditPage from "./pages/product-edit";
import MyGoodsProductDetailsPage from "./pages/my-goods-product-details";
import SelectLocationSheet from "./pages/select-location-sheet";
import WalletPage from "./pages/wallet";
import CardsPage from "./pages/cards";

import AboutPage from "./pages/about";
import FormPage from "./pages/form";

import DynamicRoutePage from "./pages/dynamic-route";
import RequestAndLoad from "./pages/request-and-load";
import NotFoundPage from "./pages/404";
import Framework7 from "framework7";
import { Router } from "framework7/modules/router/router";
import { routes as currencyRoutes } from "./pages/currencies";
import { routes as transactionsRoutes } from "./pages/transactions";
import { routes as ordersRoutes } from "./pages/orders";
import PromoteProductPage from "./pages/product-promotion/promote-product";
import { routes as cartRoutes } from "./pages/cart";
import SellerAreaPage from "./pages/seller-area";
import { routes as payoutSettings } from "./pages/payout-settings";
import { routes as sellersOrders } from "./pages/sellers-orders";
import { routes as addBusinessAccount } from "./pages/add-business-account";

export interface RouteParameters extends Router.RouteParameters {
  app?: Framework7;
}

const routes: RouteParameters[] = [
  {
    name: "HomePage",
    path: "/",
    component: HomePage,
  },
  {
    name: "HomePage_Category",
    path: "/category/:catid/subcategory/:subcatid/",
    component: HomePage,
  },
  {
    path: "/all-filtres/",
    component: AllFiltresPage,
  },
  {
    path: "/all-filtres/categories/",
    component: CategoriesPage,
  },
  {
    path: "/all-filtres/categories/subcategories/:catid/",
    component: SubcategoriesPage,
  },
  {
    path: "/all-filtres/categories/subcategories/:catid/(.*)/",
    component: SubcategoriesPage,
  },
  {
    path: "/all-filtres/location/",
    component: LocationPage,
  },
  {
    path: "/product-details/:uid/",
    component: ProductDetailsPage,
  },
  {
    path: "/profile/",
    component: ProfilePage,
    routes: [
      {
        path: "/wallet/",
        component: WalletPage,
        routes: [
          {
            path: "/cards/",
            component: CardsPage,
          },
          ...currencyRoutes,
        ],
      },
      ...transactionsRoutes,
      ...ordersRoutes,
      {
        path: "/seller-area/",
        component: SellerAreaPage,
        routes: [
          {
            path: "/my-goods/",
            component: MyGoodsPage,
            routes: [
              {
                path: "/add/:step/",
                component: ProductCreatePage,
              },
              {
                path: "/edit/:uid/",
                component: ProductCreatePage,
              },
              {
                path: "/edit/:uid/:step/",
                component: ProductCreatePage,
              },
              {
                path: "/product-details/:uid/",
                component: MyGoodsProductDetailsPage,
              },
              {
                path: "/product-details/:uid/promote/",
                component: PromoteProductPage,
              },
            ],
          },
          ...payoutSettings,
          ...sellersOrders,
          ...addBusinessAccount
        ],
      },
    ],
  },
  {
    path: "/select-category-subcategory/",
    sheet: {
      component: SelectCategorySubcategorySheetPage,
    },
  },
  {
    path: "/wish-list/",
    component: WishListPage,
  },
  {
    path: "/see-all/:catid/",
    component: SeeAllPage,
  },
  {
    path: "/product-edit/",
    component: ProductEditPage,
  },
  {
    path: "/select-location-sheet/",
    component: SelectLocationSheet,
  },
  ...cartRoutes,
  {
    path: "/about/",
    component: AboutPage,
  },
  {
    path: "/form/",
    component: FormPage,
  },
  {
    path: "/dynamic-route/blog/:blogId/post/:postId/",
    component: DynamicRoutePage,
  },
  {
    path: "/request-and-load/user/:userId/",
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: "Vladimir",
          lastName: "Kharlampidi",
          about: "Hello, i am creator of Framework7! Hope you like it!",
          links: [
            {
              title: "Framework7 Website",
              url: "http://framework7.io",
            },
            {
              title: "Framework7 Forum",
              url: "http://forum.framework7.io",
            },
          ],
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            component: RequestAndLoad,
          },
          {
            context: {
              user: user,
            },
          }
        );
      }, 1000);
    },
  },
  {
    path: "(.*)",
    component: NotFoundPage,
  },
];

export default routes;
