import React from "react";
import {
  Page,
  Block,
  BlockTitle,
  Popover,
  List,
  ListItem,
  Sheet,
} from "framework7-react";
import { LatLng } from "react-google-places-autocomplete/build/GooglePlacesAutocomplete.types";
import { AboutPopup } from "../components/AboutPopup";
import { AgreementPopup } from "../components/AgreementPopup";
import { ContactSupportPopup } from "../components/ContactSupportPopup";
import {
  IApplicationStore,
  ILocalConfig,
  ResizeEvent,
} from "../store/rootReducer";
import { compose } from "redux";
import { connect } from "react-redux";
import { Profile } from "../reducers/sessionReducer";
import { getProfile } from "../selectors/profile";
import Slider, { SliderType, SliderItem } from "../components/Slider";
import CategoriesMenu from "../components/CategoriesMenu";
import CatalogBlockTitle from "../components/CatalogBlockTitle";
import { Catalog } from "../components/Catalog/index";
import {
  chooseCategory,
  chooseSubcategory,
  allFiltresClear,
  chooseSortBy,
  chooseCategorySubcategory,
  chooseLocation,
  chooseLocationPlace,
  clearSortBy,
} from "../actions/filterActions";
import { loadCategories } from "../actions/categoryActions";
import {
  searchClear,
  ISearchParams,
  searchProducts,
  loadProductListType,
  loadProductListCategory,
  addToWishList,
  loadProductWishList,
} from "../actions/productActions";
import {
  IProduct,
  IProductState,
  ProductListType,
} from "../reducers/productReducer";
import { withTranslation, WithTranslation } from "react-i18next";
import { ICategoryClassificator } from "../reducers/categoryReducer";
import connectCategoriesClassificator from "../store/connectCategoriesClassificator";
import connectAllGoods from "../store/connectAllGoods";
import { ProfileLink } from "../components/ProfileLink";
import connectLocalConfig from "../store/connectLocalConfig";
import connectChat, { IChatProps } from "../store/connectChat";
import connectAllDeals from "../store/connectAllDeals";
import { BigMarketingBannerSlider } from "../components/BigMarketingBannerSlider";
import SelectCategorySheet from "./select-category-sheet";
import { CategoriesMenuDesktop } from "../components/categories-menu-desktop";
import {
  goToMessenger,
  TC_AGREEMENT_SETTINGS_KEY,
} from "../actions/profileActions";
import { ProfilePopover } from "../components/profile-popover";
import { SortingButton } from "../components/sorting-button";
import { sortByTypes } from "../components/SortByButtonsGroup";
import { SortBy, IFilterState } from "../reducers/filterReducer";
import connectFilter from "../store/connectFilter";
import { getSortByFields } from "./all-filtres-popup";
import connectCart, { ICartProps } from "../store/connectCart";
import connectSearch, { ISearchConnectorProps } from "../store/connectSearch";
import Navbar from "../components/navbar";
import { analytics } from "../Setup";
import { StartChat } from "../components/StartChat";
import Breadcrumb from "../components/Breadcrumbs/Breadcrumb";
import { VerifyAccountPopup } from "../components/VerifyAccountPopup";

type Props = IChatProps &
  WithTranslation &
  ICartProps &
  IFilterState &
  ISearchConnectorProps & {
    profile?: Profile;
    loadCategories?(): void;
    chosenCategoryId?: string | null;
    chosenSubcategoryId?: string | null;
    chosenLocation?: string | null;
    chosenLocationPlace?: string | null;
    chooseCategory?(catid?: string | null): void;
    chooseSubcategory?(catid?: string): void;
    clearSortBy?(): void;
    productState?: IProductState;
    loadAllGoods?(): void;
    loadProducts?(type: ProductListType): void;
    loadProductListCategory?(type: ProductListType): void;
    categoriesClassificator?: ICategoryClassificator[];
    allGoodsProducts?: IProduct[];
    loadAllGoods?(searchParams: ISearchParams): void;
    allGoodsCount?: number;
    allGoodsOffset?: number;
    allGoodsLoading?: boolean;
    localConfig?: ILocalConfig;
    addToWish?(uid?: string): void;
    loadProductWishList?(): () => Promise<void>;
    resizeEvent?: ResizeEvent;
    goToMessenger?(): void;
    clearFilter?(): void;
    chooseSortBy?(sortBy: SortBy, refresh?: boolean): void;
    chooseCategorySubcategory?(catid?: string, subcatid?: string): void;
    chooseLocation?(location?: LatLng | null): void;
    chooseLocationPlace?(locationPlace?: string | null): void;
    loadAllDeals?(searchParams: ISearchParams): void;
    reloadAllDeals?(): void;
    allDealsCount?: number;
    allDealsOffset?: number;
    allDealsLoading?: boolean;
    allDealsProducts?: IProduct[];
  };

type State = {
  searchBarEnable?: boolean;
  selectCategorySheetOpened?: boolean;
  categoriesMenuOpened?: boolean;
  profilePopoverOpened?: boolean;
  sortingMenuPopoverOpened?: boolean;
  aboutPopupOpened?: boolean;
  contactSupportPopupOpened?: boolean;
  seeAll?: boolean;
  seeAllLoaded?: boolean;
  allDeals?: boolean;
  showBadges?: boolean;
  agreementPopupOpened?: boolean;
  selectedProductUid?: string;
  startChatSheetOpened?: boolean;
  verifyAccountPopupOpened?: boolean;
};

class HomePage extends React.Component<Props, State> {
  page: any;
  constructor(props: Readonly<Props>) {
    super(props);
    this.state = {
      searchBarEnable: false,
      selectCategorySheetOpened: false,
      seeAll: false,
      seeAllLoaded: false,
      allDeals: false,
      showBadges: false,
      agreementPopupOpened: false,
      startChatSheetOpened: false,
      selectedProductUid: null,
      verifyAccountPopupOpened: false,
    };
  }

  async componentDidMount() {
    /* TODO */
    const { profile } = this.props;

    if (profile && profile.uid) {
      analytics.start(this.props.profile);

      this.loadMainSlider();
      this.loadSliders();
      this.loadAllGoods();
      this.checkUserAgreement();
    }
  }

  componentDidUpdate(prevProps: Props) {
    this.handleAllLoading(prevProps);
    this.handleInfiniteLoading(prevProps);
    this.handleChatErrors(prevProps);
    this.handleSorting(prevProps);
    this.handleCategoryMenu(prevProps);

    if (
      prevProps.chosenCategoryId !== this.props.chosenCategoryId &&
      prevProps.chosenSubcategoryId !== this.props.chosenSubcategoryId &&
      !this.state.selectCategorySheetOpened
    ) {
      this.setState({ showBadges: !this.state.showBadges });
    }

    if(this.$f7router.url.indexOf('/profile/')!==-1){
      this.$f7router.navbarsEl && this.$f7router.navbarsEl.classList.add('hide-nav-main');
    }else{
      this.$f7router.navbarsEl && this.$f7router.navbarsEl.classList.remove('hide-nav-main');
    }
  }

  loadMainSlider = () => {
    const { chosenCategoryId, chosenSubcategoryId } = this.props;
    if (!chosenCategoryId && !chosenSubcategoryId) {
      this.props.loadProducts("popular");
    }
  };

  loadSliders = () => {
    const { chosenCategoryId, chosenSubcategoryId } = this.props;
    if (chosenCategoryId || chosenSubcategoryId) {
      return;
    }

    if (this.$f7router.currentRoute.params) {
      const { catid, subcatid } = this.$f7router.currentRoute.params;
      if (catid || subcatid) {
        return;
      }
    }

    const sliderCategories = ["009", "011"];
    sliderCategories.forEach((category: string) =>
      this.props.loadProductListCategory(category)
    );
  };

  clearSearch = () => {
    const { resizeEvent } = this.props;
    const isMobile = resizeEvent.width < 567;
    if (isMobile) {
      this.setState({ seeAll: false, seeAllLoaded: false, allDeals: false });
    }
    this.props.clearSearch(!isMobile);
    this.props.clearSortBy();
  };

  handlePageInit = () => {

    if (this.$f7router.currentRoute.params && this.props.profile) {
      this.clearSearch();
      const { catid, subcatid } = this.$f7router.currentRoute.params;
      if (catid && subcatid) {
        this.props.search({
          category: subcatid,
          count: 10,
          offset: 0,
          resetSearch: true,
          resetSorting: true,
          name: null,
        });
      } else if (catid) {
        /*
        this.setState({ seeAll: true }, () => {
          this.props.chooseCategory(catid);
          this.props.search({
            category: catid,
            count: 15,
            offset: 0,
            resetSearch: true,
            resetSorting: true,
          });
        });
        */
      }
    }

    this.setState({
      searchBarEnable: false,
      selectCategorySheetOpened: false,
      categoriesMenuOpened: false,
      profilePopoverOpened: false,
      sortingMenuPopoverOpened: false,
      aboutPopupOpened: false,
      contactSupportPopupOpened: false,
      seeAll: false,
      seeAllLoaded: false,
      allDeals: false,
    });
  };

  async handleAllLoading(prevProps: Props) {
    /* TODO */
    const { profile } = this.props;
    if (profile && profile.uid && profile.uid !== prevProps.profile.uid) {
      this.clearSearch();
      this.props.loadProductWishList();
      this.loadMainSlider();
      this.loadAllGoods();

      /* TODO Maybe, we will add on profile loaded event?! */
      if (this.$f7router.currentRoute.params) {
        const { catid, subcatid } = this.$f7router.currentRoute.params;
        if (catid && subcatid) {
          this.props.chooseCategory(catid);
          this.props.chooseSubcategory(subcatid);
          this.props.search({
            category: subcatid,
            count: 10,
            offset: 0,
            resetSearch: true,
            resetSorting: true,
          });
        } else if (catid) {
          this.setState({ seeAll: true }, () => {
            this.props.chooseCategory(catid);
            this.props.search({
              category: catid,
              count: 15,
              offset: 0,
              resetSearch: true,
              resetSorting: true,
            });
          });
        }
      }

      //this.checkUserAgreement();
    }
  }

  handleInfiniteLoading = (prevProps: Props) => {
    if (
      prevProps.productState.loading &&
      prevProps.productState.loading !== this.props.productState.loading
    ) {
      this._lock = false;
    }
    if (
      prevProps.allGoodsLoading &&
      prevProps.allGoodsLoading !== this.props.allGoodsLoading
    ) {
      this._lock = false;
    }
    if (
      prevProps.allDealsLoading &&
      prevProps.allDealsLoading !== this.props.allDealsLoading
    ) {
      this._lock = false;
    }
  };

  handleSelectCategorySheet(prevProps: Props) {
    const { chosenCategoryId, chosenSubcategoryId } = this.props;
    const { selectCategorySheetOpened, seeAll, seeAllLoaded } = this.state;
    const isSelectedAnotherCategory =
      prevProps.chosenCategoryId !== chosenCategoryId;
    const isSelectedOnlyCategory =
      chosenCategoryId &&
      isSelectedAnotherCategory &&
      prevProps.chosenCategoryId !== chosenCategoryId &&
      !this.props.chosenSubcategoryId;

    if (isSelectedOnlyCategory && !selectCategorySheetOpened && !seeAll) {
      this.toggleSelectCategorySheet(true);
    } else if (
      chosenCategoryId &&
      chosenSubcategoryId &&
      selectCategorySheetOpened &&
      !seeAll &&
      !this.props.productState.loading
    ) {
      /* TODO */
      this.toggleSelectCategorySheet(false);
    } else if (
      chosenCategoryId &&
      seeAll &&
      !seeAllLoaded &&
      !this.props.productState.loading
    ) {
      this.clearSearch();
      this.setState({ seeAllLoaded: true }, () => {});
    }
  }

  handleCategoryMenu = (prevProps: Props) => {
    const { chosenCategoryId, chosenSubcategoryId } = this.props;
    const { categoriesMenuOpened } = this.state;

    if (
      chosenCategoryId &&
      chosenSubcategoryId &&
      (prevProps.chosenCategoryId !== chosenCategoryId ||
        prevProps.chosenSubcategoryId !== chosenSubcategoryId) &&
      categoriesMenuOpened
    ) {
      this.setState({ categoriesMenuOpened: false }, () => {
        this.clearSearch();
      });
    }
  };

  handleSorting = (prevProps: Props) => {
    const { allDeals } = this.state;
    const { sortBy } = this.props;
    if (!allDeals && prevProps.sortBy !== sortBy) {
      const { chosenCategoryId, chosenSubcategoryId } = this.props;
      const { searchTerm } = this.props.productState;
      const params: ISearchParams = {
        category: chosenSubcategoryId || chosenCategoryId,
        name: searchTerm,
        count: 10,
        offset: 0,
        resetSorting: true,
      };

      if (sortBy.length) {
        params.sortBy = getSortByFields(sortBy);
      }

      this.props.search(params);
    }
  };

  toggleSelectCategorySheet(show = true) {
    this.setState({ selectCategorySheetOpened: show });
  }

  handleChatErrors = (prevProps: Props) => {
    if (this.$f7route.url === this.$f7router.currentRoute.url) {
      const { error } = this.props.chatReducer;
      if (error && error !== prevProps.chatReducer.error) {
        this.$f7?.dialog.alert(error);
      }
    }
  };

  handleOnSelectCategorySheetClosed = () => {
    this.toggleSelectCategorySheet(false);
    requestAnimationFrame(() => {
      this.clearFilter();

      if (this.props.chosenCategoryId && this.props.chosenSubcategoryId) {
        this.setState({ showBadges: true });
      }
    });
  };

  clearFilter = () => {
    const { chosenSubcategoryId } = this.props;

    if (!chosenSubcategoryId) {
      this.props.chooseCategory(null);
    }
  };

  seeAllHandle(catid: string) {
    if (catid) {
      this.setState({ seeAll: true, showBadges: true }, () => {
        this.props.chooseCategory(catid);
        this.props.search({});
      });
    }
  }

  categoryClickHandle = (catid: string) => {
    if (catid === "all_filtres") {
      this.$f7router.navigate("/all-filtres/");
      return;
    }

    this.props.clearFilter();
    this.props.chooseCategory(catid);

    this.setState({
      selectCategorySheetOpened: !this.state.selectCategorySheetOpened,
    });
  };

  clearFilterHandle = () => {
    this.props.chooseCategory(null);
    this.props.chooseLocation(null);
    this.props.chooseLocationPlace(null);
    this.clearSearch();
    this.setState({ showBadges: false });
  };

  searchbarEnableHandle = () => {
    this.setState({ searchBarEnable: true });
  };

  searchbarDisableHandle = () => {
    const { chosenCategoryId, chosenSubcategoryId } = this.props;
    this.setState({ searchBarEnable: false }, () => {
      this._lock = false;
    });
    this.clearSearch();
    if (chosenCategoryId || chosenSubcategoryId) {
      this.props.search({});
    }
  };

  checkUserAgreement = () => {
    const { profile } = this.props;
    if (profile && profile.uid && !this.state.agreementPopupOpened) {
      const termsSettings = (profile.accountSettings || []).filter(s => {
        return s.name === TC_AGREEMENT_SETTINGS_KEY;
      });
      if (termsSettings.length === 0 || termsSettings[0]['value'] !== 'true') {
        this.setState({ agreementPopupOpened: true });
        analytics.register(this.props.profile);
      }
    }
  };


  _lock: boolean = false;

  loadMore = () => {
    const { searchBarEnable } = this.state;
    const { searchLoading } = this.props;
    const { products, totalCount } = this.props.productState;
    const isFilterEnable = this.isFilterEnable();

    if (this._lock) return;
    this._lock = true;

    if (
      (searchBarEnable || isFilterEnable) &&
      !searchLoading &&
      products.length !== totalCount
    ) {
      this.props.search({
        offset: this.props.productState.offset,
        count: this.props.productState.count,
      });
    }

    if (!searchBarEnable && !isFilterEnable && !this.props.allGoodsLoading) {
      this.loadAllGoods();
    }
  };

  searchbarClearHandle = () => {
    this.clearSearch();
  };

  isFilterEnable = () =>
    (this.state.searchBarEnable && this.props.resizeEvent.width < 567) ||
    this.props.chosenCategoryId ||
    this.props.chosenSubcategoryId ||
    this.props.chosenLocation;

  sliderItemClickHandle = (item: SliderItem) => {
    this.$f7router.navigate(item.href);
  };

  getSlidesPopularProducts = (): SliderItem[] => {
    const {
      productState: { productTypeGroups },
    } = this.props;

    if (productTypeGroups) {
      const popularType = productTypeGroups.filter(
        (item) => item.type === "popular"
      )[0];

      if (popularType) {
        return popularType.products
          ? popularType.products.map((item) => {
              return {
                ...item,
                image: item.imageUrl1,
                priceDiscount: item.discountedPrice,
                href: `/product-details/${item.uid}/`,
                onClick: this.sliderItemClickHandle,
                description: item.shortDescription,
              };
            })
          : [];
      }
    }

    return [];
  };

  getSlidesItems = (catid?: string): SliderItem[] => {
    const {
      productState: { productTypeGroups },
    } = this.props;
    if (productTypeGroups) {
      const popularType = productTypeGroups.filter(
        (item) => item.type === catid
      )[0];
      if (popularType) {
        return popularType.products
          ? popularType.products.map((item) => {
              return {
                ...item,
                image: item.imageUrl1,
                priceDiscount: item.discountedPrice,
                href: `/product-details/${item.uid}/`,
                onClick: this.sliderItemClickHandle,
                description: item.shortDescription,
              };
            })
          : [];
      }
    }

    return [];
  };

  loadAllGoods = () => {
    const { allGoodsLoading, allGoodsCount, allGoodsOffset } = this.props;
    if (!allGoodsCount) return;

    if (!allGoodsLoading) {
      const params: ISearchParams = {
        count: allGoodsCount,
        offset: allGoodsOffset,
      };
      this.props.loadAllGoods(params);
    }
  };

  startChatHandle = (uid?: string, message?: string) => {
    if (!uid) {
      if (!this.state.selectedProductUid) return;
      uid = this.state.selectedProductUid;
    }
    const products = this.props.productState.productTypeGroups.filter(
      (item) => item.type === "popular"
    )[0];
    const item = products.products.filter((item) => item.uid == uid)[0];

    const { loading } = this.props.chatReducer;
    if (loading) return;

    this.props.startChatWithProduct(item, message);
    analytics.startChatWithSeller(this.props.profile, item);

    this.setState({
      startChatSheetOpened: false,
      selectedProductUid: null,
    });
  };

  renderCategoryTitleInSearchResultList() {
    const selectedCategory = this.props.categoriesClassificator.length
      ? this.props.categoriesClassificator.filter(
          (item) =>
            item.categoryCode ===
            (this.props.chosenSubcategoryId || this.props.chosenCategoryId)
        )[0]
      : null;

    return selectedCategory ? (
      <div className="catalog-block-title block-title block-title-medium pure-hidden-xs">
        {selectedCategory.categoryName}
        {this.renderSortingMenu()}
      </div>
    ) : null;
  }

  renderSortingMenu() {
    const { t } = this.props;

    const sortTypes = sortByTypes.reduce((prev, cur) => {
      prev.push(...cur);
      return prev;
    }, []);
    const sortBy = this.props.sortBy[0] || SortBy.sales_first;
    const selectedSortType = sortTypes.filter(
      (item) => item.type === sortBy
    )[0];

    return (
      <SortingButton
        text={t(selectedSortType.text)}
        onClick={() => {
          /* TODO */
          const inst = this.$f7.popover.get(".sorting-menu");
          if (inst) {
            this.$f7.popover.open(inst.el, ".sorting-button");
            this.setState({
              sortingMenuPopoverOpened: !this.state.sortingMenuPopoverOpened,
            });
          }
        }}
        opened={this.state.sortingMenuPopoverOpened}
      />
    );
  }

  renderProfileLink = () => {
    const { profile, resizeEvent } = this.props;
    const props = { profile };
    if (resizeEvent && resizeEvent.width > 567) {
      props["href"] = "#";
      props["onClick"] = this.handleClickProfileLink;
    }
    return <ProfileLink key="profile_link" {...props} />;
  };

  handleClickProfileLink = () => {
    this.setState({ profilePopoverOpened: true });
  };

  renderSortingMenuPopover() {
    const { t } = this.props;

    return (
      <Popover
        className="sorting-menu"
        opened={this.state.sortingMenuPopoverOpened}
        onPopoverClosed={() =>
          this.setState({ sortingMenuPopoverOpened: false })
        }
        backdrop={false}
        slot="fixed"
      >
        <List noHairlines noChevron noHairlinesBetween>
          {sortByTypes
            .reduce((prev, cur) => {
              prev.push(...cur);
              return prev;
            }, [])
            .map((item, i) => (
              <ListItem
                key={i.toString()}
                link="#"
                popoverClose
                title={t(item.text).toString()}
                onClick={() => {
                  this.setState(
                    {
                      sortingMenuPopoverOpened: false,
                    },
                    () => this.props.chooseSortBy(item.type, true)
                  );
                }}
              />
            ))}
        </List>
      </Popover>
    );
  }

  onClickProfileLink = () => {
    const { profile, resizeEvent } = this.props;
    if (resizeEvent && resizeEvent.width > 567 && profile && profile.uid) {
      this.handleClickProfileLink();
    } else if (resizeEvent && resizeEvent.width > 567) {
      this.handleClickLoginLink();
    }
  };

  handleClickLoginLink = () => {};

  onClickLogoLink = () => {
    const { searchbar } = this.$f7;
    searchbar.disable(".search-bar");
    this.props.chooseCategory(null);
    this.clearSearch();
    this.setState({
      categoriesMenuOpened: false,
      searchBarEnable: false,
    });
    this.$f7router.navigate("/", {
      reloadAll: true,
    });
  };

  onClickOpenCategoriesMenu = () =>
    this.setState({
      categoriesMenuOpened: !this.state.categoriesMenuOpened,
    });


  render() {
    const {
      searchedProducts,
      searchLoading,
      allGoodsLoading,
      profile,
      resizeEvent,
      t,
    } = this.props;
    let isFilterEnable = this.isFilterEnable();
    const { searchBarEnable, selectCategorySheetOpened, allDeals } = this.state;
    const showOnSearch = !isFilterEnable; /* || !this.props.resizeEvent?.isXS */

    return (
      <Page
        id="home_page"
        name="home-page"
        infinite
        infiniteDistance={300}
        infinitePreloader={searchLoading || allGoodsLoading}
        onInfinite={this.loadMore}
        onPageInit={this.handlePageInit}
        subnavbar={resizeEvent.width<769}
      >
        <Navbar
          profile={profile}
          showProfileLink
          onClickProfileLink={this.onClickProfileLink}
          onClickLogoLink={this.onClickLogoLink}
          onClickOpenCategoriesMenu={this.onClickOpenCategoriesMenu}
          openCategoriesMenuButton={this.state.categoriesMenuOpened}
          showLanguageLink={false}
          onSearchbarEnable={this.searchbarEnableHandle}
          onSearchbarDisable={this.searchbarDisableHandle}
          onSearchClickClear={this.searchbarClearHandle}
          showSearchbar
          findedProducts={this.props.searchProductsAutocomplete}
          findProductLoading={this.props.searchLoadingAutocomplete}
          onFindedProductItemClick={(uid) =>
            this.$f7router.navigate(`/product-details/${uid}/`)
          }
          showMessengerButton
          onClickGoToMessenger={this.props.goToMessenger}
          slot="fixed"
        />

        {!searchBarEnable && (
          <CategoriesMenu
            className="pure-visible-xs"
            categoryOnClick={this.categoryClickHandle}
            clearFilterOnClick={this.clearFilterHandle}
            showBadges={this.state.showBadges}
          />
        )}

        {showOnSearch && !allDeals && (
          <>
            <Block className="no-padding">
              <BigMarketingBannerSlider />
            </Block>

            <BlockTitle medium>{t("Most popular")}</BlockTitle>
            <Slider
              slides={this.getSlidesPopularProducts()}
              type={SliderType.big}
              startChat={(uid) => {
                this.setState({
                  selectedProductUid: uid,
                }, () => {
                  this.setState({
                    startChatSheetOpened: true,
                  })
                });
              }}
              showIfEmpty
            />

            <CatalogBlockTitle medium onClick={() => this.seeAllHandle("009")}>
              {this.props.categoriesClassificator.length
                ? this.props.categoriesClassificator.filter(
                    (item) => item.categoryCode === "009"
                  )[0].categoryName
                : null}
            </CatalogBlockTitle>
            <Slider
              slides={this.getSlidesItems("009")}
              type={SliderType.small}
              showIfEmpty
              showFeaturesHiglight
            />

            <CatalogBlockTitle medium onClick={() => this.seeAllHandle("011")}>
              {this.props.categoriesClassificator.length
                ? this.props.categoriesClassificator.filter(
                    (item) => item.categoryCode === "011"
                  )[0].categoryName
                : null}
            </CatalogBlockTitle>
            <Slider
              slides={this.getSlidesItems("011")}
              type={SliderType.small}
              showIfEmpty
              showFeaturesHiglight
            />

            <CatalogBlockTitle medium>{t("All goods")}</CatalogBlockTitle>
            <Catalog
              items={this.props.allGoodsProducts}
              addToWish={this.props.addToWish}
              showFeaturesHiglight
            />
          </>
        )}

        {isFilterEnable && !showOnSearch && !allDeals && (
          <>
            <Breadcrumb 
              categoryCode={this.props.chosenSubcategoryId || this.props.chosenCategoryId} 
              handleBackToMain={this.onClickLogoLink}
              hideLast={true}
            />
            {this.renderCategoryTitleInSearchResultList()}
            <Catalog
              items={searchedProducts}
              addToWish={this.props.addToWish}
              showFeaturesHiglight
            />
          </>
        )}

        <div slot="fixed">
          <CategoriesMenuDesktop
            className="pure-hidden-xs"
            opened={this.state.categoriesMenuOpened}
          />
        </div>

        <SelectCategorySheet
          opened={selectCategorySheetOpened}
          onSheetClosed={this.handleOnSelectCategorySheetClosed}
          onChooseSubcategoryClick={() => this.props.search({})}
          slot="fixed"
        />

        <ProfilePopover
          profile={profile}
          backdrop={false}
          opened={this.state.profilePopoverOpened}
          target=".profile-link"
          onPopoverClosed={() => this.setState({ profilePopoverOpened: false })}
          onAboutClick={() => this.setState({ aboutPopupOpened: true })}
          onVerifyClick={() => this.setState({ verifyAccountPopupOpened: true })}
          t={t}
          slot="fixed"
        />

        <AboutPopup
          profile={profile}
          backdrop={true}
          opened={this.state.aboutPopupOpened}
          onPopupClosed={() => this.setState({ aboutPopupOpened: false })}
          onContactSupportClick={() =>
            this.setState({ contactSupportPopupOpened: true })
          }
        />

        <ContactSupportPopup
          profile={profile}
          category="Application"
          backdrop={true}
          opened={this.state.contactSupportPopupOpened}
          onPopupClosed={() =>
            this.setState({ contactSupportPopupOpened: false })
          }
        />

        <AgreementPopup
          profile={profile}
          backdrop={true}
          opened={this.state.agreementPopupOpened}
          onPopupClosed={() =>
            this.setState({ agreementPopupOpened: false })
          }
        />

        {this.renderSortingMenuPopover()}

        <Sheet
          id="start_chat_sheet"
          swipeToClose
          backdrop
          opened={this.state.startChatSheetOpened}
          slot="fixed"
          style={{ height: "auto" }}
        >
          <StartChat
            opened={this.state.startChatSheetOpened}
            productUid={this.state.selectedProductUid}
            onClose={() => {
              this.setState({
                startChatSheetOpened: false,
              });
            }}
            onStartChat={(message) => this.startChatHandle(null, message)}
          />
        </Sheet>

        <VerifyAccountPopup
          t={t}
          backdrop={true}
          opened={this.state.verifyAccountPopupOpened}
          onPopupClosed={() =>
            this.setState({ verifyAccountPopupOpened: false })
          }
        />
      </Page>
    );
  }
}

const mapStateToProps = (state: IApplicationStore) => ({
  chosenCategoryId: state.filterReducer.chosenCategoryId,
  chosenSubcategoryId: state.filterReducer.chosenSubcategoryId,
  chosenLocation: state.filterReducer.location,
  chosenLocationPlace: state.filterReducer.locationPlace,
  profile: getProfile(state),
  searchLoading: state.productReducer.loading,
  searchedProducts: state.productReducer.products || [],
  productState: state.productReducer,
  resizeEvent: state.rootReducer.resizeEvent,
  sortBy: state.filterReducer.sortBy,
});

const mapDispatchToProps = (dispatch: any) => ({
  loadCategories: () => dispatch(loadCategories()),
  chooseCategory: (catid?: string) => dispatch(chooseCategory(catid)),
  chooseSubcategory: (catid?: string) => dispatch(chooseSubcategory(catid)),
  clearSearch: () => dispatch(searchClear()),
  clearSortBy: () => dispatch(clearSortBy()),
  search: (searchParams: ISearchParams) =>
    dispatch(searchProducts(searchParams)),
  loadProducts: (type: ProductListType) => dispatch(loadProductListType(type)),
  loadProductListCategory: (type: ProductListType) =>
    dispatch(loadProductListCategory(type)),
  addToWish: (uid?: string) => dispatch(addToWishList(uid)),
  loadProductWishList: () => dispatch(loadProductWishList()),
  goToMessenger: () => dispatch(goToMessenger()),
  clearFilter: () => dispatch(allFiltresClear()),
  chooseSortBy: (sortBy: SortBy, refresh?: boolean) =>
    dispatch(chooseSortBy(sortBy, refresh)),
  chooseCategorySubcategory: (catid?: string, subcatid?: string) =>
    dispatch(chooseCategorySubcategory(catid, subcatid)),
  chooseLocation: (location?: LatLng) => dispatch(chooseLocation(location)),
  chooseLocationPlace: (locationPlace?: string) =>
    dispatch(chooseLocationPlace(locationPlace)),
});

export default compose(
  connectLocalConfig,
  connectCategoriesClassificator,
  connectAllGoods,
  connectChat,
  connectFilter,
  connectCart,
  connectAllDeals,
  connect(mapStateToProps, mapDispatchToProps),
  connectSearch,
  withTranslation()
)(HomePage);
