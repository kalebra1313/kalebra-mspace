import React from "react";
import {
  Sheet,
  PageContent,
  BlockTitle,
  F7Sheet,
  List,
  ListItem,
  Popup,
  F7Popup,
  Navbar,
  NavTitle,
} from "framework7-react";
import { WithTranslation, withTranslation } from "react-i18next";
import { bindActionCreators, compose } from "redux";
import { IcLocation, IcGlobe } from "../../components-ui/icons";
import { Address } from "../../types/commonapi";
import { IApplicationStore } from "../../store/rootReducer";
import { connect } from "react-redux";
import { getProfile } from "../../selectors/profile";
import {
  toggleSelectCustomerLocationSheet,
  changeCountry,
  toggleSelectCountryPopup,
  changeAddress,
} from "../../actions/customer-location/customerLocationActions";
import "./style.less";

function getAddressLine(address: Address) {
  return `${address.city}, ${address.firstAddressLine}, ${address.postalCode}`;
}

type Props = Partial<ReturnType<typeof mapStateToProps>> &
  Partial<ReturnType<typeof mapDispatchToProps>> &
  F7Sheet.Props &
  F7Popup.Props & {
    addressList?: Address[];
  };

const SelectCustomerLocationSheet = ({
  addressList,
  selectCustomerLocationSheetOpened,
  onSheetClosed,
  onPopupClosed,
  toggleSelectCustomerLocationSheet,
  toggleSelectCountryPopup,
  changeCountry,
  changeAddress,
  addressUid,
  t,
  resizeEvent,
  ...rest
}: Props & WithTranslation) => {
  const Component: any = resizeEvent.isXS ? Sheet : Popup;
  const dialogClosed = (instance) => {
    toggleSelectCustomerLocationSheet(false);
    if (resizeEvent.isXS && onSheetClosed) {
      onSheetClosed(instance);
    } else if (onPopupClosed) {
      onPopupClosed(instance);
    }
  };
  const props: F7Sheet.Props | F7Popup.Props = resizeEvent.isXS
    ? {
        onSheetClosed: dialogClosed,
      }
    : { onPopupClosed: dialogClosed };

  return (
    <Component
      id="select_customer_location__sheet"
      backdrop
      opened={selectCustomerLocationSheetOpened}
      {...rest}
      {...props}
    >
      <Navbar noHairline noShadow>
        <NavTitle>{t("Delivery location")}</NavTitle>
      </Navbar>
      <PageContent>
        {resizeEvent.isXS && (
          <BlockTitle medium>{t("Delivery location")}</BlockTitle>
        )}
        <List noHairlines>
          <ListItem
            link
            title={t("Select a country").toString()}
            onClick={() => {
              toggleSelectCustomerLocationSheet(false);
              setTimeout(() => toggleSelectCountryPopup(true), 380);
            }}
          >
            {/* @ts-ignore */}
            <IcGlobe slot="media" />
          </ListItem>
          {/*<ListItem link title={t("Add address").toString()} onClick={() => {}}>
          <IcLocation slot="media" />
        </ListItem>*/}
        </List>
        {addressList && addressList.length && (
          <>
            <BlockTitle medium>{t("Saved addressess")}</BlockTitle>
            <List
              mediaList
              noHairlines
              noHairlinesBetween
              className="no-margin-top no-margin-bottom saved-addressess"
            >
              {addressList.map((item, i) => (
                <ListItem
                  key={item.uid}
                  radio
                  checked={addressUid === item.uid}
                  onClick={() => {
                    changeCountry(item.country.code); // TODO: ?
                    changeAddress(item.uid);
                    toggleSelectCustomerLocationSheet(false);
                  }}
                >
                  <div slot="title">
                    <strong>{item.country.name}</strong>
                    <div className="address-line">{getAddressLine(item)}</div>
                  </div>
                </ListItem>
              ))}
            </List>
          </>
        )}
      </PageContent>
    </Component>
  );
};

const mapStateToProps = (state: IApplicationStore) => ({
  addressList: getProfile({ ...state }).addresses,
  resizeEvent: state.rootReducer.resizeEvent,
  ...state.customerLocationReducer,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      toggleSelectCustomerLocationSheet,
      toggleSelectCountryPopup,
      changeCountry,
      changeAddress,
    },
    dispatch
  );

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps)
)(SelectCustomerLocationSheet) as React.ComponentClass<Props>;
