import React, { Component } from "react";
import {
  PageContent,
  BlockTitle,
  F7Sheet,
  List,
  ListItem,
  Block,
  Icon,
  Stepper,
} from "framework7-react";
import { IApplicationStore } from "../store/rootReducer";
import { connect } from "react-redux";
import { compose } from "redux";
import { WithTranslation, withTranslation } from "react-i18next";
import { Sheet as SheetNamespace } from "framework7/components/sheet/sheet";
import { Sheet } from "../components/Sheet";
import { Button } from "../components/ThemedButton";
import { formatPrice } from "../utils";
import { IProduct } from "../reducers/productReducer";
import { ListInput } from "../components/ListInput";
import "./product-details.less";

type Props = F7Sheet.Props & {
  item: IProduct;
  onAddToCartClick?(count: number, productParams: any[]): void;
};

type State = {
  sheetOpen?: boolean;
  addToCartSheetItemCount?: number;
  formErrors?: any;
  formValidFields?: any;
  formValid: boolean;
};

class AddToCartSheetPage extends Component<Props & WithTranslation, State> {
  _sheet: SheetNamespace.Sheet = null;

  constructor(props: Readonly<Props & WithTranslation>) {
    super(props);
    this.state = {
      addToCartSheetItemCount: 1,
      formValidFields: {},
      formErrors: {},
      formValid: false,
    };
  }

  renderStepperButtons() {
    return (
      this.state.sheetOpen && (
        <Stepper
          className="add-product-stepper"
          onStepperChange={(val: any) => {
            this.setState({ addToCartSheetItemCount: val });
          }}
          min={1}
          value={this.state.addToCartSheetItemCount}
        />
      )
    );
  }

  renderItemTotal = () => {
    const {
      item: { price, discountedPrice, currencyCode },
    } = this.props;
    const total = (discountedPrice || price) * this.state.addToCartSheetItemCount;
    return formatPrice(total, currencyCode);
  };

  handleAddToCartClick = () => {
    const {
      onAddToCartClick,
      item: { productParams },
    } = this.props;
    const { addToCartSheetItemCount, formValid } = this.state;

    if (
      productParams &&
      productParams.length &&
      productParams.filter((p) => p.mandatory).length &&
      !formValid
    ) {
      return;
    }

    if (onAddToCartClick) {
      onAddToCartClick(addToCartSheetItemCount, []);
    }
  };

  getInputName(index: number) {
    return `input_${index}`;
  }

  handleBlurInput = (e: any) => this.handleUserInput(e);

  handleUserInput = (e: any) => {
    let { name, value, rawValue = null } = e.target;
    value = rawValue !== null && name !== "expireDate" ? rawValue : value;
    // @ts-ignore
    this.setState({ [name]: value }, () => this.validateField(name, value));
  };

  handleInputClear = (e: any) => {
    let { name } = e.target;
    // @ts-ignore
    this.setState({ [name]: "" }, () => this.validateField(name, ""));
  };

  validateField = (fieldName: keyof State, value: string) => {
    const {
      t,
      item: { productParams },
    } = this.props;
    const param = productParams[parseInt(fieldName.replace("input_", ""))];
    let formValidFields = this.state.formValidFields;
    let fieldValidationErrors = this.state.formErrors;
    let errorText = "";
    let requiredFieldErrorText = t("Please fill out this field.");

    if (param) {
      errorText =
        param.mandatory && value.length ? errorText : requiredFieldErrorText;

      if (param.validationRegExp) {
        const regexp = new RegExp(param.validationRegExp);
        errorText = regexp.test(value)
          ? errorText
          : t("Please fill with correct format.");
      }

      fieldValidationErrors[fieldName] = errorText;
      formValidFields[fieldName] = !errorText.length;
    }

    this.setState(
      {
        formErrors: fieldValidationErrors,
        formValidFields,
      },
      this.validateForm
    );
  };

  validateForm = () => {
    this.setState({
      formValid: !this.formHasErrors(this.state.formValidFields),
    });
  };

  formHasErrors = (formValidFields: any) => {
    return Object.keys(formValidFields).some((key) => !formValidFields[key]);
  };

  getErrorMessagesProps = (fieldName: string) => {
    const { t } = this.props;
    return {
      errorMessage: t(this.state.formErrors[fieldName]),
      errorMessageForce: !!this.state.formErrors[fieldName],
    };
  };

  renderAdditionalParameters = () => {
    const {
      item: { productParams },
      t,
    } = this.props;

    if (!productParams) return null;

    return (
      <>
        <BlockTitle className="item-title">
          {t("Additional parameters")}
        </BlockTitle>
        <List noHairlinesMd form noHairlinesBetweenMd>
          {productParams.map((param, i) => {
            let name = this.getInputName(i);
            return (
              <ListInput
                key={name}
                name={name}
                label={t(param.name).toString()}
                floatingLabel
                type="text"
                placeholder=""
                clearButton
                required={param.mandatory}
                onBlur={this.handleBlurInput}
                onChange={this.handleUserInput}
                onInputClear={this.handleInputClear}
                value={this.state[name] || ""}
                slot="list"
                {...this.getErrorMessagesProps(name)}
              />
            );
          })}
        </List>
      </>
    );
  };

  render() {
    const {
      t,
      item,
      onSheetOpen,
      onSheetClose,
      onSheetClosed,
      ...rest
    } = this.props;

    if (!item) return null;

    return (
      <Sheet
        id="add_to_cart__sheet"
        className="add-to-cart__sheet"
        swipeToClose
        backdrop
        {...rest}
        onSheetOpen={(instance?: any) => {
          if (onSheetOpen) {
            onSheetOpen(instance);
          }
          this.setState({ sheetOpen: true, addToCartSheetItemCount: 1 });
        }}
        onSheetClose={(instance?: any) => {
          if (onSheetClose) {
            onSheetClose(instance);
          }
        }}
        onSheetClosed={(instance?: any) => {
          if (onSheetClosed) {
            onSheetClosed(instance);
          }
          this.setState({
            sheetOpen: false,
            addToCartSheetItemCount: 1,
            formValid: false,
            formErrors: {},
            formValidFields: {},
          });
        }}
      >
        <BlockTitle medium>{t("Item options")}</BlockTitle>
        <BlockTitle className="item-title">{t("Choose quantity")}</BlockTitle>
        <Block>{this.renderStepperButtons()}</Block>
        {this.renderAdditionalParameters()}

        {item && (
          <Block>
            <Button
              className="add-to-cart-btn"
              round
              large
              fill
              onClick={this.handleAddToCartClick}
            >
              <span>{this.renderItemTotal()}</span>
              <span>{t("Add to cart")}</span>
            </Button>
          </Block>
        )}
      </Sheet>
    );
  }
}

const mapStateToProps = (state: IApplicationStore) => {
  return {};
};

export default compose<React.ComponentClass<Props>>(
  withTranslation(),
  connect(mapStateToProps, null)
)(AddToCartSheetPage);
