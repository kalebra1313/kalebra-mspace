import React from "react";
import "./styles.less";
import codes from "./phone-codes";
import Flag from "react-world-flags";
import {useTranslation} from "react-i18next";
import Select, {components} from 'react-select';
const {ValueContainer, Placeholder} = components;

const options = Object.keys(codes).map(key => ({code: key, value: codes[key].code, label: codes[key].code}))

const formatOptionLabel = (data) => (
  <div className="custom-option">
    <Flag className="flag" code={data?.code}/>
    <div className="text-value">{data?.label}</div>
  </div>
)

const CustomValueContainer = ({children, ...props}) => {
  return (
    <ValueContainer {...props}>
      <Placeholder {...props} isFocused={props.isFocused}>
        {props.selectProps.placeholder}
      </Placeholder>
      {React.Children.map(children, child =>
        child && child.type !== Placeholder ? child : null
      )}
    </ValueContainer>
  )
}

type PhoneCodeSelectProps = {
  value: string;
  onChange: (string) => void;
}

const PhoneCodeSelect = ({value, onChange}: PhoneCodeSelectProps) => {
  const {t} = useTranslation()

  return (
    <Select
      id="phone-code-select"
      classNamePrefix="phone-code-select"
      placeholder="Code"
      isSearchable
      options={options}
      value={value}
      onChange={onChange}
      components={{ValueContainer: CustomValueContainer}}
      formatOptionLabel={formatOptionLabel}
      noOptionsMessage={() => t("Nothing found")}
    />
  )
}

export default PhoneCodeSelect
