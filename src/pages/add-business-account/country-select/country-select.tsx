import React, {useEffect, useRef, useState} from 'react';
import './styles.less';
import Select, {components} from "react-select";
import Flag from "react-world-flags";
import {useTranslation} from "react-i18next";
import {useSelector} from "react-redux";
import {IApplicationStore} from "../../../store/rootReducer";
import classNames from "classnames";
const {ValueContainer, Placeholder} = components;

const formatOptionLabel = (country) => (
  <div className="custom-option">
    <Flag code={country?.value}/>
    {country?.label}
  </div>
)

const CustomValueContainer = ({children, ...props}) => (
  <ValueContainer {...props}>
    <Placeholder {...props} isFocused={props.isFocused}>
      {props.selectProps.placeholder}
    </Placeholder>
    {React.Children.map(children, child =>
      child && child.type !== Placeholder ? child : null
    )}
  </ValueContainer>
)

type Country = {
  value: string;
  label: string;
}

type CountrySelectProps = {
  value: Country;
  options: Country[];
  onChange: (string) => void;
  openPopup: () => void;
  className?: string;
  validate?: boolean;
}

const CountrySelect = ({value, options, onChange, openPopup, className = '', validate = false}: CountrySelectProps) => {
  const {t} = useTranslation()
  const {width: deviceWidth} = useSelector((state: IApplicationStore) => state.rootReducer.resizeEvent)
  const isSmallScreen = deviceWidth < 768

  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [error, setError] = useState("")

  const wrapperRef = useRef(null)
  const handleClickOutsideCountry = (event) => {
    if (isMenuOpen && wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      if (!value) setError(t("Please select an item in the list."))
      setIsMenuOpen(false)
    }
  }
  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutsideCountry)
    return () => {
      document.removeEventListener("mousedown", handleClickOutsideCountry)
    }
  }, [wrapperRef, isMenuOpen])

  const handleOnMenuOpen = () => {
    if (!isSmallScreen) {
      setIsMenuOpen(true)
    } else {
      setIsMenuOpen(false)
      openPopup()
    }
  }

  const handleChange = (country) => {
    setIsMenuOpen(false)
    if (country.value) setError("")
    onChange(country)
  }

  return (
    <div className={classNames(className, "country-select", error && !isMenuOpen && "error" )} ref={wrapperRef}>
      <Select
        id="country-select"
        classNamePrefix="country-select"
        placeholder={t("Country")}
        isSearchable
        value={value}
        options={options}
        onChange={handleChange}
        components={{ValueContainer: CustomValueContainer}}
        formatOptionLabel={formatOptionLabel}
        noOptionsMessage={() => t("Nothing found")}
        menuIsOpen={isMenuOpen}
        onMenuOpen={handleOnMenuOpen}
      />
      {validate && error && !isMenuOpen && <div className="error-message">{error}</div>}
    </div>
  )
}

export default CountrySelect
