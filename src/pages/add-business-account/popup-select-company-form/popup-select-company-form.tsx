import React from "react";
import "./style.less";
import {Link, Page} from "framework7-react";

type Form = {
  value: string;
  label: string;
}

type PopupSelectCompanyForm = {
  options: Form[];
  onChange: (string) => void;
  closePopup: () => void;
}

const PopupSelectCompanyForm = ({options, onChange, closePopup}: PopupSelectCompanyForm) => {
  const handleOptionClick = (option) => {
    onChange(option)
    closePopup()
  }

  return (
    <Page id="select-company-form-popup-page">
      <div className="header">
        <h2>Company Legal Form</h2>
        <Link onClick={closePopup} iconF7="xmark" iconOnly/>
      </div>
      <ul className="options">
        {options?.map((option, index) => (
          <li key={index} onClick={() => handleOptionClick(option)}>
            {option.label}
          </li>
        ))}
      </ul>
    </Page>
  )
}

export default PopupSelectCompanyForm
