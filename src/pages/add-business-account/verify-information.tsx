import React, {useEffect} from "react";
import "./style.less";
import {Button, Navbar, NavLeft, Link, Col, Page, Row} from "framework7-react";
import {Router} from "framework7/modules/router/router";
import {useDispatch, useSelector} from "react-redux";
import {IApplicationStore} from "../../store/rootReducer";
import {AccountWsControllerApi} from "../../types/commonapi";
import {useTranslation} from "react-i18next";
import {updateProfile} from "../../actions/sessionActions";

type VerifyInformationProps = {
  f7router?: Router.Router;
}

const VerifyInformation = ({f7router}: VerifyInformationProps) => {
  const {t} = useTranslation()
  const info = useSelector((state: IApplicationStore) => state.addCompanyReducer)
  const {width: deviceWidth} = useSelector((state: IApplicationStore) => state.rootReducer.resizeEvent)
  const isSmallScreen = deviceWidth < 768

  const {companyInfo} = info
  const dispatch = useDispatch()

  useEffect(() => {
    const sendInfo = async () => {
      await new AccountWsControllerApi().transformAccountUsingPOST({
        companyAddress: {
          countryCode: companyInfo.country,
          city: companyInfo.addressCity,
          firstAddressLine: companyInfo.address,
          postalCode: companyInfo.postcode
        },
        companyType: Number(companyInfo.form),
        companyName: companyInfo.name,
        registrationNumber: companyInfo.regNumber,
        taxNumber: companyInfo.vatNumber,
        companyEmail: companyInfo.email,
        companyPhone: {
          fullNumber: companyInfo.phone,
          countryCode: companyInfo.phoneCode
        },
        companyUrls: companyInfo.links
      }, "business")
      await updateProfile(dispatch)
    }

    if (info) sendInfo().then()
  }, [info])

  return (
    <Page id="verify_business_account_page" name="verify_business_account_page">
      <Navbar>
        <NavLeft>
          {!isSmallScreen ? (
          <Link back iconF7="xmark" iconOnly/>
          ) : (
            <Link href="/profile/seller-area/" iconF7="xmark" iconOnly panelClose/>
          )}
        </NavLeft>
      </Navbar>

      <Row resizableFixed>
        <Col width="100">
          <div className="main-content">
            <h2>{t("Your data is being verified")}</h2>
            <p>{t("Your data will be checked soon. Meanwhile, you can add products and set up your homepage.")}</p>
            <Button fill round onClick={() => f7router.navigate('/')}>{t("Seller Area")}</Button>
          </div>
        </Col>
      </Row>
    </Page>
  )
}

export default VerifyInformation
