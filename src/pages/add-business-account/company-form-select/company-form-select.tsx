import React, {useEffect, useRef, useState} from "react";
import "./styles.less";
import classNames from "classnames";
import {useSelector} from "react-redux";
import {IApplicationStore} from "../../../store/rootReducer";
import {useTranslation} from "react-i18next";

type Form = {
  value: string;
  label: string;
}

type CompanyFormSelectProps = {
  value?: Form;
  options: Form[];
  label?: string;
  onChange: (string) => void;
  openPopup: () => void;
  className?: string;
}

const CompanyFormSelect = ({value, options, label, onChange, openPopup, className}: CompanyFormSelectProps) => {
  const {t} = useTranslation()
  const [isOpen, setIsOpen] = useState(false)
  const [selectedOption, setSelectedOption] = useState("")
  const [error, setError] = useState("")
  const {width: deviceWidth} = useSelector((state: IApplicationStore) => state.rootReducer.resizeEvent)
  const isSmallScreen = deviceWidth < 768

  const wrapperRef = useRef(null)
  useEffect(() => {
    const handleClickOutsideCompany = (event) => {
      if (isOpen && wrapperRef.current && !wrapperRef.current.contains(event.target)) {
        if (!selectedOption) setError(t("Please select an item in the list."))
        setIsOpen(false)
      }
    }

    document.addEventListener("mousedown", handleClickOutsideCompany)
    return () => {
      document.removeEventListener("mousedown", handleClickOutsideCompany)
    }
  }, [wrapperRef, selectedOption, isOpen])

  useEffect(() => {
    if (value) setSelectedOption(value.label)
  }, [value])

  const handleOpen = () => {
    if (!isSmallScreen) {
      setIsOpen(!isOpen)
    } else {
      openPopup()
    }
  }

  const onOptionClicked = (option) => {
    setSelectedOption(option.label)
    setError("")
    setIsOpen(false)
    onChange(option)
  }

  return (
    <div className={className} id="dropdown" ref={wrapperRef}>
      <div className={classNames("dropdown-inner", isOpen && "open")} onClick={handleOpen}>
        <div
          className={classNames(
            "label",
            isOpen && "opened",
            (isOpen || selectedOption) && "moveUp",
            error && "show-error"
          )}
        >
          {label}
        </div>
        <div className={classNames("value", error && "show-error")}>{selectedOption}</div>
        {error && <div className="error-message">{error}</div>}
      </div>
      {isOpen && (
        <ul>
          {options.map((option, index) => (
            <li
              key={index}
              onClick={() => onOptionClicked(option)}
              className={classNames(option.label === selectedOption && "active")}
            >
              {option.label}
            </li>
          ))}
        </ul>
      )}
    </div>
  )
}

export default CompanyFormSelect
