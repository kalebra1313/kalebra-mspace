import React, {useEffect, useState} from "react";
import {useTranslation} from "react-i18next";
import {
  Button,
  Link,
  List,
  ListInput,
  Checkbox,
  Navbar,
  NavLeft,
  NavTitle,
  NavRight,
  Icon,
  Col, Page, Row, ListItem, Panel, Popup
} from "framework7-react";
import "./style.less";
import {IApplicationStore} from "../../store/rootReducer";
import {useDispatch, useSelector} from "react-redux";
import {IcDelete, IcLocation} from "../../components-ui/icons";
import {SET_COMPANY_INFO} from "../../actions/addCompanyActions";
import {Router} from "framework7/modules/router/router";
import {MapPopup} from "../../components/MapPopup";
import CompanyFormSelect from "./company-form-select/company-form-select";
import CountrySelect from "./country-select/country-select";
import PhoneCodeSelect from "./phone-code-select/phone-code-select";
import AddStoreInformation from "./add-store-information";
import PopupSelectCompanyForm from "./popup-select-company-form/popup-select-company-form";
import {CountrySelectPopup} from "../../components/CountrySelectPopup";

type AddCompanyInformationProps = {
  f7router: Router.Router;
}

const AddCompanyInformation = ({f7router}: AddCompanyInformationProps) => {
  const {t} = useTranslation()
  const dispatch = useDispatch()

  const countries = useSelector((state: IApplicationStore) => state.classificatorReducer.countryClassificator
    .map(c => ({value: c.code, label: c.name})))
  const {width: deviceWidth} = useSelector((state: IApplicationStore) => state.rootReducer.resizeEvent)
  const isSmallScreen = deviceWidth < 768
  const location = useSelector((state: IApplicationStore) => state.filterReducer.allFiltresLocation)
  const termsAndPrivacy = useSelector((state: IApplicationStore) => state.classificatorReducer
    .entitiesClassificators.Url_app)
  const companyForms = useSelector((state: IApplicationStore) => state.classificatorReducer
    .entitiesClassificators.Company_BusinessType.map(item => ({value: item.code, label: item.value})))

  const [country, setCountry] = useState(null)
  const [form, setForm] = useState(null)
  const [name, setName] = useState("")
  const [regNumber, setRegNumber] = useState("")
  const [vatNumber, setVatNumber] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState("")
  const [phoneCode, setPhoneCode] = useState(null)
  const [links, setLinks] = useState([""])
  const [addressCountry, setAddressCountry] = useState(null)
  const [addressCity, setAddressCity] = useState("")
  const [address, setAddress] = useState("")
  const [postcode, setPostcode] = useState("")
  const [isAgreeWithTerms, setIsAgreeWithTerms] = useState(false)

  // const [country, setCountry] = useState({value: "AF", label: "Afghanistan"})
  // const [form, setForm] = useState({value: "5", label: "Limited liability company"})
  // const [name, setName] = useState("warner")
  // const [regNumber, setRegNumber] = useState("warnerReg")
  // const [vatNumber, setVatNumber] = useState("warnerVat")
  // const [email, setEmail] = useState("warner@gmail.com")
  // const [phone, setPhone] = useState("77515246")
  // const [phoneCode, setPhoneCode] = useState({code: "AD", value: "+376", label: "+376"})
  // const [links, setLinks] = useState(["https://git.mvp.sh/outsource/mspace/-/merge_requests/77", "https://google.com"])
  // const [addressCountry, setAddressCountry] = useState({value: "AX", label: "Åland Islands"})
  // const [addressCity, setAddressCity] = useState("Tiraspol")
  // const [address, setAddress] = useState("19-15 Strada Stepnaia")
  // const [postcode, setPostcode] = useState("5500")
  // const [isAgreeWithTerms, setIsAgreeWithTerms] = useState(true)

  const [isEmailValid, setIsEmailValid] = useState(true)
  const [isLinksValid, setIsLinksValid] = useState([true])
  const [isPostCodeValid, setIsPostCodeValid] = useState(true)

  const [isNextButtonEnabled, setIsNextButtonEnabled] = useState(false)
  useEffect(() => {
    const isAllLinksNotEmpty = !!links.reduce((accumulator, currentValue) => accumulator && currentValue)
    const isAllLinksValid = isLinksValid.reduce((accumulator, currentValue) => accumulator && currentValue, true)
    setIsNextButtonEnabled(!!(country && country.value && form && form.value && name && regNumber && email && isEmailValid
      && phoneCode && phoneCode.value && phone && isAllLinksNotEmpty && isAllLinksValid && addressCountry && addressCountry.value
      && addressCity && address && postcode && isPostCodeValid && isAgreeWithTerms))
  }, [country && !!country.value, form && form.value, !!name, !!regNumber, !!email, isEmailValid, phoneCode && phoneCode.value,
    !!phone, links, isLinksValid, addressCountry && !!addressCountry.value, !!addressCity, !!address, isPostCodeValid, isAgreeWithTerms])

  const [isCountryPopupOpened, setIsCountryPopupOpened] = useState(false)
  const [isAddressCountryPopupOpened, setIsAddressCountryPopupOpened] = useState(false)
  const [isCompanyFormPopupOpened, setIsCompanyFormPopupOpened] = useState(false)
  const [isMapPopupOpened, setIsMapPopupOpened] = useState(false)

  const handleInput = (e, field) => {
    const mapSetFunctions = {
      name: setName,
      regNumber: setRegNumber,
      vatNumber: setVatNumber,
      email: setEmail,
      phone: setPhone,
      addressCity: setAddressCity,
      address: setAddress,
      postcode: setPostcode
    }
    mapSetFunctions[field](e.target.value);
  }

  const handleLinksChange = (e, index) => {
    const newLinks: string[] = [...links];
    newLinks[index] = e.target.value;
    setLinks(newLinks)
  }

  const handleLinksRemove = (index) => {
    const newLinks = [...links]
    if (newLinks.length > 1 && index !== -1) {
      newLinks.splice(index, 1)
      setLinks(newLinks)
    }

    const newIsLinksValid = [...isLinksValid]
    if (newIsLinksValid.length > 1 && index !== -1) {
      newIsLinksValid.splice(index, 1)
      setIsLinksValid(newIsLinksValid)
    }
  }

  const handleLinksAdd = () => {
    setLinks([...links, ""])
    setIsLinksValid([...isLinksValid, true])
  }

  const handleClickNextButton = () => {
    const validPhoneCode = phoneCode.value.replace('+', '')
    const companyInfo = {
      country: country.value,
      form: form.value,
      name,
      regNumber,
      vatNumber,
      email,
      phone: `${validPhoneCode}${phone}`,
      phoneCode: validPhoneCode,
      links,
      addressCountry: addressCountry.value,
      addressCity,
      address,
      postcode
    }
    dispatch({type: SET_COMPANY_INFO, companyInfo})
    if (!isSmallScreen) {
      f7router.navigate('/profile/seller-area/add-business-account/store-info/')
    }
  }

  const setLocation = (address) => {
    setAddressCountry(countries.find(c => c.value === address.countryCode))
    setAddressCity(address.city)
    setAddress(address.firstAddressLine)
    if (address.postalCode) setPostcode(address.postalCode)
  }

  const handleLinksValidation = (isValid, index) => {
    const newIsLinksValid = [...isLinksValid]
    newIsLinksValid[index] = isValid
    setIsLinksValid(newIsLinksValid)
  }

  return (
    <Page id="add_company_information_page" name="add_company_information_page">
      <Navbar className="add-company-information-navbar">
        <NavLeft>
          {!isSmallScreen ? (
            <Link back iconF7="xmark" iconOnly/>
          ) : (
            <Link href="/profile/seller-area/" iconF7="xmark" iconOnly panelClose/>
          )}
        </NavLeft>
        <NavTitle>{t("Switch to Business Account")}</NavTitle>
        <NavRight>
          <Button className="next-button" fill round disabled={!isNextButtonEnabled} onClick={handleClickNextButton}>
            {t("Next")}
            <Icon className="next-icon" f7="chevron_right" size="16px"/>
          </Button>
        </NavRight>
      </Navbar>

      <Row resizableFixed>
        <Col width="15" className="menu-steps">
          <List noHairlines noHairlinesBetween>
            <ListItem className="active">{t("Company Information").toString()}</ListItem>
            <ListItem title={t("Add Store").toString()}/>
          </List>
        </Col>
        <Col width="100" medium="85">
          <div className="main-content">
            <h1>{t("Add Company Information")}</h1>
            <p>{t("Add your company information and create your first store as business account")}</p>

            <div className="block-inputs">
              <h2>{t("General Information")}</h2>
              <CountrySelect
                className="custom-dropdown"
                value={country}
                options={countries}
                onChange={setCountry}
                validate
                openPopup={() => setIsCountryPopupOpened(true)}
              />
              <CompanyFormSelect
                className="custom-dropdown"
                value={form}
                label={t("Company Legal Form").toString()}
                options={companyForms}
                onChange={setForm}
                openPopup={() => setIsCompanyFormPopupOpened(true)}
              />
              <List noHairlinesMd>
                <ListInput
                  value={name}
                  label={t("Company Name").toString()}
                  floatingLabel
                  type="text"
                  required
                  validate
                  className="info-input"
                  onChange={e => handleInput(e, 'name')}
                />
                <ListInput
                  value={regNumber}
                  label={t("Registration Number").toString()}
                  floatingLabel
                  type="text"
                  required
                  validate
                  className="info-input"
                  onChange={e => handleInput(e, 'regNumber')}
                />
                <ListInput
                  value={vatNumber}
                  label={t("VAT Number (Optional)").toString()}
                  floatingLabel
                  type="text"
                  className="info-input"
                  onChange={e => handleInput(e, 'vatNumber')}
                />
              </List>
            </div>

            <div className="block-inputs">
              <h2 className="h2-contacts">{t("Contacts")}</h2>
              <p>{t("Contact information required for the verification process. It will not be shown to customers.")}</p>
              <List noHairlinesMd>
                <ListInput
                  value={email}
                  label={t("E-mail").toString()}
                  floatingLabel
                  type="email"
                  required
                  validate
                  onValidate={(isValid) => setIsEmailValid(isValid)}
                  className="info-input"
                  onChange={e => handleInput(e, 'email')}
                />
                <div className="inputs-row">
                  <PhoneCodeSelect value={phoneCode} onChange={setPhoneCode}/>
                  <ListInput
                    value={phone}
                    label={t("Phone Number").toString()}
                    floatingLabel
                    type="tel"
                    required
                    validate
                    pattern="[0-9]*"
                    errorMessage={t("only digits")}
                    className="info-input"
                    onChange={e => handleInput(e, 'phone')}
                  />
                </div>
                {links?.map((link, index) => (
                  <div className="inputs-row" key={index}>
                    <ListInput
                      label={t("Link").toString()}
                      floatingLabel
                      type="url"
                      required
                      validate
                      onValidate={(isValid) => handleLinksValidation(isValid, index)}
                      className="info-input"
                      value={link}
                      onChange={e => handleLinksChange(e, index)}
                    />
                    <Button
                      className="delete-link-button"
                      onClick={() => handleLinksRemove(index)}
                      disabled={links.length === 1}
                    >
                      <IcDelete fill="#CCCCCC"/>
                    </Button>
                  </div>
                ))}
                <Button onClick={handleLinksAdd} className="add-link-button" color="red">+ {t("Add a Link")}</Button>
              </List>
            </div>

            <div className="block-inputs">
              <h2>{t("Company Legal Address")}</h2>
              <Button className="select-map-button" onClick={() => setIsMapPopupOpened(true)}>
                <IcLocation fill="#EF5D54"/>&nbsp;{t("Select on the Map")}
              </Button>
              <List noHairlinesMd>
                <div className="inputs-row md-column">
                  <CountrySelect
                    className="custom-dropdown-row"
                    value={addressCountry}
                    options={countries}
                    onChange={setAddressCountry}
                    openPopup={() => setIsAddressCountryPopupOpened(true)}
                  />
                  <ListInput
                    label={t("City").toString()}
                    floatingLabel
                    type="text"
                    required
                    className="info-input"
                    value={addressCity}
                    onChange={e => handleInput(e, 'addressCity')}
                  />
                </div>
                <ListInput
                  label={t("Address").toString()}
                  floatingLabel
                  type="text"
                  required
                  className="info-input"
                  value={address}
                  onChange={e => handleInput(e, 'address')}
                />
                <ListInput
                  label={t("Postcode").toString()}
                  floatingLabel
                  type="text"
                  errorMessage={t("only digits")}
                  required
                  validate
                  pattern="[0-9]*"
                  maxlength="10"
                  onValidate={(isValid) => setIsPostCodeValid(isValid)}
                  className="info-input"
                  onChange={e => handleInput(e, 'postcode')}
                  value={postcode}
                />
              </List>
              <div className="privacy">
                <Checkbox checked={isAgreeWithTerms} onChange={() => setIsAgreeWithTerms(!isAgreeWithTerms)}/>
                <div>
                  {t("You Agree with our")}&nbsp;
                  <Link external target="_blank" href={termsAndPrivacy.find(i => i.code === "PrivacyPolicy")?.value}>
                    {t("Privacy Policy")}
                  </Link>&nbsp;
                  {t("and")}&nbsp;
                  <Link external target="_blank" href={termsAndPrivacy.find(i => i.code === "TermsAndConditions")?.value}>
                    {t("Terms of Use")}
                  </Link>
                </div>
              </div>
            </div>
            {isSmallScreen && (
              <>
                <div className="pager-footer">
                  <div className="active"/>
                  <div/>
                </div>
                <Button
                  className="next-mobile"
                  panelOpen="right"
                  disabled={!isNextButtonEnabled}
                  onClick={handleClickNextButton}
                >
                  Next<Icon className="next-icon" f7="chevron_right" size="16px"/>
                </Button>
              </>
            )}
          </div>
        </Col>
      </Row>

      <MapPopup
        backdrop={false}
        coordinates={location}
        initialized={isMapPopupOpened}
        opened={isMapPopupOpened}
        onPopupClosed={() => setIsMapPopupOpened(false)}
        onLocationSelect={(position, place, placeId, address) => setLocation(address)}
      />

      {isSmallScreen && (
        <>
          <Panel right cover swipe swipeOnlyClose>
            <AddStoreInformation f7router={f7router}/>
          </Panel>
          <CountrySelectPopup
            opened={isCountryPopupOpened}
            onCountrySelect={(country) => setCountry({value: country.code, label: country.name})}
            onPopupClosed={() => setIsCountryPopupOpened(false)}
          />
          <CountrySelectPopup
            opened={isAddressCountryPopupOpened}
            onCountrySelect={(country) => setAddressCountry({value: country.code, label: country.name})}
            onPopupClosed={() => setIsAddressCountryPopupOpened(false)}
          />
          <Popup
            id="select-company-form-popup"
            backdrop
            opened={isCompanyFormPopupOpened}
            onPopupClose={() => setIsCompanyFormPopupOpened(false)}
            slot="fixed"
          >
            <PopupSelectCompanyForm
              options={companyForms}
              onChange={setForm}
              closePopup={() => setIsCompanyFormPopupOpened(false)}
            />
          </Popup>
        </>
      )}
    </Page>
  )
}

export default AddCompanyInformation
