import React, {useEffect, useRef, useState} from "react";
import {useTranslation} from "react-i18next";
import "./style.less";
import {
  Button,
  List,
  ListInput,
  Icon,
  Navbar,
  NavLeft,
  Link,
  NavTitle,
  NavRight,
  Col,
  Page,
  Row, ListItem
} from "framework7-react";
import {IcDelete} from "../../components-ui/icons";
import {SET_STORE_INFO} from "../../actions/addCompanyActions";
import {useDispatch, useSelector} from "react-redux";
import {Router} from "framework7/modules/router/router";
import {IApplicationStore} from "../../store/rootReducer";

type AddStoreInformationProps = {
  f7router: Router.Router;
}

const AddStoreInformation = ({f7router}: AddStoreInformationProps) => {
  const {t} = useTranslation()
  const dispatch = useDispatch()
  const inputFile = useRef(null)
  const {width: deviceWidth} = useSelector((state: IApplicationStore) => state.rootReducer.resizeEvent)
  const isSmallScreen = deviceWidth < 768

  const [avatarImg, setAvatarImg] = useState("")
  const [name, setName] = useState("")
  const [email, setEmail] = useState("")
  const [phone, setPhone] = useState("")
  const [description, setDescription] = useState("")
  const [links, setLinks] = useState([""])
  const [isLinksValid, setIsLinksValid] = useState([true])

  // const [avatarImg, setAvatarImg] = useState("")
  // const [name, setName] = useState("warner store")
  // const [email, setEmail] = useState("store@gmail.com")
  // const [phone, setPhone] = useState("3754821562")
  // const [description, setDescription] = useState("store description")
  // const [links, setLinks] = useState(["http://store.com", "http://store.org"])
  // const [isLinksValid, setIsLinksValid] = useState([true, true])

  const [isDoneButtonEnabled, setIsDoneButtonEnabled] = useState(false)
  useEffect(() => {
    const isAllLinksNotEmpty = !!links.reduce((accumulator, currentValue) => accumulator && currentValue)
    const isAllLinksValid = isLinksValid.reduce((accumulator, currentValue) => accumulator && currentValue, true)
    setIsDoneButtonEnabled(!!(name && isAllLinksNotEmpty && isAllLinksValid))
  }, [name, links, isLinksValid])

  const handleInput = (e, field) => {
    const mapSetFunctions = {
      name: setName,
      email: setEmail,
      phone: setPhone,
      description: setDescription,
    }
    mapSetFunctions[field](e.target.value);
  }

  const handleLinksChange = (e, index) => {
    const newLinks: string[] = [...links];
    newLinks[index] = e.target.value;
    setLinks(newLinks)
  }

  const handleLinksRemove = (index) => {
    const newLinks = [...links]
    if (newLinks.length > 1 && index !== -1) {
      newLinks.splice(index, 1)
      setLinks(newLinks)
    }

    const newIsLinksValid = [...isLinksValid]
    if (newIsLinksValid.length > 1 && index !== -1) {
      newIsLinksValid.splice(index, 1)
      setIsLinksValid(newIsLinksValid)
    }
  }

  const handleLinksAdd = () => {
    setLinks([...links, ""])
    setIsLinksValid([...isLinksValid, true])
  }

  const handleClickDoneButton = () => {
    dispatch({type: SET_STORE_INFO, storeInfo: {name, email, phone, description, links}})
    f7router.navigate('/profile/seller-area/add-business-account/verify-data/')
  }

  const onFileUpload = (e) => {
    const file = e.target.files[0]
    if (file) {
      const reader = new FileReader()
      reader.readAsDataURL(file)
      reader.onload = (ev) => setAvatarImg(ev.target.result)
    }
  }

  const handleLinksValidation = (isValid, index) => {
    const newIsLinksValid = [...isLinksValid]
    newIsLinksValid[index] = isValid
    setIsLinksValid(newIsLinksValid)
  }

  return (
    <Page id="add_store_information_page" name="add_store_information_page">
      <Navbar className="add-store-information-navbar">
        <NavLeft>
          {!isSmallScreen ? (
            <Link back iconF7="xmark" iconOnly/>
          ) : (
            <Link href="/profile/seller-area/" iconF7="xmark" iconOnly panelClose/>
          )}
        </NavLeft>
        <NavTitle>{t("Switch to Business Account")}</NavTitle>
        <NavRight>
          <Button className="next-button" fill round disabled={!isDoneButtonEnabled} onClick={handleClickDoneButton}>
            {t("Done")}
          </Button>
        </NavRight>
      </Navbar>

      <Row resizableFixed>
        <Col width="15" className="menu-steps">
          <List noHairlines noHairlinesBetween>
            <ListItem>
              <Icon f7="checkmark_alt_circle_fill" color="red" size="24" className="step-passed-icon"/>
              {t("Company Information").toString()}
            </ListItem>
            <ListItem title={t("Add Store").toString()} className="active"/>
          </List>
        </Col>
        <Col width="100" medium="85">
          <div className="main-content">
            <h1>{t("Create Store")}</h1>
            <List noHairlinesMd>
              <div className="store-image-wrapper">
                <input type="file" name="file" ref={inputFile} onChange={onFileUpload} style={{display: "none"}}/>
                {!avatarImg ? (
                  <div className="store-image" onClick={() => inputFile.current.click()}>
                    <Icon f7="camera"/>
                  </div>
                ) : (
                  <img className="store-image" src={avatarImg} alt="avatar"/>
                )}
              </div>
              <ListInput
                label={t("Store Name (required)").toString()}
                floatingLabel
                type="text"
                required
                validate
                maxlength="40"
                className="info-input"
                onChange={e => handleInput(e, 'name')}
              />
              <ListInput
                label={t("Store Email").toString()}
                floatingLabel
                type="email"
                validate
                maxlength="40"
                className="info-input"
                onChange={e => handleInput(e, 'email')}
              />
              <ListInput
                label={t("Store Phone Number").toString()}
                floatingLabel
                type="tel"
                validate
                maxlength="20"
                pattern="[0-9]*"
                errorMessage={t("only digits")}
                className="info-input"
                onChange={e => handleInput(e, 'phone')}
              />
            </List>

            <h2>{t("Description")}</h2>
            <p>{t("Describe in 3-5 word what you sell in store.")}</p>
            <List noHairlinesMd>
              <ListInput
                label={t("Description").toString()}
                floatingLabel
                type="text"
                validate
                maxlength="60"
                className="info-input"
                onChange={e => handleInput(e, 'description')}
              />
            </List>

            <h2>{t("Links")}</h2>
            <p>{t("Provide links to websites used to sell goods or services (website, instagram account, etc.). This won't be displayed on store profile.")}</p>
            <List noHairlinesMd>
              {links?.map((link, index) => (
                <div className="inputs-row" key={index}>
                  <ListInput
                    label={t("Link (required)").toString()}
                    floatingLabel
                    type="url"
                    required
                    validate
                    onValidate={(isValid) => handleLinksValidation(isValid, index)}
                    className="info-input"
                    value={link}
                    onChange={e => handleLinksChange(e, index)}
                  />
                  <Button
                    className="delete-link-button"
                    onClick={() => handleLinksRemove(index)}
                    disabled={links.length === 1}
                  >
                    <IcDelete fill="#CCCCCC"/>
                  </Button>
                </div>
              ))}
              <Button onClick={handleLinksAdd} className="add-link-button" color="red">+ {t("Add a Link")}</Button>
            </List>
            {isSmallScreen && (
              <>
                <Button className="prev-mobile" back panelClose>
                  <Icon className="prev-icon" f7="chevron_left" size="16px"/>Back
                </Button>
                <div className="pager-footer">
                  <div className="active"/>
                  <div className="active"/>
                </div>
                <Button
                  className="next-mobile"
                  panelClose="right"
                  disabled={!isDoneButtonEnabled}
                  onClick={handleClickDoneButton}
                >
                  Done<Icon className="next-icon" f7="chevron_right" size="16px"/>
                </Button>
              </>
            )}
          </div>
        </Col>
      </Row>
    </Page>
  )
}

export default AddStoreInformation
