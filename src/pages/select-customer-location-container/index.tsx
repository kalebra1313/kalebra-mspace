import React from "react";
import { bindActionCreators, compose } from "redux";
import { CountrySelectPopup } from "../../components/CountrySelectPopup";
import { IApplicationStore } from "../../store/rootReducer";
import SelectCustomerLocationSheet from "../select-customer-location__sheet";
import {
  toggleSelectCustomerLocationSheet,
  changeCountry,
  toggleSelectCountryPopup,
} from "../../actions/customer-location/customerLocationActions";
import { WithTranslation, withTranslation } from "react-i18next";
import { connect } from "react-redux";
import { getProfile } from "../../selectors/profile";
import { F7Popup } from "framework7-react";

type Props = Partial<ReturnType<typeof mapStateToProps>> &
  Partial<ReturnType<typeof mapDispatchToProps>> &
  F7Popup.Props & {};

class SelectCustomerLocationContainer extends React.Component<
  Props & WithTranslation
> {
  render() {
    const {
      countrySelectPopupOpened,
      toggleSelectCountryPopup,
      changeCountry,
    } = this.props;
    return (
      <>
        <SelectCustomerLocationSheet />
        <CountrySelectPopup
          opened={countrySelectPopupOpened}
          onCountrySelect={(val) => {
            if (val) {
              changeCountry(val.code, true);
            }
          }}
          onPopupClosed={() => {
            toggleSelectCountryPopup(false);
          }}
        />
      </>
    );
  }
}

const mapStateToProps = (state: IApplicationStore) => ({
  addressList: getProfile({ ...state }).addresses,
  ...state.customerLocationReducer,
});
const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      toggleSelectCustomerLocationSheet,
      toggleSelectCountryPopup,
      changeCountry,
    },
    dispatch
  );

export default compose(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps)
)(SelectCustomerLocationContainer) as React.ComponentClass<Props>;
