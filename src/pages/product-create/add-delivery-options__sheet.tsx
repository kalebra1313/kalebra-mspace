import React from "react";
import {
  Sheet,
  PageContent,
  BlockTitle,
  F7Sheet,
  List,
  ListItem,
} from "framework7-react";
import { WithTranslation, withTranslation } from "react-i18next";
import { compose } from "redux";
import { DeliveryMethod } from "../../types/marketplaceapi";
import { IcDelivery, IcLocation } from "../../components-ui/icons";
import { formatPrice } from "../../utils";
import connectCurrencies, {
  ICurrencyProps,
} from "../../store/connectCurrencies";

export const DELIVERY_METHOD = "Delivery method";
export const PICK_UP_POINT = "Pick-up point";

export type DeliveryMethodItem = DeliveryMethod & {
  selected?: boolean;
};

const getItemIcon = (type: DeliveryMethod.TypeEnum) =>
  type === DeliveryMethod.TypeEnum.DELIVERY ? (
    // @ts-ignore
    <IcDelivery slot="media" />
  ) : (
    // @ts-ignore
    <IcLocation slot="media" />
  );

type Props = F7Sheet.Props & {
  onSelectItemClickHandle?(
    item: typeof DELIVERY_METHOD | typeof PICK_UP_POINT
  ): void;
  deliveryMethods: DeliveryMethodItem[];
  onDeliveryMethodClickHandle?(uid: string): void;
};

const AddDeliveryOptionsSheet = ({
  t,
  onSelectItemClickHandle,
  deliveryMethods,
  onDeliveryMethodClickHandle,
  currencies,
  ...rest
}: Props & WithTranslation & ICurrencyProps) => (
  <Sheet id="add_delivery_options__sheet" backdrop {...rest}>
    <PageContent>
      <BlockTitle medium>{t("Add a delivery options")}</BlockTitle>
      <List noHairlines>
        <ListItem
          link
          title={t("Delivery method").toString()}
          noChevron
          onClick={() => onSelectItemClickHandle(DELIVERY_METHOD)}
        >
          {/* @ts-ignore */}
          <IcDelivery slot="media" />
        </ListItem>
        <ListItem
          link
          title={t("Pick-up point").toString()}
          noChevron
          onClick={() => onSelectItemClickHandle(PICK_UP_POINT)}
        >
          {/* @ts-ignore */}
          <IcLocation slot="media" />
        </ListItem>
      </List>
      {deliveryMethods && deliveryMethods.length && (
        <>
          <BlockTitle medium>{t("Saved")}</BlockTitle>
          <List
            noHairlines
            className="no-margin-top no-margin-bottom delivery-methods"
          >
            {deliveryMethods.map((item, i) => (
              <ListItem
                key={item.uid || i.toString()}
                noChevron
                link
                onClick={() => onDeliveryMethodClickHandle(item.uid)}
                className={item.selected ? "selected" : ""}
              >
                {getItemIcon(item.type)}
                <div slot="title">
                  <strong style={{ marginBottom: 12 }}>{item.name}</strong>
                  {item.options &&
                    item.options.length &&
                    item.options.map((o, i) => (
                      <div
                        key={o.uid || i.toString()}
                        style={{ marginBottom: 12 }}
                      >
                        <div>
                          {o.countries &&
                            o.countries.map((c) => c.name).join(", ")}
                        </div>
                        <div>{`${o.deliveryTimeDaysMin}-${o.deliveryTimeDaysMax}`}</div>
                        <div>
                          {o.price > 0
                            ? formatPrice(
                                o.price,
                                currencies.filter(
                                  (c) => c.code === o.currencyCode
                                )[0].symbol
                              )
                            : t("Free shipping")}
                        </div>
                      </div>
                    ))}
                </div>
              </ListItem>
            ))}
          </List>
        </>
      )}
    </PageContent>
  </Sheet>
);

export default compose(
  withTranslation(),
  connectCurrencies
)(AddDeliveryOptionsSheet) as React.ComponentClass<Props>;
