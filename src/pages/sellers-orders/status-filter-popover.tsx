import React from 'react';
import { Popover, F7Popover, List, ListItem, Row, Col, Block, Button, PageContent } from 'framework7-react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { compose } from 'redux';

type Props = WithTranslation & F7Popover.Props & {
  filter?: { [key: string]: string; },
  selected?: string[],
  onSelect?(status: string): void,
  onClear?(): void,
  onApply?(): void,
}

const StatusFilterPopover = ({filter, selected, onSelect, onClear, onApply, t, ...rest}: Props) => (
  <Popover
    id="status_filter_popover"
    closeByBackdropClick
    closeByOutsideClick
    {...rest}
  >
    <PageContent>
      <Block className="title">
        {t("Status")}
      </Block>
      <Block>
        <Row>
          <Col>
            <List noHairlines noHairlinesBetween>
              <ListItem
                checkbox
                title={t(filter['SE']).toString()}
                name="status"
                value="SE"
                checked={selected.includes("SE")}
                onChange={() => onSelect("SE")}
              />
              <ListItem
                checkbox
                title={t(filter['PA']).toString()}
                name="status"
                value="PA"
                checked={selected.includes("PA")}
                onChange={() => onSelect("PA")}
              />
              <ListItem
                checkbox
                title={t(filter['PRC']).toString()}
                name="status"
                value="PRC"
                checked={selected.includes("PRC")}
                onChange={() => onSelect("PRC")}
              />
              <ListItem
                checkbox
                title={t(filter['SHP']).toString()}
                name="status"
                value="SHP"
                checked={selected.includes("SHP")}
                onChange={() => onSelect("SHP")}
              />
              <ListItem
                checkbox
                title={t(filter['RCV']).toString()}
                name="status"
                value="RCV"
                checked={selected.includes("RCV")}
                onChange={() => onSelect("RCV")}
              />
            </List>
          </Col>
          <Col>
            <List noHairlines noHairlinesBetween>
              <ListItem
                checkbox
                title={t(filter['DLV']).toString()}
                name="status"
                value="DLV"
                checked={selected.includes("DLV")}
                onChange={() => onSelect("DLV")}
              />
              <ListItem
                checkbox
                title={t(filter['CA']).toString()}
                name="status"
                value="CA"
                checked={selected.includes("CA")}
                onChange={() => onSelect("CA")}
              />
              <ListItem
                checkbox
                title={t(filter['RF']).toString()}
                name="status"
                value="RF"
                checked={selected.includes("RF")}
                onChange={() => onSelect("RF")}
              />
              <ListItem
                checkbox
                title={t(filter['RP']).toString()}
                name="status"
                value="RP"
                checked={selected.includes("RP")}
                onChange={() => onSelect("RP")}
              />
            </List>
          </Col>
        </Row>
      </Block>
      <Block className="actions">
        <Row>
          <Col>
            {selected.length > 0 && (
              <Button
                round
                iconMaterial="clear"
                text={t("Clear all")}
                onClick={onClear}
                popoverClose
              />
            )}
          </Col>
          <Col>
            <Button
              round
              fill
              disabled={selected.length === 0}
              text={t("Apply")}
              onClick={onApply}
              popoverClose
            />
          </Col>
        </Row>
      </Block>
    </PageContent>
  </Popover>
)

export default compose(
  withTranslation()
)(StatusFilterPopover)