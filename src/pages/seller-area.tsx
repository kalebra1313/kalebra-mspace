import React, { Component } from "react";
import { Page, Navbar } from "framework7-react";
import { WithTranslation, withTranslation } from "react-i18next";
import { compose } from "redux";
import { SellerAreaMenu } from "../components/SellerAreaMenu";

class SellerAreaPage extends Component<WithTranslation> {

  render() {
    const { t } = this.props;

    return (
      <Page id="seller_area_page" name="seller-area-page">
        <Navbar
          title={t("Seller Area")}
          backLink={t("Back").toString()}
          noHairline
          noShadow
        />
        <SellerAreaMenu/>
      </Page>
    )
  }
}

export default compose(
  withTranslation(),
)(SellerAreaPage);
