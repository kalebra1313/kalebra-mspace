import React, { Component } from "react";
import ReactDOM from "react-dom";
import {
  Page,
  Block,
  BlockTitle,
  Icon,
  List,
  ListItem,
  Sheet,
  Row,
  Col,
  Popup,
  NavRight,
  Link,
  Navbar as F7Navbar,
  Chip,
} from "framework7-react";
import { compose } from "redux";
import { ContactSupportPopup } from "../components/ContactSupportPopup";
import connectFilter from "../store/connectFilter";
import connectCategories from "../store/connectCategories";
import { IApplicationStore, ResizeEvent } from "../store/rootReducer";
import { IProduct } from "../reducers/productReducer";
import { connect } from "react-redux";
import { ICategoryClassificator } from "../reducers/categoryReducer";
import connectCategoriesClassificator from "../store/connectCategoriesClassificator";
import { Price } from "../components/Price/index";
import AddWishButton from "../components/AddWishButton";
import ShareButton from "../components/ShareButton";
import {
  addToWishList,
  ISearchParams,
  loadProductWishList,
  searchClear,
  searchProducts,
} from "../actions/productActions";
import Slider, { SliderType, SliderItem } from "../components/Slider";
import { loadProductDetail } from "../actions/productActions";
import { withTranslation, WithTranslation } from "react-i18next";
import { Button } from "../components/ThemedButton";
import connectChat, { IChatProps } from "../store/connectChat";
import connectShare, { IShareProps } from "../store/connectShare";
import Map from "../components/Map";
import {
  DescriptionDetails,
  TechnicalDetails,
} from "../components/ProductDetails";
import { Profile } from "../reducers/sessionReducer";
import { getProfile } from "../selectors/profile";
import { getProductDetailsLink } from "../actions/shareActions";

import "./categories.less";
import "./product-details.less";
import { VideoPlayer } from "../components/VideoPlayer";
import { goToMessenger } from "../actions/profileActions";
import { chooseCategory } from "../actions/filterActions";
import { ProfileLink } from "../components/ProfileLink";
import { CategoriesMenuDesktop } from "../components/categories-menu-desktop";
import { ProfilePopover } from "../components/profile-popover";
import connectCart, { ICartProps } from "../store/connectCart";
import { createProductUrl, detectLocation, formatDate, formatPrice, getCategory, getSubRoutes } from "../utils";
import AddToCartSheetPage from "./product-details-add-to-cart__sheet";
import HaversineGeolocation from "haversine-geolocation";
import Navbar from "../components/navbar";
import connectSearch, { ISearchConnectorProps } from "../store/connectSearch";
import { Country } from "../types/commonapi";
import { DeliveryMethod } from "../types/marketplaceapi";
import { analytics } from "../Setup";
import { StartChat } from "../components/StartChat";
import Translation from "../components/Translation/Translation";
import PopoverButton from "../components/PopoverButton/PopoverButton";
import PopoverList from "../components/PopoverList/PopoverList";
import Breadcrumb from "../components/Breadcrumbs/Breadcrumb";
import { VerifyAccountPopup } from "../components/VerifyAccountPopup";
import { AboutPopup } from "../components/AboutPopup";
import Catalog from "../components/Catalog";

type Props = WithTranslation &
  IChatProps &
  IShareProps &
  ICartProps &
  ISearchConnectorProps & {
    uid?: string;
    productDetailsLoading?: boolean;
    productDetailsLoadingError?: any;
    item?: IProduct;
    categoriesClassificator?: ICategoryClassificator[];
    wishList?: IProduct[];
    addToWish?(uid?: string): void;
    updatingProfile?: boolean;
    loadProductDetail?(uid: string): void;
    profile?: Profile;
    loadProductWishList?(): () => Promise<void>;
    entryDirect?: boolean;
    resizeEvent?: ResizeEvent;
    goToMessenger?(): void;
    chooseCategory?(catid?: string | null): void;
    countryClassificator: Country[];
    country?: Country;
  };

type State = {
  searchBarEnable?: boolean;
  selectCategorySheetOpened?: boolean;
  categoriesMenuOpened?: boolean;
  profilePopoverOpened?: boolean;
  sortingMenuPopoverOpened?: boolean;
  addToCartSheetOpened?: boolean;
  addToCartSheetItemCount?: number;
  addToCartStepperInit?: boolean /* bug with Stepper */;
  loginPopupOpened?: boolean;
  registerPopupOpened?: boolean;
  contactSupportPopupOpened?: boolean;
  userLatLng?: any;
  startChatSheetOpened?: boolean;
  startChatPopupOpened?: boolean;
  sliderZoom: boolean;
  activeSlideIndex: number;
  translated: boolean;
  aboutPopupOpened?: boolean;
  verifyAccountPopupOpened?: boolean;
  sheetPopoverIsOpen: boolean;
};

const latLng = {
  latitude: 0,
  longitude: 0,
};

const addToCartItemDefaultState = {
  addToCartSheetItemCount: 1,
  addToCartStepperInit: false,
};

const deliveryKeysTitles = {
  shippingAllowed: "Shipping allowed",
  pickupAllowed: "Pick up allowed",
  returnAccepted: "Return allowed",
};

const containerStyle = {
  position: "relative",
  width: "100%",
  height: "526px",
};

class ProductDetailsPage extends Component<
  Props,
  {
    playVideoSheetOpened?: boolean;
    videoId?: string;
    videoType?: string;
    imageHoveredItemImageSwitcher?: number;
    openedFromCart?: boolean;
  } & State
> {
  constructor(props: Props) {
    super(props);
    this.state = {
      playVideoSheetOpened: false,
      addToCartSheetOpened: false,
      openedFromCart: false,
      loginPopupOpened: false,
      registerPopupOpened: false,
      contactSupportPopupOpened: false,
      startChatSheetOpened: false,
      startChatPopupOpened: false,
      userLatLng: latLng,
      sliderZoom: false,
      activeSlideIndex: 1,
      translated: true,
      aboutPopupOpened: false,
      verifyAccountPopupOpened: false,
      sheetPopoverIsOpen: false,
      ...addToCartItemDefaultState,
    };
  }

  pageInitHandle = () => {
    const filteredRoutes = this.$f7router.history.filter(
      (item) => item !== this.$f7route.path
    );

    if (
      filteredRoutes.length &&
      filteredRoutes[filteredRoutes.length - 1] === "/cart/"
    ) {
      this.setState({ openedFromCart: true });
    }
  };

  pageAfterInHandle = () => {
    this.loadProductDetails();
  };

  getSellerProducts = () => {
    const { item, search } = this.props;
    search({
      sellerUid: item.sellerUid,
      count: 10,
      offset: 0,
      resetSorting: true,
    });
  }

  async componentDidMount() {
    const location = await detectLocation();
    if (location.latitude !== 0 && location.longitude !== 0) {
      this.setState({
        userLatLng: {
          latitude: location.latitude,
          longitude: location.longitude,
        },
      });
    }

    // this.$f7ready((f7) => {
    //   console.log('ready')
    //   this.getSellerProducts();
    //   f7.once('pageInit', () => {
    //     console.log('pageInit')
    //     this.props.search({});
    //   })
    // })
  }

  async componentDidUpdate(prevProps: Props) {
    this.handleChatErrors(prevProps);
    this.handleShareErrors(prevProps);

    if (
      !this.props.productDetailsLoading &&
      !this.props.item &&
      !this.props.productDetailsLoadingError
    ) {
      this.props.loadProductWishList();
      this.loadProductDetails();
    } else if (
      prevProps.productDetailsLoadingError !==
      this.props.productDetailsLoadingError
    ) {
      this.$f7.dialog.alert(this.props.productDetailsLoadingError);
      console.log(this.props.productDetailsLoadingError);
    }

  }

  handleChatErrors = (prevProps: Props) => {
    const { error } = this.props.chatReducer;
    if (error && error !== prevProps.chatReducer.error) {
      this.$f7?.dialog.alert(error);
    }
  };

  handleShareErrors = (prevProps: Props) => {
    const { error } = this.props.shareStore;
    if (error && error !== prevProps.shareStore.error) {
      this.$f7?.dialog.alert(error);
    }
  };

  loadProductDetails = () => {
    const { uid } = this.props;
    this.props.loadProductDetail(uid);
  };

  addToWish = () => {
    const { profile, item, uid } = this.props;

    if (profile && profile.uid !== null && !this.isAddedToWishList()) {
      analytics.addToWishList(profile, item);
    }

    this.props.addToWish(uid);
  };

  addToCart = (count, isMobile: boolean) => {
    const { profile, item } = this.props;
    this.props.cartAdd(item, count);

    if (isMobile) {
      this.setState({ addToCartSheetOpened: false });
    } else {
      this.setState({ ...addToCartItemDefaultState });
    }

    if (profile && profile.uid !== null) {
      analytics.addToCart(profile, item, count);
    }
  };

  isAddedToWishList = () => {
    const { uid, wishList } = this.props;
    return wishList.filter((item) => item.uid === uid).length > 0;
  };

  getProductImagesSlides = (): SliderItem[] => {
    const { item } = this.props;
    const thumbnails = Object.keys(item)
      .filter((item) => item.startsWith("imageThumbnailUrl"))
      .map((key) => item[key]);

    return (
      item.images.map((image, i) => ({ image, small: thumbnails[i] })) || []
    );
  };

  renderAddressBlock = () => {
    const {
      item: { address, coordinates },
      t,
    } = this.props;
    const { userLatLng } = this.state;

    if (!address) return null;

    let addressString = "";
    try {
      if (address.countryCode) addressString = address.countryCode;
      if (address.city)
        addressString =
          addressString + (addressString ? ", " : "") + address.city;
    } catch (err) { }

    function toLatLgnLiteral(coordinates: string) {
      try {
        const arr = coordinates.split(",");
        return { lat: parseFloat(arr[0]), lng: parseFloat(arr[1]) };
      } catch { }
      return { lat: 0, lng: 0 };
    }

    function distance() {
      const latLng = toLatLgnLiteral(coordinates);
      if (
        userLatLng.latitude === 0 ||
        userLatLng.longitude === 0 ||
        latLng.lat === 0 ||
        latLng.lng === 0
      ) {
        return <></>;
      }

      const productLatLng = {
        latitude: latLng.lat,
        longitude: latLng.lng,
      };

      const distance = HaversineGeolocation.getDistanceBetween(
        userLatLng,
        productLatLng
      );
      if (distance > 0) {
        return (
          <>
            <span className="distance">
              {distance} {t("km")}
            </span>
          </>
        );
      }

      return <></>;
    }

    return (
      <Block className="location-label-block">
        {this.props.resizeEvent.width <= 768 && <BlockTitle>{t('Location')}</BlockTitle>}
        <div className="location-label">
          <i className="icon ic-location" />
          {addressString && <span className="address">{addressString}</span>}
          {distance()}
        </div>
        {coordinates && (
          <div className="location-label-block-map">
            <Map
              containerStyle={containerStyle}
              zoom={12}
              center={toLatLgnLiteral(coordinates)}
              zoomControl
            />
          </div>
        )}
      </Block>
    );
  };

  startChatHandle = async (message: string) => {
    const { item } = this.props;
    if (!item?.sellerPhone) {
      this.$f7?.dialog.alert("Seller phone not found!");
      return;
    }
    this.props.startChatWithProduct(item, message);
    analytics.startChatWithSeller(this.props.profile, this.props.item);
    this.setState({
      startChatPopupOpened: false,
      startChatSheetOpened: false,
    });
  };

  startChatMessage = () => {
    const { item, t } = this.props;
    const productUrl = createProductUrl(item.uid);
    return t("ChatMessageProduct", { productUrl });
  };

  reportAdvertisementHandle = () => {
    this.setState({ contactSupportPopupOpened: true });
  };

  handleOpenAddToCartSheetClick = () =>
    this.setState({
      ...addToCartItemDefaultState,
      addToCartStepperInit: true,
      addToCartSheetOpened: true,
    });

  handleAddToCartClick = () => {
    this.setState({ addToCartSheetOpened: false });
  };

  renderBottomImageSwitcher = (imagesItems: any[]) => {
    if (imagesItems.length === 1) return null;

    return (
      <Block className="image-switcher">
        {imagesItems.slice(0, 5).map((item, i) => {
          return (
            <div
              key={i.toString()}
              className="image-switcher-item"
              onMouseOver={() =>
                this.setState({
                  imageHoveredItemImageSwitcher: i,
                })
              }
            >
              {item.videoId ? (
                <i className="icon material-icons">play_circle_filled</i>
              ) : (
                <img src={item.small} />
              )}
            </div>
          );
        })}
      </Block>
    );
  };

  renderShippingInfo() {
    const { item, country, t } = this.props;

    if (item.type === "S") {
      return null;
    }

    if (!item.shippingAllowed && !item.pickupAllowed) {
      return null;
    }

    let text = "";
    let countryCode = country ? country.code : "_______";
    let deliveryMethods = item.deliveryMethods || [];
    let byCountry = deliveryMethods.filter((m) => {
      return m.options
        ? !!m.options.filter(
          (o) =>
            (o.countries &&
              !!o.countries.filter((c) => c.code === countryCode).length) ||
            (o.pickupAddress && o.pickupAddress.countryCode === countryCode)
        ).length
        : false;
    });

    let pickupPoints = byCountry.filter(
      (m) => m.type === DeliveryMethod.TypeEnum.PICKUP
    );

    if (!deliveryMethods.length) {
      text = t("Contact the seller to discuss shipping details");
    } else if (
      deliveryMethods.length &&
      !byCountry.length &&
      !pickupPoints.length
    ) {
      text = t("No delivery to selected country");
    } else {
      let lowestPriceMethod = byCountry
        .filter((m) => m.type === DeliveryMethod.TypeEnum.DELIVERY)
        .sort((a, b) => {
          return (
            (a.options && a.options.length
              ? a.options
                .filter(
                  (o) =>
                    o.countries.filter((c) => c.code === countryCode).length
                )
                .sort((a, b) => a.price - b.price)[0].price
              : 0) -
            (b.options && b.options.length
              ? b.options
                .filter(
                  (o) =>
                    o.countries.filter((c) => c.code === countryCode).length
                )
                .sort((a, b) => a.price - b.price)[0].price
              : 0)
          );
        })[0];

      if (
        lowestPriceMethod &&
        lowestPriceMethod.options &&
        lowestPriceMethod.options.length
      ) {
        let lowestPriceOption = lowestPriceMethod.options[0];
        text = `+ ${t("Shipping price", {
          price: formatPrice(lowestPriceOption.price, item.currencyCode),
        })}`;
      } else if (pickupPoints[0]) {
        let point = pickupPoints[0];
        if (point.options && point.options[0]) {
          let option = point.options[0];
          text = Object.keys(option.pickupAddress)
            .filter((key) =>
              "postalCode, countryCode, city, firstAddressLine".includes(key)
            )
            .map((key) => option.pickupAddress[key])
            .filter((field) => !!field)
            .join(", ");
        }
      }
    }

    return <div className="shipping-info">{text}</div>;
  }

  renderQtyPopover = () => {
    const {
      state: { sheetPopoverIsOpen, addToCartSheetItemCount },
      props: { 
        item: {quantity, type}, 
        t 
      },
    } = this;

    const isInfinityType: boolean = type === 'I';
    const isServiceType: boolean = type === 'S';
    const isPType: boolean = type === 'P';
    const itemCount = isServiceType ? 10 : isInfinityType ? 999 : quantity;

    return !(quantity < 1 && isPType) && (
      <div className="popover-qty">
        <PopoverButton
          value={addToCartSheetItemCount}
          text={`Qty`}
          popoverOpen=".popover-qty-menu"
          onChange={this.handleSheetChange}
          onClick={() => this.setState({ sheetPopoverIsOpen: true })}
          quantity={quantity}
          itemType={type}
        />
        <PopoverList
          text={`${t("Available")}: ${itemCount}`}
          className="popover-qty-menu"
          popoverQty={itemCount}
          selectedValue={addToCartSheetItemCount}
          onChange={this.handleSheetChange}
          popoverIsOpen={sheetPopoverIsOpen}
          onClick={() => this.setState({ sheetPopoverIsOpen: false })}
          itemType={type}
        />
      </div>
    )
  }

  setSliderZoom = (value) => {
    this.setState({ sliderZoom: value });
  }

  handleShareProduct = () => {
    const { item, profile } = this.props;
    analytics.shareProduct(profile, item);
    this.props.share(item.name, getProductDetailsLink(item.uid));
  };

  handleTranslate = () => {
    this.setState((prevState) => {
      return {
        ...prevState,
        translated: !prevState.translated
      }
    })
  }

  renderHashTags = () => {
    const { item, t } = this.props
    const tags = item && item.hashtags && item.hashtags.split(',');

    return tags ? (
      <>
        <BlockTitle className="tags-title">{t("Tags")}</BlockTitle>
        <Block className="product-details-left-side-tags">
          {tags.map((item, index) => (
            <div className="tag-item" key={index}>
              <Chip text={`#${item}`} />
            </div>
          ))}
        </Block>
      </>
    ) : null
  }

  handleSheetChange = value => this.setState({ addToCartSheetItemCount: value });

  renderProductDetails = () => {
    const {t, entryDirect, categoriesClassificator } = this.props;
    const { imageHoveredItemImageSwitcher, sliderZoom, translated } = this.state;
    let { item } = this.props;

    if (!item) return <Block></Block>;
    
    if (!translated && item.localizations) {
      const {originalLanguageCode, category, localizations} = item;
      
      const originalLanguage = localizations.find(item => item.language === originalLanguageCode);

      item = { 
        ...item, 
        ...originalLanguage,
        category
      };
    }
        
    const { video, purchasable, category, price, discountedPrice, currencyCode, name, productDate, description, sellerUid, localizations } = item;
    let imagesItems = this.getProductImagesSlides();

    if (video) {
      /* Video must be first */
      imagesItems = [
        { videoId: video.id, videoType: video.type.toString() },
        ...imagesItems,
      ];
    }

    // TODO
    // @ts-ignore
    const isPurchasableItem = purchasable;

    const handleClickZoomSlider = (e) => {
      const { className } = e.target;

      if (className.indexOf('swiper-button-next') !== -1 || className.indexOf('swiper-button-prev') !== -1) {
        return;
      }

      !sliderZoom && this.setSliderZoom(true);
    }

    const currentCategory = getCategory(categoriesClassificator, category);
    const subRoutes = getSubRoutes(currentCategory);
    const mainCategory = subRoutes && subRoutes[0];
    const subCategory = subRoutes && subRoutes[1];

    return (
      <Row className="product-details-row">
        <Col width="100" className="pure-hidden-xs">
          <F7Navbar
            title=""
            backLink={entryDirect ? false : t("Back").toString()}
            noHairline
            noShadow
          >
            <Breadcrumb categoryCode={category} handleBackToMain={this.onClickLogoLink} />
          </F7Navbar>
        </Col>
        <Col className="product-details-left-side" width="60">
          <Block className="no-padding images product-details-left-side-slider">
            <div className="product-details-left-side-slider-container" onClick={handleClickZoomSlider}>
              <Slider
                className="slider-images"
                slides={imagesItems}
                type={SliderType.images}
                videoPlayOnClick={(videoId, videoType) => {
                  this.setState({
                    playVideoSheetOpened: true,
                    videoId,
                    videoType,
                  });
                }}
                slideIndex={imageHoveredItemImageSwitcher}
                sliderZoom={sliderZoom}
                changeActiveIndex={(activeSlideIndex) => this.setState({ activeSlideIndex })}
              />
              <div className="pure-hidden-xs">
                {this.renderBottomImageSwitcher(imagesItems)}
              </div>
            </div>
          </Block>
          <div className="product-details-left-side-content">
            {/* for small screen*/}
            {this.props.resizeEvent.width <= 768 && <div className="product-details-mobile">
              {localizations && <Translation translated={translated} onClick={this.handleTranslate} />}
              <Block className="product-details-mobile-add ">
                <BlockTitle className="product-title"><h2>{name}</h2></BlockTitle>
                <div className="buttons-container">
                  <AddWishButton
                    onClick={this.addToWish}
                    active={this.isAddedToWishList()}
                  />
                </div>
              </Block>

              <Block className="product-details-mobile-price">
                <Price
                  price={price}
                  discountedPrice={discountedPrice}
                  currencyCode={currencyCode}
                />

                {this.renderShippingInfo()}

                <div className="toolbar-container product-details-mobile-actions">
                  {this.renderQtyPopover()}
                  {this.renderAddToCartButton()}
                </div>
              </Block>

              <Block className="product-details-mobile-info">
                <div className="product-details-mobile-info-content">
                  <p><span>{t("Posted")}</span> {formatDate(item.productDate.toString())}</p>
                  {mainCategory && <p><span>{t('Category')}</span> <a>{mainCategory.categoryName}</a></p>}
                  {subCategory && <p><span>{t('Subcategory')}</span> <a>{subCategory.categoryName}</a></p>}
                </div>
              </Block>
            </div>}
            <Block className="product-details-left-side-characteristics">
              <div>
                <TechnicalDetails product={item} />
              </div>
            </Block>

            <Block className="product-details-left-side-delivery">
              {this.renderDeliveryInformationBlock()}
            </Block>

            {item.description && (
              <>
                <BlockTitle>{t("Description")}</BlockTitle>
                <Block className="product-details-left-side-description">
                  <div>
                    <DescriptionDetails
                      text={description}
                      moreLinkText={t("More")}
                      lessLinkText={t("Less")}
                      textShow={400}
                    />
                  </div>
                </Block>
              </>
            )}

            {this.renderHashTags()}

            {this.renderAddressBlock()}
            {/* for large screen */}
            {this.props.resizeEvent.width > 768 && <Block className="product-details-left-side-report">
              <p>{t("Do you have any complaints about this product?")}</p>
              <Button
                round
                fill
                className="report-advertisement-button"
                onClick={this.reportAdvertisementHandle}
              >
                {t("Report Advertisement")}
              </Button>
            </Block>}
          </div>
        </Col>
        <Col width="35" className="product-details-right-side">
          {/* for large screen */}
          {this.props.resizeEvent.width > 768 && <>
            {localizations && <Translation translated={translated} onClick={this.handleTranslate} />}
            <Block className="product-details-right-add-share">
              <BlockTitle className="product-title"><h2>{name}</h2></BlockTitle>
              <div className="buttons-container">
                <AddWishButton
                  onClick={this.addToWish}
                  active={this.isAddedToWishList()}
                  text={t(this.isAddedToWishList() ? "Added to wishlist" : "Add to wishlist")}
                />
                <ShareButton
                  onClick={this.handleShareProduct}
                  text={t("Share")}
                />
              </div>
            </Block>

            <Block className="product-details-right-price">
              <Price
                price={price}
                discountedPrice={discountedPrice}
                currencyCode={currencyCode}
              />

              {this.renderShippingInfo()}

              <div className="toolbar-container product-details-right-actions">
                {this.renderQtyPopover()}
                {this.renderAddToCartButton()}
              </div>
            </Block>

            <Block className="product-details-right-info">
              <div className="product-details-right-info-content">
                <p><span>{t("Posted")}</span> {formatDate(productDate.toString())}</p>
                {mainCategory && <p><span>{t('Category')}</span> <a>{mainCategory.categoryName}</a></p>}
                {subCategory && <p><span>{t('Subcategory')}</span> <a>{subCategory.categoryName}</a></p>}
              </div>
            </Block>
          </>}

          <Block className="product-details-right-seller">
            {this.props.resizeEvent.width <= 768 && <BlockTitle>{t("Seller")}</BlockTitle>}
            <div className="product-details-right-seller-block">
              <div className="product-details-right-seller-block-info">
                <Icon material="account_circle" />
                <div>
                  <p>{sellerUid}</p>
                  <span>{t("Last seen recently")}</span>
                </div>
              </div>
              {!this.props.chatReducer.loading && <div
                className="product-details-right-seller-block-chat"
                onClick={() => this.props.resizeEvent.width > 629 ? this.setState({ startChatPopupOpened: true }) : this.setState({ startChatSheetOpened: true })}
              >
                <Icon f7="chat_bubble_text" />
              </div>}
            </div>
          </Block>
          {/* for small screen */}
          {this.props.resizeEvent.width <= 768 && <Block className="product-details-left-side-report mobile">
            <p>{t("Do you have any complaints about this product?")}</p>
            <Button
              round
              fill
              className="report-advertisement-button"
              onClick={this.reportAdvertisementHandle}
            >
              {t(`${this.props.resizeEvent.width > 567 ? 'Report Advertisement' : 'Report Ad'}`)}
            </Button>
          </Block>}
          {sliderZoom && ReactDOM.createPortal(
            <div className="slider-zoom-modal">
              <div className="slider-zoom-modal-container">
                <div className="slider-zoom-modal-container-head" onClick={() => this.setSliderZoom(false)}>
                  <span className="slider-zoom-modal-container-head-window">{item.name}</span>
                  <Icon material="close" className="slider-zoom-modal-container-head-close" />
                  <span className="slider-zoom-modal-container-head-mobile">{this.state.activeSlideIndex} from {imagesItems.length}</span>
                  <Icon material="arrow_back" className="slider-zoom-modal-container-head-back" />
                </div>
                <Slider
                  className="slider-images"
                  slides={imagesItems}
                  type={SliderType.images}
                  videoPlayOnClick={(videoId, videoType) => {
                    this.setState({
                      playVideoSheetOpened: true,
                      videoId,
                      videoType,
                    });
                  }}
                  slideIndex={imageHoveredItemImageSwitcher}
                  sliderZoom={sliderZoom}
                  changeActiveIndex={(activeSlideIndex) => this.setState({ activeSlideIndex })}
                />
              </div>
            </div>,
            document.getElementById('app')
          )}
        </Col>
      </Row>
    );
  };

  renderDeliveryInformationBlock() {
    const { item, t } = this.props;
    var deliveryInfoKeys: (keyof Pick<
      typeof item,
      "shippingAllowed" | "pickupAllowed" | "returnAccepted"
    >)[] = ["shippingAllowed", "returnAccepted", "pickupAllowed"];

    const items = deliveryInfoKeys
      .map((key) => {
        return (
          item[key] && (
            <ListItem key={key}>
              <div slot="media">
                {item[key] && <Icon material="checkmark_alt" />}
              </div>
              <div slot="title">{t(deliveryKeysTitles[key])}</div>
            </ListItem>
          )
        );
      })
      .filter((item) => !!item);

    return (
      !!items.length && (
        <>
          <BlockTitle>
            {t("Delivery Information")}
          </BlockTitle>
          <List noChevron noHairlines mediaList className="delivery-info">
            {items}
          </List>
        </>
      )
    );
  }

  renderProfileLink = () => {
    const { profile } = this.props;
    return (
      <ProfileLink
        key="profile_link"
        profile={profile}
        href="#"
        onClick={this.handleClickProfileLink}
      />
    );
  };

  handleClickProfileLink = () => {
    this.setState({ profilePopoverOpened: true });
  };

  renderAddToCartButton() {
    const { item, t, resizeEvent } = this.props;
    const { addToCartSheetItemCount } = this.state;
    if (!item) return null;
    // TODO
    // @ts-ignore
    const isPurchasableItem = item.purchasable;
    const forMobile = resizeEvent.width <= 567;
    const sheetCount = addToCartSheetItemCount === 'input' ? '1' : addToCartSheetItemCount;
    const handler = forMobile ? this.handleOpenAddToCartSheetClick : () => this.addToCart(sheetCount);

    if (!isPurchasableItem) return null;

    if (forMobile) {
      return (
        <Block
          slot="fixed"
          className="add-to-cart-btn-container pure-visible-xs"
        >
          <Button large fill round onClick={handler}>
            {t("Add to cart")}
          </Button>
        </Block>
      );
    }

    return (
      <div className="add-to-cart-btn-container">
        <Button large fill round onClick={handler}>
          {t("Add to cart")}
        </Button>
      </div>
    );
  }

  onClickLogoLink = () => {
    this.$f7.searchbar.disable(".search-bar");
    this.props.chooseCategory(null);
    this.props.clearSearch();
    this.$f7router.navigate("/");
  };

  onClickOpenCategoriesMenu = () =>
    this.setState({
      categoriesMenuOpened: !this.state.categoriesMenuOpened,
    });

  searchbarEnableHandle = () => {
    this.setState({ searchBarEnable: true });
  };

  searchbarDisableHandle = () => {
    this.setState({ searchBarEnable: false }, () => {
      this.props.clearSearch();
    });
  };

  searchbarClearHandle = () => {
    this.props.clearSearch();
  };

  render() {
    const { entryDirect, t, resizeEvent, item, profile, searchedProducts } = this.props;
    const { playVideoSheetOpened, videoId, videoType } = this.state;
    const isMobile = resizeEvent.width < 769;

    return (
      <Page
        id="product_details"
        name="product-details"
        onPageAfterIn={this.pageAfterInHandle}
        subnavbar={resizeEvent.width < 769}
      >
        <Navbar
          profile={profile}
          showProfileLink={!isMobile}
          onClickProfileLink={this.handleClickProfileLink}
          onClickLogoLink={this.onClickLogoLink}
          onClickOpenCategoriesMenu={this.onClickOpenCategoriesMenu}
          openCategoriesMenuButton={this.state.categoriesMenuOpened}
          showLanguageLink={false}
          onSearchbarEnable={this.searchbarEnableHandle}
          onSearchbarDisable={this.searchbarDisableHandle}
          onSearchClickClear={this.searchbarClearHandle}
          findedProducts={this.props.searchProductsAutocomplete}
          findProductLoading={this.props.searchLoadingAutocomplete}
          onFindedProductItemClick={(uid) =>
            this.$f7router.navigate(`/product-details/${uid}/`)
          }
          showMessengerButton
          onClickGoToMessenger={this.props.goToMessenger}
          backLink={!entryDirect && isMobile ? t("Back").toString() : false}
          onReportAdvertisementClick={this.reportAdvertisementHandle}
          slot="fixed"
          showShare={true}
        />

        {this.renderProductDetails()}

        {/* <Row className="catalog-block">
          {searchedProducts.length && <BlockTitle>{t("Seller products")}</BlockTitle>}
          <Catalog
            items={searchedProducts}
            addToWish={this.props.addToWish}
            showFeaturesHiglight
          />
        </Row> */}

        <Popup
          id="play_video_sheet"
          className="play-video-popup"
          swipeToClose
          backdrop
          opened={playVideoSheetOpened && resizeEvent.width >= 768}
          onPopupClosed={() => {
            if (resizeEvent.width >= 768) {
              this.setState({
                playVideoSheetOpened: false,
                videoId: null,
                videoType: null,
              });
            }
          }}
        >
          <Page>
            <F7Navbar title="" bgColor="black">
              <NavRight>
                <Link popupClose>{t("Close")}</Link>
              </NavRight>
            </F7Navbar>
            <VideoPlayer videoId={videoId} videoType={videoType} />
          </Page>
        </Popup>

        <Sheet
          id="play_video_sheet"
          swipeToClose
          backdrop
          opened={playVideoSheetOpened && resizeEvent.width < 768}
          onSheetClosed={() => {
            if (resizeEvent.width < 768) {
              this.setState({
                playVideoSheetOpened: false,
                videoId: null,
                videoType: null,
              });
            }
          }}
        >
          <VideoPlayer videoId={videoId} videoType={videoType} />
        </Sheet>

        <div slot="fixed">
          <CategoriesMenuDesktop
            className="pure-hidden-xs"
            opened={this.state.categoriesMenuOpened}
          />
        </div>

        <ProfilePopover
          profile={this.props.profile}
          backdrop={false}
          opened={this.state.profilePopoverOpened}
          target=".profile-link"
          onPopoverClosed={() => this.setState({ profilePopoverOpened: false })}
          onAboutClick={() => this.setState({ aboutPopupOpened: true })}
          onVerifyClick={() => this.setState({ verifyAccountPopupOpened: true })}
          t={t}
          slot="fixed"
        />

        <AddToCartSheetPage
          opened={this.state.addToCartSheetOpened}
          onSheetClosed={() => this.setState({ addToCartSheetOpened: false })}
          item={item}
          onAddToCartClick={(count) => this.addToCart(count, true)}
        />

        {item && (
          <ContactSupportPopup
            profile={profile}
            category="Product"
            product={item}
            backdrop={false}
            opened={this.state.contactSupportPopupOpened}
            onPopupClosed={() =>
              this.setState({ contactSupportPopupOpened: false })
            }
          />
        )}

        {item && (
          <Sheet
            id="start_chat_sheet"
            swipeToClose
            backdrop
            opened={this.state.startChatSheetOpened}
            slot="fixed"
            style={{ height: "auto" }}
          >
            <StartChat
              opened={this.state.startChatSheetOpened}
              productUid={item.uid}
              onClose={() => {
                this.setState({
                  startChatSheetOpened: false,
                });
              }}
              onStartChat={(message) => this.startChatHandle(message)}
            />
          </Sheet>
        )}

        {item && (
          <Popup
            id="start_chat_popup"
            backdrop
            opened={this.state.startChatPopupOpened}
            slot="fixed"
          >
            <StartChat
              opened={this.state.startChatPopupOpened}
              productUid={item.uid}
              onClose={() => {
                this.setState({
                  startChatPopupOpened: false,
                });
              }}
              onStartChat={(message) => this.startChatHandle(message)}
            />
          </Popup>
        )}

        <AboutPopup
          profile={profile}
          backdrop={true}
          opened={this.state.aboutPopupOpened}
          onPopupClosed={() => this.setState({ aboutPopupOpened: false })}
          onContactSupportClick={() =>
            this.setState({ contactSupportPopupOpened: true })
          }
        />

        <VerifyAccountPopup
          t={t}
          backdrop={true}
          opened={this.state.verifyAccountPopupOpened}
          onPopupClosed={() =>
            this.setState({ verifyAccountPopupOpened: false })
          }
        />
      </Page>
    );
  }
}

const mapStateToProps = (state: IApplicationStore, props: Props) => {
  const { uid } = props;
  const {
    productDetails,
    productDetailsLoading,
    productDetailsLoadingError,
  } = state.productReducer;

  let item = state.productReducer.products.filter(
    (item) => item.uid === uid
  )[0];
  if (!item) {
    const { productTypeGroups } = state.productReducer;
    const group = productTypeGroups.filter(
      (item) => item.products.filter((item) => item.uid === uid).length
    )[0];
    if (group) {
      item = group.products.filter((item) => item.uid === uid)[0];
    }
  }

  if (!item) {
    const { productsWishList } = state.productReducer;
    item = productsWishList.filter((item) => item.uid === uid)[0];
  }

  if (!item) {
    const { products } = state.allGoodsReducer;
    item = products.filter((item) => item.uid === uid)[0];
  }

  return {
    productDetailsLoading,
    productDetailsLoadingError,
    item: productDetails && productDetails.uid === uid ? productDetails : item,
    wishList: state.productReducer.productsWishList,
    updatingProfile: state.profileReducer.updating,
    profile: getProfile(state),
    entryDirect: entryPageNameEqual("product-details", state),
    resizeEvent: state.rootReducer.resizeEvent,
    language: state.rootReducer.language,
    countryClassificator: state.classificatorReducer.countryClassificator,
    country:
      state.customerLocationReducer.country || getProfile({ ...state }).country,
    searchedProducts: state.productReducer.products || [],
  };
};

const mapDispatchToProps = (dispatch: any) => ({
  addToWish: (uid?: string) => dispatch(addToWishList(uid)),
  loadProductDetail: (uid: string) => dispatch(loadProductDetail(uid)),
  loadProductWishList: () => dispatch(loadProductWishList()),
  goToMessenger: () => dispatch(goToMessenger()),
  chooseCategory: (catid?: string) => dispatch(chooseCategory(catid)),
  clearSearch: () => dispatch(searchClear()),
  search: (searchParams: ISearchParams) => dispatch(searchProducts(searchParams)),
});

export default compose(
  withTranslation(),
  connectFilter,
  connectCategories,
  connectChat,
  connectShare,
  connectCart,
  connect(mapStateToProps, mapDispatchToProps),
  connectSearch,
  connectCategoriesClassificator
)(ProductDetailsPage);

// Helpers
const entryPageNameEqual = (pageName: string, store: IApplicationStore) => {
  return store.rootReducer.entryPageName === pageName;
};
