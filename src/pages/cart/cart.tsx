import React from "react";
import {
  Page,
  Block,
  Navbar,
  List,
  ListItem,
  Icon,
  Link,
  PageContent,
  BlockTitle,
  Popup,
  NavRight,
  Searchbar,
  NavLeft,
  NavTitle,
  Row,
  Col,
} from "framework7-react";
import { WithTranslation, withTranslation } from "react-i18next";
import { compose } from "redux";
import { IApplicationStore, ResizeEvent } from "../../store/rootReducer";
import { connect } from "react-redux";
import connectCart, { ICartProps } from "../../store/connectCart";
import "./style.less";
import { Button } from "../../components/ThemedButton";
import { formatPrice } from "../../utils";
import Dom7 from "dom7";
import { Sheet } from "../../components/Sheet";
import { addToWishList } from "../../actions/productActions";
import DeliveryMethodsSheet from "./delivery-methods__sheet";
import { Country } from "../../types/commonapi";
import { ISessionState, Profile } from "../../reducers/sessionReducer";
import { LoginDesktopPopup } from "../../components/LoginDesktop";
import { RegisterDesktopPopup } from "../../components/RegisterDesktop";
import { IClassificator } from "../../reducers/classificatorReducer";
import CartProductList from "../../components/cart-product-list";
import { IProduct } from "../../reducers/productReducer";
import { selectForPurchaseBySeller } from "../../actions/cartActions";
import { chain } from "lodash";

const deliveryTitlesTypes = {
  free: "Free",
  fast_fixed: "Shipping",
};

type Props = Page.Props &
  ICartProps & {
    addToWish?(uid?: string): void;
    countryClassificator: Country[];
    sessionState: ISessionState;
    resizeEvent?: ResizeEvent;
    deliveryMethods: {
      title: string;
      description: string;
      uid: string;
    }[];
    wishList?: IProduct[];
    selectForPurchaseBySeller?(sellerUid: string): void;
  };

class CartPage extends React.Component<
  Props & WithTranslation,
  {
    groupInfoMoreSheetOpened?: boolean;
    groupInfoKey?: string;
    itemInfoMoreSheetOpened?: boolean;
    itemInfoId?: string;
    deliveryMethodsSheetOpened?: boolean;
    chooseCountryPopupOpened?: boolean;
    choosedCountry?: string;
    loginPopupOpened?: boolean;
    registerPopupOpened?: boolean;
  }
> {
  constructor(props: Readonly<Props & WithTranslation>) {
    super(props);
    this.state = {
      groupInfoMoreSheetOpened: false,
      itemInfoMoreSheetOpened: false,
      deliveryMethodsSheetOpened: false,
      chooseCountryPopupOpened: false,
      choosedCountry: props.countryClassificator[0].code,
    };
  }

  pageInitHandle = () => {
    Dom7(".cart-catalog .item-link").on("taphold", (e) => {
      const $itemEl = Dom7(e.target).closest("li");
      if ($itemEl) {
        const id = $itemEl.attr("id");
        this.setState({ itemInfoMoreSheetOpened: true, itemInfoId: id });
      }
    });

    Dom7(".cart-product-list .product-item").on(
      "taphold",
      this.itemLinkTapholdListener
    );
  };

  componentDidUpdate(prevProps: Props) {}

  componentWillUnmount() {
    Dom7(".cart-product-list .product-item").off(
      "taphold",
      this.itemLinkTapholdListener
    );
  }

  itemLinkTapholdListener = (ev: any) => {
    const $el = Dom7(ev.target).closest("li")[0];
    if ($el) {
      const id = $el.getAttribute("id");
      if (id) {
        this.setState({
          itemInfoId: id,
          itemInfoMoreSheetOpened: true,
        });
      }
    }
  };

  getCartTitle = () => {
    const { t, itemsCart } = this.props;
    return `${t("Cart")} ${
      itemsCart.length > 0 ? `(${itemsCart.length})` : ""
    }`;
  };

  renderCartTotal = () => {
    const { t, itemsCart } = this.props;
    const { currencyCode } = itemsCart[0].product;

    return (
      <div className="cart-total">
        <span className="label">{t("Total")}:</span>
        <span>{formatPrice(this.props.totalCart, currencyCode)}</span>
      </div>
    );
  };

  handleOnChangeStepper = (item: any, value: number) => {
    if (value === 0) {
      this.props.cartRemove(item, 1);
      return;
    }

    this.props.cartUpdate(item, value);
  };

  handlePayOnlyThisSellerClick = () => {
    const { groupInfoKey } = this.state;
    this.setState({ groupInfoMoreSheetOpened: false });
    this.props.selectForPurchaseBySeller(groupInfoKey);
    this.$f7router.navigate("checkout/");
  };

  handleRemoveGoodsFromThisSeller = () => {
    const { groupInfoKey } = this.state;
    this.setState({ groupInfoMoreSheetOpened: false });
    setTimeout(() => {
      this.props.cartRemoveBySeller(groupInfoKey);
    }, 380);
  };

  handleMoveToWishListClick = () => {
    const { itemInfoId } = this.state;
    const { itemsCart } = this.props;
    this.setState({ itemInfoMoreSheetOpened: false }, () => {
      setTimeout(() => {
        this.props.addToWish(itemInfoId);
        const itemInfo = itemsCart.filter(
          (item) => item.product.uid === itemInfoId
        )[0];
        this.props.cartRemove(itemInfo.product, itemInfo.count);
      }, 380);
    });
  };

  handleRemoveGoodsClick = () => {
    const { itemInfoId } = this.state;
    this.setState({ itemInfoMoreSheetOpened: false }, () => {
      setTimeout(() => {
        const itemFound = this.props.itemsCart.filter(
          (item) => item.product.uid === itemInfoId
        )[0];
        if (itemFound) {
          this.props.cartRemove(itemFound.product, itemFound.count);
        }
      }, 380);
    });
  };

  handleDeleteItemClick = (product: IProduct) => {
    const { t } = this.props;
    const itemFound = this.props.itemsCart.filter(
      (item) => item.product.uid === product.uid
    )[0];
    if (itemFound) {
      this.$f7.dialog.confirm(t("Remove") + "?", t("Remove"), () => {
        this.props.cartRemove(itemFound.product, itemFound.count);
      });
    }
  };

  handleDeliveryMethodsClick = () => {
    this.setState({ deliveryMethodsSheetOpened: true });
  };

  goToCheckout = () => {
    const {
      sessionState: { logged },
      resizeEvent,
    } = this.props;

    if (!logged) {
      if (resizeEvent && resizeEvent.width > 567) {
        this.setState({ loginPopupOpened: true });
      } else {
        this.$f7router.navigate("/login/");
      }
      return;
    }

    this.$f7router.navigate("checkout/");
  };

  ifCartHasMultipleManufacturer = (): boolean => {
    const { itemsCart } = this.props;
    if (!itemsCart.length) return false;
    return (
      chain(itemsCart)
        .groupBy((item) => item.product.sellerUid)
        .map((value, key) => {
          return { key, items: value };
        })
        .value().length > 1
    );
  };

  render() {
    const { t, itemsCart, totalCart, resizeEvent } = this.props;
    const currencyCode = itemsCart.length
      ? itemsCart[0].product.currencyCode
      : "";

    const ifCartHasMultipleManufacturer = this.ifCartHasMultipleManufacturer();

    return (
      <Page id="cart_page" name="cart-page" onPageInit={this.pageInitHandle}>
        <Navbar
          title={this.getCartTitle()}
          backLink={t("Back").toString()}
          noHairline
          noShadow
        />
        <div className="block-title block-title-medium pure-hidden-xs">
          {t("Shopping cart") + ` (${itemsCart.length})`}
        </div>
        <Row>
          <Col width="100" medium="70">
            <CartProductList
              estimatedDeliveryTimeLabel={t("Estimated delivery time period", {
                period: "14-23",
              })}
              freeDeliveryLabel={t("Free delivery")}
              items={itemsCart}
              onMoreLinkClick={(group) => {
                this.setState({
                  groupInfoMoreSheetOpened: true,
                  groupInfoKey: group,
                });
              }}
              onProductCountStepperChange={this.handleOnChangeStepper}
              onSelectDeliveryMethodClick={
                () => {} /*this.handleDeliveryMethodsClick*/
              }
              onAddToWishListClick={(product) =>
                this.props.addToWish(product.uid)
              }
              wishList={this.props.wishList}
              onDeleteItemClick={(product) =>
                this.handleDeleteItemClick(product)
              }
              showActions
              onPayOnlyThisSellerClick={(sellerUid) => {
                this.props.selectForPurchaseBySeller(sellerUid);
                this.$f7router.navigate("checkout/");
              }}
              payOnlyThisSellerLabel={t("Pay only this Seller")}
              isItemLink={resizeEvent.isXS}
            />
          </Col>
          <Col
            width="100"
            medium="30"
            className="pure-hidden-xs"
            style={{ position: "relative" }}
          >
            <div className="order-summary-container">
              <BlockTitle medium>{t("Order summary")}</BlockTitle>
              <List
                className="order-summary"
                noHairlines
                noChevron
                noHairlinesBetween
              >
                <ListItem
                  title={t("Items") + ` (${itemsCart.length})`}
                  after={formatPrice(totalCart, currencyCode)}
                ></ListItem>
                <ListItem
                  title={t("Delivery").toString()}
                  after={formatPrice(0, currencyCode)}
                ></ListItem>
                <ListItem
                  title={t("Total").toString()}
                  after={formatPrice(totalCart, currencyCode)}
                  className="total"
                ></ListItem>
              </List>
              <Block>
                {/* TODO move to component */}
                <Button
                  round
                  large
                  fill
                  onClick={this.goToCheckout}
                  disabled={ifCartHasMultipleManufacturer || this.props.itemsCart.length === 0}
                >
                  <span>{t("Go to checkout")}</span>
                </Button>
                {ifCartHasMultipleManufacturer && (
                  <Block className="warning">
                    {t(
                      "We are sorry, but you can proceed with checkout for one seller only. Please select the seller and continue with checkout"
                    )}
                  </Block>
                )}
              </Block>
            </div>
          </Col>
        </Row>

        {itemsCart.length && (
          <Block
            slot="fixed"
            className="go-to-checkout-btn-container pure-visible-xs"
          >
            {/* TODO move to component */}
            <Button
              className="go-to-checkout-btn"
              round
              large
              fill
              onClick={this.goToCheckout}
              disabled={ifCartHasMultipleManufacturer || this.props.itemsCart.length === 0}
            >
              <span>{this.renderCartTotal()}</span>
              <span>{t("Go to checkout")}</span>
            </Button>
            {ifCartHasMultipleManufacturer && (
              <Block className="warning">
                {t(
                  "We are sorry, but you can proceed with checkout for one seller only. Please select the seller and continue with checkout"
                )}
              </Block>
            )}
          </Block>
        )}

        {/* TODO move to component */}
        <Sheet
          id="group_info_more_sheet"
          swipeToClose
          backdrop
          opened={this.state.groupInfoMoreSheetOpened}
          onSheetClosed={() => {
            this.setState({
              groupInfoMoreSheetOpened: false,
            });
          }}
          slot="fixed"
          style={{ height: "auto" }}
        >
          <List noHairlines noChevron noHairlinesBetween>
            <ListItem
              link="#"
              title={t("Pay only this Seller").toString()}
              onClick={this.handlePayOnlyThisSellerClick}
            >
              <Icon slot="media" material="account_balance_wallet" />
            </ListItem>
            <ListItem
              link="#"
              title={t("Remove goods from this seller").toString()}
              onClick={this.handleRemoveGoodsFromThisSeller}
            >
              <Icon slot="media" material="delete_outline" />
            </ListItem>
          </List>
        </Sheet>

        {/* TODO move to component */}
        <Sheet
          id="item_info_more_sheet"
          swipeToClose
          backdrop
          opened={this.state.itemInfoMoreSheetOpened}
          onSheetClosed={() => {
            this.setState({
              itemInfoMoreSheetOpened: false,
            });
          }}
          slot="fixed"
          style={{ height: "auto" }}
        >
          <List noHairlines noChevron noHairlinesBetween>
            <ListItem
              link="#"
              title={t("Move to Wish List").toString()}
              onClick={this.handleMoveToWishListClick}
            >
              <Icon slot="media" material="favorite_border" />
            </ListItem>
            <ListItem
              link="#"
              title={t("Remove Goods").toString()}
              onClick={this.handleRemoveGoodsClick}
            >
              <Icon slot="media" material="delete_outline" />
            </ListItem>
          </List>
        </Sheet>

        <DeliveryMethodsSheet
          opened={this.state.deliveryMethodsSheetOpened}
          onSheetClosed={() =>
            this.setState({ deliveryMethodsSheetOpened: false })
          }
          items={[
            {
              title: t("Free delivery"),
              description: t("Estimated delivery time period", {
                period: "14-23",
              }),
              uid: "-",
            },
          ]}
          selectedItemUid={"-"}
          choosedCountry={
            this.props.countryClassificator.filter(
              (item) => item.code === this.state.choosedCountry
            )[0].name
          }
          onChooseCountryClick={() => {
            this.setState({ deliveryMethodsSheetOpened: false });
            setTimeout(() => {
              this.setState({ chooseCountryPopupOpened: true });
            }, 380);
          }}
          onApplyClick={() => {
            this.setState({ deliveryMethodsSheetOpened: false });
          }}
          slot="fixed"
        />

        {/* TODO move to component */}
        <Popup
          id="choose_country__popup"
          className="choose-country__popup"
          opened={this.state.chooseCountryPopupOpened}
          onPopupClosed={() =>
            this.setState({ chooseCountryPopupOpened: false })
          }
        >
          <Navbar backLink={false} noHairline noShadow sliding>
            <NavLeft>
              <Link iconOnly onClick={() => this.$f7.popup.close()}>
                <Icon className="icon-back" />
              </Link>
            </NavLeft>
            <NavTitle>{t("Choose country")}</NavTitle>
            <NavRight>
              <Link
                searchbarEnable=".searchbar-demo"
                iconIos="f7:search"
                iconAurora="f7:search"
                iconMd="material:search"
              ></Link>
            </NavRight>
            <Searchbar
              className="searchbar-demo"
              expandable
              searchContainer=".search-list"
              searchIn=".item-title"
              disableButton={!this.$theme.aurora}
            ></Searchbar>
          </Navbar>
          <PageContent style={{ paddingTop: 0 }}>
            <List
              className="searchbar-not-found"
              noChevron
              noHairlines
              noHairlinesBetween
            >
              <ListItem title={t("Nothing found").toString()} />
            </List>
            {/* TODO replace with VirtualList! */}
            <List
              className="search-list searchbar-found"
              noChevron
              noHairlines
              noHairlinesBetween
            >
              {this.props.countryClassificator.map((val) => (
                <ListItem
                  link
                  key={val.code}
                  title={val.name}
                  onClick={() => {
                    this.$f7.popup.close();
                    this.setState({ choosedCountry: val.code });
                  }}
                />
              ))}
            </List>
          </PageContent>
        </Popup>

        <LoginDesktopPopup
          className="pure-hidden-xs"
          opened={this.state.loginPopupOpened}
          onPopupClosed={() => {
            const {
              sessionState: { logged },
            } = this.props;
            this.setState({ loginPopupOpened: false });

            if (logged && this.props.itemsCart.length === 0) {
              this.goToCheckout();
            }
          }}
          t={t}
          slot="fixed"
          onRegister={() => {
            this.setState({ loginPopupOpened: false });
            this.setState({ registerPopupOpened: true });
          }}
        />

        <RegisterDesktopPopup
          className="pure-hidden-xs"
          opened={this.state.registerPopupOpened}
          onPopupClosed={() => this.setState({ registerPopupOpened: false })}
          t={t}
          slot="fixed"
        />
      </Page>
    );
  }
}

const buildDeliveryMethods = (
  deliveryDurations: IClassificator[],
  deliveryPrices: IClassificator[]
) => {
  return Object.keys(deliveryTitlesTypes).map((key) => {
    return {
      title: deliveryTitlesTypes[key],
    };
  });
};

const mapStateToProps = (state: IApplicationStore) => ({
  countryClassificator: state.classificatorReducer.countryClassificator,
  sessionState: state.sessionReducer,
  resizeEvent: state.rootReducer.resizeEvent,
  deliveryMethods: buildDeliveryMethods(
    state.classificatorReducer.entitiesClassificators.Delivery_Duration,
    state.classificatorReducer.entitiesClassificators.Delivery_Prices
  ),
  wishList: state.productReducer.productsWishList,
});

const mapDispatchToProps = (dispatch: any) => ({
  addToWish: (uid?: string) => dispatch(addToWishList(uid)),
  selectForPurchaseBySeller: (sellerUid: string) =>
    dispatch(selectForPurchaseBySeller(sellerUid)),
});

export default compose(
  connectCart,
  connect(mapStateToProps, mapDispatchToProps),
  withTranslation()
)(CartPage);
