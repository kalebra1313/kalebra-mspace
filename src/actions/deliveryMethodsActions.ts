import {
  DeliveryMethod,
  DeliveryMethodControllerApi,
} from "../types/marketplaceapi";

export const DELIVERY_METHODS_ADD_LOADING = "DELIVERY_METHODS_ADD_LOADING";
export const DELIVERY_METHODS_ADD_SUCCESS = "DELIVERY_METHODS_ADD_SUCCESS";
export const DELIVERY_METHODS_ADD_ERROR = "DELIVERY_METHODS_ADD_ERROR";

export const DELIVERY_METHODS_ALL_LOADING = "DELIVERY_METHODS_ALL_LOADING";
export const DELIVERY_METHODS_ALL_SUCCESS = "DELIVERY_METHODS_ALL_SUCCESS";
export const DELIVERY_METHODS_ALL_ERROR = "DELIVERY_METHODS_ALL_ERROR";

export const addDeliveryMethods = (deliveryMethod: DeliveryMethod) => async (
  dispatch: any
) => {
  dispatch({ type: DELIVERY_METHODS_ADD_LOADING });
  try {
    let result;
    if (
      // Bug
      deliveryMethod.uid &&
      deliveryMethod.uid.length ===
        "6738534e-5965-427a-84fa-edab00e397a4".length /* TODO: ugly... */
    ) {
      result = await new DeliveryMethodControllerApi().updateAccountDeliveryMethodUsingPOST(
        deliveryMethod
      );
    } else {
      result = await new DeliveryMethodControllerApi().createAccountDeliveryMethodUsingPUT(
        deliveryMethod
      );
    }
    dispatch({ type: DELIVERY_METHODS_ADD_SUCCESS, item: result.body[0] });
  } catch (err) {
    let error =
      err.response && err.response.data && err.response.data.errorData
        ? err.response.data.errorData.message
        : err.toString();
    dispatch({ type: DELIVERY_METHODS_ADD_ERROR, error });
  }
};

export const getAllDeliveryMethods = () => async (dispatch: any) => {
  dispatch({ type: DELIVERY_METHODS_ALL_LOADING });
  try {
    const result = await new DeliveryMethodControllerApi().getAccountDeliveryMethodsUsingGET();
    dispatch({ type: DELIVERY_METHODS_ALL_SUCCESS, items: result.body || [] });
  } catch (err) {
    let error =
      err.response && err.response.data && err.response.data.errorData
        ? err.response.data.errorData.message
        : err.toString();
    dispatch({ type: DELIVERY_METHODS_ALL_ERROR, error });
  }
};
