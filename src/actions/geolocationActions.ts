import { detectLocation, reverseGeocoding } from "../utils";
import { changeCountry } from "./customer-location/customerLocationActions";

export const INIT_GEOLOCATION_LOADING = "INIT_GEOLOCATION_LOADING";
export const INIT_GEOLOCATION_SUCCESS = "INIT_GEOLOCATION_SUCCESS";

export const initGeolocation = () => async (dispatch: any) => {
  dispatch({ type: INIT_GEOLOCATION_LOADING });

  const data = await detectLocation();
  const result = await reverseGeocoding(data.latitude, data.longitude);
  let country = null;

  if (result && result.results && result.results.length) {
    const address_components = result.results[0].address_components;
    if (address_components && address_components.length) {
      country = {
        long_name: address_components[0].long_name,
        short_name: address_components[0].short_name,
      };
      dispatch(changeCountry(country.short_name));
    }
  }

  dispatch({
    type: INIT_GEOLOCATION_SUCCESS,
    data: { ...data, country },
  });
};
