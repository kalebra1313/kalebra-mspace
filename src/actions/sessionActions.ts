import moment from "moment";
import { client, commonapiURL } from "../axios";
import {
  CreateAccountRequest,
  ICreateAccountFormError,
  Profile,
  Token,
} from "../reducers/sessionReducer";
import { IApplicationStore } from "../store/rootReducer";
import { AccountWsControllerApi } from "../types/commonapi";
import { getAuthCodeFromURL, getQueryParameterFromURL } from "../utils";
import { loadMyCurrencies } from "./myCurrenciesActions";
import { Account } from "../types/commonapi";
import { CHANGE_APP_LANGUAGE } from "./classificatorActions";

export const CHANGE_LANGUAGE = "CHANGE_LANGUAGE";

export const SESSION_RESTORING = "SESSION_RESTORING";
export const SESSION_LOADING = "SESSION_LOADING";
export const SESSION_SUCCESS = "SESSION_SUCCESS";
export const SESSION_LOGOUT = "SESSION_LOGOUT";
export const SESSION_ERROR = "SESSION_ERROR";
export const VALIDATION_LOADING = "VALIDATION_LOADING";
export const VALIDATION_SUCCESS = "VALIDATION_SUCCESS";
export const REGISTRATION_LOADING = "REGISTRATION_LOADING";
export const REGISTRATION_SUCCESS = "REGISTRATION_SUCCESS";
export const REGISTRATION_ERROR = "REGISTRATION_ERROR";
export const UPDATE_PROFILE_LOADING = "UPDATE_PROFILE_LOADING";
export const UPDATE_PROFILE_SUCCESS = "UPDATE_PROFILE_SUCCESS";
export const UPDATE_PROFILE_ERROR = "UPDATE_PROFILE_ERROR";
export const AUTHORIZATION_CODE_LOADING = "AUTHORIZATION_CODE_LOADING";
export const AUTHORIZATION_CODE_SUCCESS = "AUTHORIZATION_CODE_SUCCESS";
export const AUTHORIZATION_CODE_ERROR = "AUTHORIZATION_CODE_ERROR";

async function getAuthorizationCode(username: string, password: string) {
  const res = await client.post(
    commonapiURL + `/auth/mobile/authorization_code`,
    { username, password }
  );
  const { code } = res.data;
  return code;
}

async function getAuthorizationCodeByAccessToken(accessToken: string) {
  const res = await client.post(
    commonapiURL + `/auth/mobile/authorization_code`,
    { accessToken }
  );
  const { code } = res.data;
  return code;
}

async function getAuthorizationData(code: string) {
  const res = await client.post(commonapiURL + `/auth/mobile/token`, { code });
  return { ...res.data };
}

async function getProfile() {
  return (await new AccountWsControllerApi().getFullAccountUsingPOST()).account;
}

export const restoreSession = () => (dispatch: any) => {
  const user = {};
  dispatch(sessionLoading());
  dispatch(sessionRestoring());
  dispatch(sessionSuccess(user, null));
};

export const authByUrl = () => async (dispatch: any) => {
  const authCode = getAuthCodeFromURL();
  return authCode
    ? dispatch(loginWithCode(authCode))
    : dispatch(
        loginWithUserPassword(process.env.USERNAME, process.env.PASSWORD)
      );
};

export const loginWithCode = (code: string) => async (dispatch: any) => {
  dispatch(sessionLoading());
  try {
    const authData: Token = await getAuthorizationData(code);
    if (!authData) {
      throw new Error("Auth data is null.");
    }
    const profile = await getProfile();
    dispatch(loadMyCurrencies());
    dispatch(sessionSuccess(profile, authData));
    dispatch(replaceAccountLanguage(profile));
  } catch (error) {
    dispatch(sessionError(error.toString()));
  }
};

export const loginWithUserPassword = (
  username: string,
  password: string
) => async (dispatch: any) => {
  dispatch(sessionLoading());
  try {
    const code: string = await getAuthorizationCode(username, password);
    const authData: Token = await getAuthorizationData(code);
    const profile = await getProfile();
    dispatch(loadMyCurrencies());
    dispatch(sessionSuccess(profile, authData));
    dispatch(replaceAccountLanguage(profile));
  } catch (error) {
    dispatch(sessionError(error.toString()));
  }
};

export const updateProfile = async (dispatch: any) => {
  dispatch(updateProfileLoading());
  try {
    const profile = await getProfile();
    dispatch(updateProfileSuccess(profile));
  } catch (error) {
    dispatch(updateProfileError(error.toString()));
  }
}

export const createAuthorizationCode = () => async (dispatch: any, getState: () => IApplicationStore) => {
  dispatch(authorizationCodeLoading());
  try {
    const accessToken = getState().sessionReducer.accessToken;
    const code: string = await getAuthorizationCodeByAccessToken(accessToken);
    dispatch(authorizationCodeSuccess(code));
  } catch (error) {
    dispatch(authorizationCodeError(error.toString()));
  }
};

function checkRegistrationFormErrors(
  request: CreateAccountRequest,
  fields: string[],
  parameters: string[]
) {
  fields.forEach((field) => {
    if (
      typeof request[field] === "undefined" ||
      request[field] === null ||
      !request[field].toString().length
    ) {
      parameters.push(field);
    }
  });
}

export const validateRequest = (
  request: CreateAccountRequest,
  fields: string[]
) => (dispatch: any) => {
  dispatch(validationLoading());

  const parameters: string[] = [];
  checkRegistrationFormErrors(request, fields, parameters);

  if (parameters.length) {
    dispatch(
      registrationError("Please fill out this field.", [
        { message: "", parameters },
      ])
    );
    return;
  }

  if (request.password !== request.passwordRepeat) {
    dispatch(
      registrationError("Please make sure your passwords match", [
        {
          message: "Please make sure your passwords match",
          parameters: ["password", "passwordRepeat"],
        },
      ])
    );
    return;
  }

  if (request.accept !== true) {
    dispatch(
      registrationError(
        "You must accept our Privacy Policy and Terms of Use before continue",
        [
          {
            message:
              "You must accept our Privacy Policy and Terms of Use before continue",
            parameters: ["accept"],
          },
        ]
      )
    );
    return;
  }

  const currentDate = new Date();
  const birthDate = new Date(request.birthDate);

  if (currentDate.getTime() < birthDate.getTime()) {
    dispatch(
      registrationError("Birth Date should not be greater than today", [
        {
          message: "Birth Date should not be greater than today",
          parameters: ["birthDate"],
        },
      ])
    );
    return;
  }

  dispatch(validationSuccess());
};

export const register = (request: CreateAccountRequest) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(registrationLoading());
  const state = getState();

  const requiredFields = [
    "email",
    "phone",
    "firstName",
    "lastName",
    "birthDate",
    "country",
    "password",
    "passwordRepeat",
    "accept",
  ];
  validateRequest(request, requiredFields);

  if (state.sessionReducer.error) {
    return;
  }

  request.address = {
    countryCode: request.country,
    city: request.city || "-",
    postalCode: request.postalCode || "-",
    firstAddressLine: request.addressLine || "-",
  };

  // @ts-ignore
  request.mobilePhone = {fullNumber: request.phone.toString()};

  delete request.city;
  delete request.postalCode;
  delete request.addressLine;
  delete request.phone;
  delete request.passwordRepeat;
  delete request.accept;

  const date = moment(request.birthDate, "YYYY-MM-DD");
  // @ts-ignore
  request.birthDate = date.format("YYYYMMDDHHmmss");

  let profile: Profile;
  try {
    const response = await new AccountWsControllerApi().createPersonalAccountUsingPOST(
      request
    );
    if (response.errorData) {
      const message = response.errorData.errorMessage;
      const parameters = response.errorData.parameters || [];
      if (parameters.length > 0) {
        let error: ICreateAccountFormError = {
          message: "Please fill out this field.",
          parameters: [],
        };
        parameters.forEach((parameter: string) => {
          if (parameter.includes("hone")) {
            error.parameters.push("phone");
          } else if (parameter.includes("countryCode")) {
            error.parameters.push("country");
          } else if (parameter.includes("firstAddressLine")) {
            error.parameters.push("addressLine");
          } else {
            error.parameters.push(parameter);
          }
        });
        const formErrors: ICreateAccountFormError[] = [error];
        dispatch(registrationError(message, formErrors));
      } else {
        throw new Error(message);
      }
    }
    profile = response.account;
    dispatch(registrationSuccess(profile));
  } catch (err) {
    dispatch(registrationError(err.message, []));
    return;
  }
};

export const replaceAccountLanguage = (account: Account) => (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  const { isLanguageChanged } = getState().rootReducer;
  if (!isLanguageChanged && account && (account.language || account.communicationLanguage)) {
    const languageQueryParam = getQueryParameterFromURL("language");
    const accountLanguage = account.language || account.communicationLanguage;
    if (!languageQueryParam && accountLanguage) {
      dispatch(changeLanguage(accountLanguage.code));
    }
  }
};

export const logoutUser = () => sessionLogout();

const sessionRestoring = () => ({
  type: SESSION_RESTORING,
});

const sessionLoading = () => ({
  type: SESSION_LOADING,
});

const sessionSuccess = (profile: any, authData: Token) => ({
  type: SESSION_SUCCESS,
  profile,
  authData,
});

const sessionError = (error: any) => ({
  type: SESSION_ERROR,
  error,
});

const sessionLogout = () => ({
  type: SESSION_LOGOUT,
});

const validationLoading = () => ({
  type: VALIDATION_LOADING,
});

const validationSuccess = () => ({
  type: VALIDATION_SUCCESS,
});

const registrationLoading = () => ({
  type: REGISTRATION_LOADING,
});

const registrationSuccess = (profile: any) => ({
  type: REGISTRATION_SUCCESS,
  profile,
});

const changeLanguage = (language: string) => ({
  type: CHANGE_APP_LANGUAGE,
  language,
});

const registrationError = (
  error: any,
  parameters: ICreateAccountFormError[]
) => ({
  type: REGISTRATION_ERROR,
  error,
  parameters,
});

const updateProfileLoading = () => ({
  type: UPDATE_PROFILE_LOADING,
});

const updateProfileSuccess = (profile: Profile) => ({
  type: UPDATE_PROFILE_SUCCESS,
  profile,
});

const updateProfileError = (error: any) => ({
  type: UPDATE_PROFILE_ERROR,
  error,
});

const authorizationCodeLoading = () => ({
  type: AUTHORIZATION_CODE_LOADING,
});

const authorizationCodeSuccess = (code: any) => ({
  type: AUTHORIZATION_CODE_SUCCESS,
  code,
});

const authorizationCodeError = (error: any) => ({
  type: AUTHORIZATION_CODE_ERROR,
  error,
});
