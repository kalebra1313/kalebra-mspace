import {
  OrderWsControllerApi,
  Card,
  CardWsControllerApi,
  CreditCardWsControllerApi,
  ErrorData,
  SettingsWsControllerApi,
} from "../types/commonapi";
import * as Sentry from "@sentry/browser";

import { SavedCard } from "../reducers/paymentCardsReducer";
import { IApplicationStore } from "../store/rootReducer";
import { Profile } from "../reducers/sessionReducer";
import { CreditCardPaymentWsControllerApi } from "../types/paymentapi";
import { createUUID, generateToken } from "../utils";

export const PAYMENT_CARD_LIST_LOADING = "PAYMENT_CARD_LIST_LOADING";
export const PAYMENT_CARD_LIST_LOADING_SUCCESS =
  "PAYMENT_CARD_LIST_LOADING_SUCCESS";
export const PAYMENT_CARD_LIST_LOADING_ERROR =
  "PAYMENT_CARD_LIST_LOADING_ERROR";

export const PAYMENT_CARD_SAVING = "PAYMENT_CARD_SAVING";
export const PAYMENT_CARD_SAVING_SUCCESS = "PAYMENT_CARD_SAVING_SUCCESS";
export const PAYMENT_CARD_SAVING_TEMPORARY_SUCCESS =
  "PAYMENT_CARD_SAVING_TEMPORARY_SUCCESS";
export const PAYMENT_CARD_SAVING_ERROR = "PAYMENT_CARD_SAVING_ERROR";

export const PAYMENT_CARD_DELETING = "PAYMENT_CARD_DELETING";
export const PAYMENT_CARD_DELETING_SUCCESS = "PAYMENT_CARD_DELETING_SUCCESS";
export const PAYMENT_CARD_DELETING_ERROR = "PAYMENT_CARD_DELETING_ERROR";

export const PAYMENT_CARD_SELECT_FOR_PAYMENT =
  "PAYMENT_CARD_SELECT_FOR_PAYMENT";
export const PAYMENT_CARD_SELECT_FOR_PAYMENT_SUCCESS =
  "PAYMENT_CARD_SELECT_FOR_PAYMENT_SUCCESS";
export const PAYMENT_CARD_SELECT_FOR_PAYMENT_ERROR =
  "PAYMENT_CARD_SELECT_FOR_PAYMENT_ERROR";

export const PAYMENT_CARD_VERIFY_LOADING = "PAYMENT_CARD_VERIFY_LOADING";
export const PAYMENT_CARD_VERIFY_SUCCESS = "PAYMENT_CARD_VERIFY_SUCCESS";
export const PAYMENT_CARD_VERIFY_ERROR = "PAYMENT_CARD_VERIFY_ERROR";

interface IErrorData {
  errorData?: ErrorData;
  warningData?: ErrorData;
}

export const throwIsHasErrorData = (response: IErrorData) => {
  let errorText: string;
  const { errorData, warningData } = response;
  if (errorData && errorData.errorCode) {
    errorText = `${errorData.errorMessage} (${errorData.errorCode})`;
  } else if (warningData && warningData.errorCode) {
    errorText = `${errorData.errorMessage} (${errorData.errorCode})`;
  }
  const error = new Error(errorText);
  Sentry.captureException(error);
  if (errorText) throw error;
};

const getAmountForCardLinking = async (currencyCode: string) => {
  return parseInt(
    (
      await new SettingsWsControllerApi().getSettingsUsingPOST({
        settingsNames: [`LinkCard_Amount_${currencyCode.toUpperCase()}`],
      })
    ).settings[0].value
  );
};

export const loadPaymentCardsList = () => async (dispatch: any) => {
  dispatch(paymentCardListListLoadingAction());
  try {
    dispatch(
      paymentCardListListLoadingSuccessAction(
        (await new CardWsControllerApi().getAccountCreditCardListUsingPOST())
          .cardList || []
      )
    );
  } catch (err) {
    dispatch(paymentCardListListLoadingErrorAction(err.toString()));
  }
};

export const createSeller = (profile: Profile): any => {
  const seller: any = {};
  const primaryEmail =
    profile.accountEmails && profile.accountEmails.filter((e) => e.primary)[0];
  const primaryPhone =
    profile.accountPhones && profile.accountPhones.filter((p) => p.primary)[0];
  if (primaryEmail) {
    seller.email = primaryEmail.email;
  } else {
    seller.phone = {
      countryCode: primaryPhone.countryCode,
      number: primaryPhone.number,
    };
  }
  return seller;
};

const createLinkCardOrder = async (
  amountTotal: number,
  currencyCode: string,
  seller: any
) => {
  return (
    await new OrderWsControllerApi().createLinkCardOrderUsingPOST({
      order: {
        amountTotal,
        currencyCode,
        seller,
      },
    })
  ).order;
};

export const savePaymentCard = (card: SavedCard, withOrder = true) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(paymentCardSavingAction());

  try {
    const state = getState();
    const seller = createSeller(state.sessionReducer.profile);
    const currency = state.myCurrenciesReducer.currencies.filter(
      (item) => item.primary
    )[0];

    const token = await generateToken(card);

    // @ts-ignore
    if (!card.uid) {
      // @ts-ignore
      card.uid = createUUID() + "_temporary";
      // @ts-ignore
      card.maskedNumber = createCardNumberMask(card);
    }

    card.token = token;

    if (withOrder) {
      const amount = await getAmountForCardLinking(currency.currency.code);
      const order = await createLinkCardOrder(
        amount,
        currency.currency.code,
        seller
      );

      if (!order || !order.uid) {
        throw new Error("Order was not created");
      }

      const data = {
        orderUid: order.uid,
        amount: order.amountTotal,
        currencyCode: order.currencyCode,
        paymentMethod: "bankcard",
        token,
        saveCard: card.saveCard,
      };

      if ((order.seller as any).email) {
        data["email"] = (order.seller as any).email;
      } else if ((order.seller as any).phone) {
        data["phone"] = (order.seller as any).phone;
      }

      const res = await new CreditCardPaymentWsControllerApi().paymentUsingPOST1(
        data
      );
      throwIsHasErrorData(res);
      dispatch(loadPaymentCardsList());
    }

    dispatch(paymentCardSavingSuccessAction(card));
  } catch (err) {
    dispatch(paymentCardSavingErrorAction(err.toString()));
  }
};

const createCardNumberMask = (card: SavedCard) => {
  const { cardNumber } = card;
  return (
    cardNumber.substr(0, 6) +
    "******" +
    cardNumber.substr(cardNumber.length - 4, 4)
  );
};

export const savePaymentCardTemporary = (card: SavedCard) => async (
  dispatch: any
) => {
  /* TODO */
  if (!card.uid) {
    card.uid = createUUID() + "_temporary";
  }

  if (!card.maskedNumber && card.cardNumber) {
    card.maskedNumber = createCardNumberMask(card);
  }

  dispatch(paymentCardSavingTemporarySuccessAction(card));
};

export const deletePaymentCard = (uid: string) => async (dispatch: any) => {
  dispatch(paymentCardDeletingAction());
  try {
    const res = await new CreditCardWsControllerApi().deleteUsingPOST2({
      creditCardUid: uid,
    });
    throwIsHasErrorData(res);
    dispatch(paymentCardDeletingSuccessAction(uid));
  } catch (err) {
    dispatch(paymentCardDeletingErrorAction(err.toString()));
  }
};

export const selectForPayment = (uid: string) => async (dispatch: any) => {
  dispatch(paymentCardSelectForPaymentAction());
  try {
    const res = await new CreditCardWsControllerApi().setAsPrimaryUsingPOST1({
      creditCardUid: uid,
    });
    throwIsHasErrorData(res);
    dispatch(paymentCardSelectForPaymentSuccessAction(uid));
  } catch (err) {
    dispatch(paymentCardSelectForPaymentErrorAction(err.toString()));
  }
};

export const verifyCreditCard = (
  cardUid: string,
  verificationCode: string
) => async (dispatch: any) => {
  dispatch(verifyCreditCardLoadingAction());
  try {
    const res = await new CardWsControllerApi().verifyCreditCardUsingPOST({
      cardUid,
      verificationCode,
    });
    throwIsHasErrorData(res);
    const { cardList = [] } = res;
    dispatch(loadPaymentCardsList());
    dispatch(verifyCreditCardSuccessAction(cardList));
  } catch (err) {
    dispatch(verifyCreditCardErrorAction(err.toString()));
  }
};

const paymentCardListListLoadingAction = () => ({
  type: PAYMENT_CARD_LIST_LOADING,
});

const paymentCardListListLoadingSuccessAction = (cards: Card[]) => ({
  type: PAYMENT_CARD_LIST_LOADING_SUCCESS,
  cards,
});

const paymentCardListListLoadingErrorAction = (error: any) => ({
  type: PAYMENT_CARD_LIST_LOADING_ERROR,
  error,
});

const paymentCardSavingAction = () => ({
  type: PAYMENT_CARD_SAVING,
});

const paymentCardSavingSuccessAction = (card: SavedCard) => ({
  type: PAYMENT_CARD_SAVING_SUCCESS,
  card,
});

const paymentCardSavingTemporarySuccessAction = (card: SavedCard) => ({
  type: PAYMENT_CARD_SAVING_TEMPORARY_SUCCESS,
  card,
});

const paymentCardSavingErrorAction = (error: any) => ({
  type: PAYMENT_CARD_SAVING_ERROR,
  error,
});

const paymentCardDeletingAction = () => ({
  type: PAYMENT_CARD_DELETING,
});

const paymentCardDeletingSuccessAction = (uid: string) => ({
  type: PAYMENT_CARD_DELETING_SUCCESS,
  uid,
});

const paymentCardDeletingErrorAction = (error: any) => ({
  type: PAYMENT_CARD_DELETING_ERROR,
  error,
});

const paymentCardSelectForPaymentAction = () => ({
  type: PAYMENT_CARD_SELECT_FOR_PAYMENT,
});

const paymentCardSelectForPaymentSuccessAction = (uid: string) => ({
  type: PAYMENT_CARD_SELECT_FOR_PAYMENT_SUCCESS,
  uid,
});

const paymentCardSelectForPaymentErrorAction = (error: any) => ({
  type: PAYMENT_CARD_SELECT_FOR_PAYMENT_ERROR,
  error,
});

const verifyCreditCardLoadingAction = () => ({
  type: PAYMENT_CARD_VERIFY_LOADING,
});

const verifyCreditCardSuccessAction = (cardList: Card[]) => ({
  type: PAYMENT_CARD_VERIFY_SUCCESS,
  cardList,
});

const verifyCreditCardErrorAction = (error: any) => ({
  type: PAYMENT_CARD_VERIFY_ERROR,
  error,
});
