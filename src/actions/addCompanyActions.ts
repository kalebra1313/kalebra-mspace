export const SET_COMPANY_INFO = 'SET_COMPANY_INFO'
export const SET_STORE_INFO = 'SET_STORE_INFO'

export const setCompanyInfo = () => (dispatch: any, companyInfo) => dispatch({ type: SET_COMPANY_INFO, companyInfo })
export const setStoreInfo = () => (dispatch: any, storeInfo) => dispatch({ type: SET_STORE_INFO, storeInfo })
