import { ProductListType, IProduct } from "../reducers/productReducer";
import { IApplicationStore } from "../store/rootReducer";
import { isProductInWish } from "./profileActions";
import {
  ProductControllerApi,
  ProductSearchListRequest,
  WishListControllerApi,
} from "../types/marketplaceapi";
import { getSortByFields } from "../pages/all-filtres-popup";

export const PRODUCT_LIST_LOADING = "PRODUCT_LIST_LOADING";
export const PRODUCT_LIST_LOADING_SUCCESS = "PRODUCT_LIST_LOADING_SUCCESS";
export const PRODUCT_LIST_LOADING_ERROR = "PRODUCT_LIST_LOADING_ERROR";

export const PRODUCT_LIST_TYPE_LOADING = "PRODUCT_LIST_TYPE_LOADING";
export const PRODUCT_LIST_TYPE_LOADING_SUCCESS =
  "PRODUCT_LIST_TYPE_LOADING_SUCCESS";
export const PRODUCT_LIST_TYPE_LOADING_ERROR =
  "PRODUCT_LIST_TYPE_LOADING_ERROR";

export const PRODUCT_DETAILS_LOADING = "PRODUCT_DETAILS_LOADING";
export const PRODUCT_DETAILS_LOADING_SUCCESS =
  "PRODUCT_DETAILS_LOADING_SUCCESS";
export const PRODUCT_DETAILS_LOADING_ERROR = "PRODUCT_DETAILS_LOADING_ERROR";

export const PRODUCT_ADDED_TO_WISH = "PRODUCT_ADDED_TO_WISH";

export const PRODUCT_WISH_LIST_LOADING = "PRODUCT_WISH_LIST_LOADING";
export const PRODUCT_WISH_LIST_LOADING_SUCCESS =
  "PRODUCT_WISH_LIST_LOADING_SUCCESS";
export const PRODUCT_WISH_LIST_LOADING_ERROR =
  "PRODUCT_WISH_LIST_LOADING_ERROR";

export const SEARCH_UPDATE_RESULT_COUNT = "SEARCH_UPDATE_RESULT_COUNT";

export type SortBy = {
  direction?: "ASC" | "DESC";
  field?: string;
  sortingIndex?: number;
};

export type ISearchParams = Pick<
  ProductSearchListRequest,
  | "address"
  | "category"
  | "coordinates"
  | "hashtags"
  | "inStock"
  | "name"
  | "count"
  | "offset"
  | "sortBy"
  | "type"
  | "sellerUid"
> & {
  resetSearch?: boolean;
  resetSorting?: boolean;
};

export function mapProductDetailsImage(item: IProduct) {
  const keys = Object.keys(item);
  const imagesKeys = keys.filter((key) => key.includes("imageUrl"));
  return imagesKeys.map((key) => item[key]);
}

function getCountryNameFromState(state: IApplicationStore): string {
  return (
    (state.customerLocationReducer.country &&
      state.customerLocationReducer.country.code) ||
    (state.sessionReducer.profile.country &&
      state.sessionReducer.profile.country.code) ||
    ""
  );
}

export async function loadProductDetails(
  uid: string,
  state: IApplicationStore
): Promise<IProduct> {
  const { language } = state.rootReducer;
  const item: IProduct = (
    await new ProductControllerApi().productSearchDetailsUsingPOST(
      { uid },
      getCountryNameFromState(state),
      language
    )
  ).body[0];
  item.wish = isProductInWish(item.uid, state);
  item.images = mapProductDetailsImage(item);
  return item;
}

export const searchProducts = (
  searchParams: ISearchParams,
  autocomplete = false
) => async (dispatch: any, getState: () => IApplicationStore) => {
  dispatch(productListLoadingAction(autocomplete));

  const state = getState();
  const { language } = state.rootReducer;
  const { chosenCategoryId, chosenSubcategoryId, sortBy } = state.filterReducer;
  const { count, offset, searchTerm } = state.productReducer;

  searchParams["name"] =
    typeof searchParams["name"] === "undefined" ? null : searchParams["name"];

  searchParams = {
    ...searchParams,
    category: chosenSubcategoryId || chosenCategoryId,
    name: searchParams.name ? searchParams.name : searchTerm,
    count: searchParams.count || count,
    offset:
      typeof searchParams["offset"] === "undefined"
        ? searchParams.name === searchTerm
          ? offset
          : 0
        : searchParams.offset, // ???????????????????????????????
  };

  if (sortBy.length) {
    searchParams.sortBy = getSortByFields(sortBy);
  }

  try {
    let body: IProduct[],
      totalCount: number,
      response = null;

    response = await new ProductControllerApi().productSearchUsingPOST(
      searchParams,
      getCountryNameFromState(state),
      language
    );

    body = response.body || [];
    totalCount = response.totalCount;
    body.forEach((item: IProduct) => {
      item.wish = isProductInWish(item.uid, state);
      item.images = mapProductDetailsImage(item);
    });

    return dispatch(
      productListLoadingSuccessAction(
        body,
        false,
        searchParams.name,
        totalCount,
        searchParams.resetSearch,
        searchParams.resetSorting,
        autocomplete,
        searchParams.offset || 0
      )
    );
  } catch (err) {
    dispatch(productListLoadingErrorAction(err.toString()));
  }
};

export const searchClear = (autocomplete = false) => {
  return productListLoadingSuccessAction(
    [],
    true,
    null,
    0,
    true,
    true,
    autocomplete
  );
};

export const updateSearchResultCount = (searchParams: ISearchParams) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(productListLoadingAction());

  const state = getState();
  const {
    allFiltresChosenCategoryId,
    allFiltresChosenSubcategoryId,
    allFiltresSortBy,
  } = state.filterReducer;
  const { language } = state.rootReducer;
  const { count, offset, searchTerm } = state.productReducer;

  const defaultCount =
    allFiltresChosenCategoryId || allFiltresChosenSubcategoryId ? 15 : 10;

  searchParams["name"] =
    typeof searchParams["name"] === "undefined" ? null : searchParams["name"];
  searchParams = {
    ...searchParams,
    category: allFiltresChosenSubcategoryId || allFiltresChosenCategoryId,
    name: searchParams.name ? searchParams.name : searchTerm,
    count: searchParams.name === searchTerm ? count : defaultCount,
    offset: 0,
  };

  if (allFiltresSortBy.length) {
    searchParams.sortBy = getSortByFields(allFiltresSortBy);
  }

  if (searchParams.count === 0) {
    searchParams.count = 15;
  }

  try {
    const totalCount = (
      await new ProductControllerApi().productSearchUsingPOST(
        searchParams,
        getCountryNameFromState(state),
        language
      )
    ).totalCount;
    dispatch({ type: SEARCH_UPDATE_RESULT_COUNT, totalCount });
  } catch (error) {}
};

export const loadProductListType = (listType: ProductListType) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(productListTypeLoadingAction(listType));

  const state = getState();
  const { language } = state.rootReducer;

  try {
    const controller = new ProductControllerApi();
    const items: IProduct[] =
      (listType === "all"
        ? (await controller.productSearchUsingPOST({}, getCountryNameFromState(state), language)).body
        : (
            await controller.productWidgetListUsingGET(
              listType,
              getCountryNameFromState(state),
              language
            )
          ).body) || [];
    items.forEach((item) => {
      item.wish = isProductInWish(item.uid, state);
      item.images = mapProductDetailsImage(item);
    });
    dispatch(productListTypeLoadingSuccessAction(items, listType));
  } catch (error) {
    dispatch(productListTypeLoadingErrorAction(error.toString(), listType));
  }
};

export const loadProductListCategory = (listType: ProductListType) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(productListTypeLoadingAction(listType));

  const state = getState();
  const { language } = state.rootReducer;

  try {
    const items: IProduct[] =
      (
        await new ProductControllerApi().productSearchUsingPOST(
          {
            category: listType,
            count: 15
          },
          getCountryNameFromState(state),
          language
        )
      ).body || [];
    items.forEach((item) => {
      item.wish = isProductInWish(item.uid, state);
      item.images = mapProductDetailsImage(item);
    });
    dispatch(productListTypeLoadingSuccessAction(items, listType));
  } catch (error) {
    dispatch(productListTypeLoadingErrorAction(error.toString(), listType));
  }
};

export const loadProductDetail = (uid?: string) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(productDetailsLoadingAction());

  const state = getState();

  try {
    const data = await loadProductDetails(uid, state);
    dispatch(productDetailsLoadingSuccessAction(data));
  } catch (error) {
    const errorMessage =
      error.response && error.response.data && error.response.data.errorData
        ? error.response.data.errorData.message
        : error.toString();
    dispatch(productDetailsLoadingErrorAction(errorMessage));
  }
};

export const loadProductWishList = () => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  dispatch(productWishListLoadingAction());

  const state = getState();
  const { language } = state.rootReducer;

  try {
    const items: IProduct[] =
      (await new WishListControllerApi().wishListUsingGET(language)).body || [];
    items.forEach((item) => {
      item.wish = true;
      item.images = mapProductDetailsImage(item);
    });
    dispatch(productWishListLoadingSuccessAction(items));
  } catch (error) {
    dispatch(productWishListLoadingErrorAction(error.toString()));
  }
};

export const addToWishList = (uid?: string) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  /* get before call productAddedToWish! */
  const state = getState();
  const { productsWishList } = state.productReducer;
  const productAlreadyAdded = productsWishList.filter(
    (item) => item.uid === uid
  ).length;

  dispatch(productAddedToWish(uid));

  try {
    const controller = new WishListControllerApi();
    const items: IProduct[] =
      (productAlreadyAdded
        ? (await controller.removeFromWishListUsingDELETE(uid)).body || []
        : (await controller.addToWishListUsingPUT(uid)).body) || [];
    items.forEach((item) => {
      item.wish = true;
      item.images = mapProductDetailsImage(item);
    });
    dispatch(productWishListLoadingSuccessAction(items));
  } catch (err) {}
};

const productListLoadingAction = (autocomplete = false) => ({
  type: PRODUCT_LIST_LOADING,
  autocomplete,
});

const productListLoadingSuccessAction = (
  products: IProduct[],
  is_clear?: boolean,
  searchTerm?: string,
  totalCount?: number,
  is_reset?: boolean,
  isResetSorting?: boolean,
  autocomplete?: boolean,
  offset?: number
) => ({
  type: PRODUCT_LIST_LOADING_SUCCESS,
  products,
  is_clear,
  searchTerm,
  totalCount,
  is_reset,
  isResetSorting,
  autocomplete,
  offset,
});

const productListLoadingErrorAction = (error: any) => ({
  type: PRODUCT_LIST_LOADING_ERROR,
  error,
});

const productListTypeLoadingAction = (listType: ProductListType) => ({
  type: PRODUCT_LIST_TYPE_LOADING,
  listType,
});

const productListTypeLoadingSuccessAction = (
  products: any[],
  listType: ProductListType
) => ({
  type: PRODUCT_LIST_TYPE_LOADING_SUCCESS,
  products,
  listType,
});

const productListTypeLoadingErrorAction = (
  error: any,
  listType: ProductListType
) => ({
  type: PRODUCT_LIST_TYPE_LOADING_ERROR,
  error,
  listType,
});

const productDetailsLoadingAction = () => ({
  type: PRODUCT_DETAILS_LOADING,
});

const productDetailsLoadingSuccessAction = (product: any) => ({
  type: PRODUCT_DETAILS_LOADING_SUCCESS,
  product,
});

const productDetailsLoadingErrorAction = (error: any) => ({
  type: PRODUCT_DETAILS_LOADING_ERROR,
  error,
});

const productWishListLoadingAction = () => ({
  type: PRODUCT_WISH_LIST_LOADING,
});

const productWishListLoadingSuccessAction = (products: IProduct[]) => ({
  type: PRODUCT_WISH_LIST_LOADING_SUCCESS,
  products,
});

const productWishListLoadingErrorAction = (error: any) => ({
  type: PRODUCT_WISH_LIST_LOADING_ERROR,
  error,
});

const productAddedToWish = (uid: string) => ({
  type: PRODUCT_ADDED_TO_WISH,
  uid,
});
