import { IApplicationStore } from "../store/rootReducer"
import { getPlatform, Platform } from '../utils'
import { BannerControllerApi } from '../types/marketplaceapi'

export const MARKETING_BANNERS_LIST_LOADING = 'MARKETING_BANNERS_LIST_LOADING'
export const MARKETING_BANNERS_LIST_LOADING_SUCCESS = 'MARKETING_BANNERS_LIST_LOADING_SUCCESS'

function getChannel() {
  const platform = getPlatform()
  switch (platform) {
    case Platform.Android:
      return 'android'
    case Platform.iOS:
      return 'ios'
    default:
      return 'web'
  }
}

const BANNER_COUNT = 5

export const loadMarketingBanners = () => async (dispatch: any, getState: () => IApplicationStore) => {
  dispatch({ type: MARKETING_BANNERS_LIST_LOADING })
  try {
    const state = getState();
    const { language } = state.rootReducer;
    const items = (await new BannerControllerApi().bannerListUsingGET(getChannel(), BANNER_COUNT, language)).body || []
    dispatch({ type: MARKETING_BANNERS_LIST_LOADING_SUCCESS, banners: items })
  } catch (error) { }
}

export const registerBannerClick = (bannerUid: string, channel?: string) => async (_dispatch: any, _getState: () => IApplicationStore) => {
  try {
    if (!channel) channel = getChannel()
    await new BannerControllerApi().processManualClickUsingGET(bannerUid, channel)
  } catch (error) { }
}