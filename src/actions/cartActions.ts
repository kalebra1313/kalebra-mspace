import { IProduct } from "../reducers/productReducer";
import { IApplicationStore } from "../store/rootReducer";
import {
  CartControllerApi,
  CommonMarketplaceApiResponseCart,
} from "../types/marketplaceapi";

export const CART_LOAD = "CART_LOAD";
export const CART_LOAD_SUCCESS = "CART_LOAD_SUCCESS";
export const CART_LOAD_ERROR = "CART_LOAD_ERROR";
export const CART_ADD = "CART_ADD";
export const CART_UPDATE = "CART_UPDATE";
export const CART_REMOVE = "CART_REMOVE";
export const CART_REMOVE_BY_SELLER = "CART_REMOVE_BY_SELLER";
export const CART_CLEAR = "CART_CLEAR";
export const SELECT_FOR_PURCHASE = "SELECT_FOR_PURCHASE";

const convertCommonMarketplaceApiResponseCart = (
  result: CommonMarketplaceApiResponseCart
): {
  product: IProduct;
  price: number;
  count: number;
  discount: number;
  total: number;
  selectedForPurchase?: boolean;
}[] => {
  return [];
};

export const cartLoad = () => async (dispatch) => {
  dispatch({
    type: CART_LOAD,
  });
  try {
    const result = await new CartControllerApi().getAccountCartUsingGET();

    if (result.successful) {
      dispatch({
        type: CART_LOAD_SUCCESS,
        items: convertCommonMarketplaceApiResponseCart(result),
      });
    } else {
      throw new Error(JSON.stringify(result.errorData));
    }
  } catch (err) {
    dispatch({
      type: CART_LOAD_ERROR,
      error: err.toString(),
    });
  }
};

export const cartAdd = (item: IProduct, count: number) => async (
  dispatch: any
) => {
  try {
    dispatch({
      type: CART_ADD,
      item,
      count,
    });
    /*
    const result = await new CartControllerApi().addItemToCartUsingPOST({
      quantity: count,
      productUid: item.uid,
    });

    if (result.successful) {
      dispatch({
        type: CART_ADD,
        item,
        count,
      });
    }
    */
  } catch (err) {}
};

export const cartUpdate = (item: IProduct, count: number) => async (
  dispatch: any
) => {
  try {
    /*
    await new CartControllerApi().removeItemFromCartUsingPOST({
      productUid: item.uid,
    });
    await new CartControllerApi().addItemToCartUsingPOST({
      quantity: count,
      productUid: item.uid,
    });
    */
  } catch (err) {}

  dispatch({
    type: CART_UPDATE,
    item,
    count,
  });
};

export const cartRemove = (item: IProduct, count: number) => async (
  dispatch: any
) => {
  try {
    /*
    await new CartControllerApi().removeItemFromCartUsingPOST({
      productUid: item.uid,
    });
    */
  } catch (err) {}

  dispatch({
    type: CART_REMOVE,
    item,
    count,
  });
};

export const cartRemoveBySeller = (seller: string) => async (
  dispatch: any,
  getState: () => IApplicationStore
) => {
  const state = getState();
  try {
    /*
    const items = state.cartReducer.items.filter(
      (item) => item.product.sellerUid === seller
    );
    items.forEach(async (item) => {
      await new CartControllerApi().removeItemFromCartUsingPOST({
        productUid: item.uid,
      });
    });
    */
  } catch (err) {}

  dispatch({
    type: CART_REMOVE_BY_SELLER,
    seller,
  });
};

export const cartClear = (onlySelectedForPurchase = false) => {
  return {
    type: CART_CLEAR,
    onlySelectedForPurchase,
  };
};

export const BY_SELLER = "BY_SELLER";

export const SelectForPurchaseFilters = {
  BySeller: BY_SELLER,
};

export const selectForPurchaseBySeller = (sellerUid: string) => {
  return {
    type: SELECT_FOR_PURCHASE,
    filter: SelectForPurchaseFilters.BySeller,
    sellerUid,
  };
};
