import { ISearchParams, mapProductDetailsImage } from "./productActions"
import { IApplicationStore } from "../store/rootReducer"
import { IProduct } from "../reducers/productReducer"
import { isProductInWish } from "./profileActions"
import { ProductControllerApi } from '../types/marketplaceapi'
import { getProfile } from "../selectors/profile";

export const ALL_GOODS_LOADING = 'ALL_GOODS_LOADING'
export const ALL_GOODS_LOADING_SUCCESS = 'ALL_GOODS_LOADING_SUCCESS'
export const ALL_GOODS_LOADING_ERROR = 'ALL_GOODS_LOADING_ERROR'

export const loadAllGoods = (searchParams: ISearchParams) => async (dispatch: any, getState: () => IApplicationStore) => {
  dispatch(allGoodsLoadingAction())

  const state = getState();
  const { language } = state.rootReducer;
  let country = state.customerLocationReducer.country?.code
    || getProfile({ ...state }).country?.code
    || language;

  try {
    const items: IProduct[] = (await new ProductControllerApi().productSearchUsingPOST(searchParams, country, language)).body || []
    items.forEach(item => {
      item.wish = isProductInWish(item.uid, state)
      item.images = mapProductDetailsImage(item)
    })
    dispatch(allGoodsLoadingSuccessAction(items))
  } catch (error) {
    dispatch(allGoodsLoadingErrorAction(error.toString()))
  }
}

const allGoodsLoadingAction = () => ({
  type: ALL_GOODS_LOADING
})

const allGoodsLoadingSuccessAction = (products: IProduct[]) => ({
  type: ALL_GOODS_LOADING_SUCCESS,
  products,
})

const allGoodsLoadingErrorAction = (error: any) => ({
  type: ALL_GOODS_LOADING_ERROR,
  error
})