import { AnyAction } from "redux";
import {
  CART_ADD,
  CART_CLEAR,
  CART_REMOVE,
  CART_REMOVE_BY_SELLER,
  CART_UPDATE,
  SELECT_FOR_PURCHASE,
  SelectForPurchaseFilters,
  BY_SELLER,
  CART_LOAD_SUCCESS,
} from "../actions/cartActions";
import { IProduct } from "./productReducer";

export interface ICartState {
  isFetching: boolean;
  isUpdating: boolean;
  error?: any;
  items: {
    product: IProduct;
    price: number;
    count: number;
    discount: number;
    total: number;
    selectedForPurchase?: boolean;
  }[];
  discount: number;
  total: number;
  discountSelectedForPurchase: number;
  totalSelectedForPurchase: number;
}

const initialState: ICartState = {
  isFetching: false,
  isUpdating: false,
  items: [],
  discount: 0,
  total: 0,
  discountSelectedForPurchase: 0,
  totalSelectedForPurchase: 0,
};

const refreshCartState = (cartState: ICartState): ICartState => {
  const items = cartState.items.map((item) => {
    return {
      ...item,
      total: item.count * item.price - item.discount,
    };
  });

  return {
    ...cartState,
    items,
    total:
      items.map((item) => item.total).reduce((p, c) => p + c, 0) -
      cartState.discount,
    totalSelectedForPurchase:
      items
        .map((item) => (item.selectedForPurchase ? item.total : 0))
        .reduce((p, c) => p + c, 0) - cartState.discount,
  };
};

const getPrice = (item: IProduct) => {
  if (typeof item.discountedPrice !== "undefined" && item.discountedPrice !== 0)
    return item.discountedPrice;
  return item.price;
};

const cartReducer = (state = initialState, action: AnyAction): ICartState => {
  switch (action.type) {
    case CART_ADD: {
      const { item, count } = action;

      const foundItem = state.items.filter(
        (_item) => _item.product.uid === item.uid
      )[0];

      if (foundItem) {
        foundItem.count += typeof count !== "undefined" ? count : 1;
        foundItem.selectedForPurchase = true;
      } else {
        state.items.push({
          product: item,
          count,
          price: getPrice(item),
          discount: 0,
          total: 0,
          selectedForPurchase: true,
        });
      }

      return {
        ...refreshCartState(state),
      };
    }
    case CART_UPDATE: {
      const { item, count } = action;

      const foundItem = state.items.filter(
        (_item) => _item.product.uid === item.uid
      )[0];

      if (foundItem) {
        foundItem.count = typeof count !== "undefined" ? count : 1;
        foundItem.selectedForPurchase = true;
      } else {
        state.items.push({
          product: item,
          count,
          price: getPrice(item),
          discount: 0,
          total: 0,
          selectedForPurchase: true,
        });
      }

      return {
        ...refreshCartState(state),
      };
    }
    case CART_REMOVE: {
      const { item, count } = action;

      const foundItem = state.items.filter(
        (_item) => _item.product.uid === item.uid
      )[0];

      if (
        foundItem &&
        typeof count !== "undefined" &&
        foundItem.count > count
      ) {
        foundItem.count -= count;
      } else {
        state.items = state.items.filter(
          (_item) => _item.product.uid !== item.uid
        );
      }

      return {
        ...refreshCartState(state),
      };
    }
    case CART_REMOVE_BY_SELLER: {
      const { seller } = action;
      state.items = state.items.filter(
        (item) => item.product.sellerUid !== seller
      );
      return {
        ...refreshCartState(state),
      };
    }
    case CART_CLEAR: {
      const { onlySelectedForPurchase } = action;
      if (onlySelectedForPurchase) {
        state.items = state.items
          .filter((item) => !item.selectedForPurchase)
          .map((item) => {
            return { ...item, selectedForPurchase: true };
          });
      } else {
        state.items = [];
      }
      return {
        ...refreshCartState(state),
      };
    }
    case SELECT_FOR_PURCHASE: {
      const { filter } = action;
      let { items } = state;

      if (filter === BY_SELLER) {
        const { sellerUid } = action;
        items = items.map((item) => {
          return {
            ...item,
            selectedForPurchase: item.product.sellerUid === sellerUid,
          };
        });
      }

      state.items = items;

      return {
        ...refreshCartState(state),
      };
    }
    case CART_LOAD_SUCCESS: {
      const { items } = action;
      state.items = items;

      return {
        ...refreshCartState(state),
      };
    }
    default:
      return state;
  }
};

export default cartReducer;
