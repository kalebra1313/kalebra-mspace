import { AnyAction } from "redux";
import { Profile } from "./sessionReducer";
import {
  PROFILE_UPDATING,
  PROFILE_UPDATING_SUCCESS,
  PROFILE_UPDATING_ERROR,
  ACCOUNT_UPDATE_SETTINGS,
  ACCOUNT_UPDATE_SETTINGS_SUCCESS,
  ACCOUNT_UPDATE_SETTINGS_ERROR,
} from "../actions/profileActions";

export interface IProfileState {
  profile?: Profile,
  updating?: boolean,
  error?: any
}

const initialState: IProfileState = {
  profile: {},
  updating: false,
  error: null
}

const profileReducer = (state = initialState, action: AnyAction): IProfileState => {
  switch (action.type) {
    case PROFILE_UPDATING:
      return {
        ...state,
        updating: true,
        error: null
      }
    case PROFILE_UPDATING_SUCCESS:
      const { profile } = action
      return {
        ...state,
        updating: false,
        profile,
      }
    case PROFILE_UPDATING_ERROR:
      return {
        ...state,
        updating: false,
        error: action.error
      }
    case ACCOUNT_UPDATE_SETTINGS:
      return {
        ...state,
        updating: true,
        error: null
      }
    case ACCOUNT_UPDATE_SETTINGS_SUCCESS:
      return {
        ...state,
        updating: false,
        profile: action.profile,
      }
    case ACCOUNT_UPDATE_SETTINGS_ERROR:
      return {
        ...state,
        updating: false,
        error: action.error
      }
    default:
      return state
  }
}

export default profileReducer
