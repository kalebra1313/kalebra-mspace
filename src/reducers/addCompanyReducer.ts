import { AnyAction } from "redux";
import {SET_COMPANY_INFO, SET_STORE_INFO} from "../actions/addCompanyActions";

export interface IAddCompanyState {
  companyInfo: {
    country: string;
    form: string;
    name: string;
    regNumber: string;
    vatNumber: string;
    email: string;
    phone: string;
    phoneCode: string;
    links: string[];
    addressCountry: string;
    addressCity: string;
    address: string;
    postcode: string;
  };
  storeInfo: {
    name: string;
    email: string;
    phone: string;
    description: string;
    links: string[];
  };
}

const testInitialState: IAddCompanyState = {
  companyInfo: {
    country: "AF",
    form: "5",
    name: "warner",
    regNumber: "warnerReg",
    vatNumber: "warnerVat",
    email: "warner@gmail.com",
    phone: "37677515246",
    phoneCode: "376",
    links: ["https://git.mvp.sh/outsource/mspace/-/merge_requests/77", "https://google.com"],
    addressCountry: "MD",
    addressCity: "Tiraspol",
    address: "19-15 Strada Stepnaia",
    postcode: "5500"
  },
  storeInfo: {
    name: "",
    email: "",
    phone: "",
    description: "",
    links: [""]
  }
}

const initialState: IAddCompanyState = {
  companyInfo: {
    country: "",
    form: "",
    name: "",
    regNumber: "",
    vatNumber: "",
    email: "",
    phone: "",
    phoneCode: "",
    links: [""],
    addressCountry: "",
    addressCity: "",
    address: "",
    postcode: ""
  },
  storeInfo: {
    name: "",
    email: "",
    phone: "",
    description: "",
    links: [""]
  }
}

const addCompanyReducer = (state = initialState, action: AnyAction): IAddCompanyState => {
  switch (action.type) {
    case SET_COMPANY_INFO:
      return {
        ...state,
        companyInfo: action.companyInfo
      }
    case SET_STORE_INFO:
      return {
        ...state,
        storeInfo: action.storeInfo
      }
    default:
      return state
  }
}

export default addCompanyReducer
