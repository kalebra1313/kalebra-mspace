import React, {memo} from 'react';
import { useState } from 'react';
import { useEffect } from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { useDispatch } from 'react-redux';
import { compose } from 'redux';
import { chooseCategorySubcategory } from '../../actions/filterActions';
import { ICategoryClassificator } from '../../reducers/categoryReducer';
import connectCategoriesClassificator from '../../store/connectCategoriesClassificator';
import { getCategory, getSubRoutes } from '../../utils';
import './index.less';

type Props = WithTranslation & {
  categoryCode?: string;
  categoriesClassificator: ICategoryClassificator[];
  hideLast?: boolean;
  handleBackToMain: () => void;
}

const Breadcrumb = ({categoryCode, categoriesClassificator, hideLast, handleBackToMain, t}: Props) => {

  const [subRoutes, setSubRoutes] = useState([]);
  const dispatch = useDispatch();
  
  useEffect(() => {
    const category = getCategory(categoriesClassificator, categoryCode);
    const subRoutes = getSubRoutes(category);

    if(hideLast){
      subRoutes.pop();
    }
    setSubRoutes(subRoutes);
  }, [categoryCode, categoriesClassificator, hideLast]);

  const hadnleClick =  (categoryCode) => {
    dispatch(chooseCategorySubcategory(
      subRoutes[0].categoryCode,
      categoryCode
    ))
  }
  
  return (
    <div className="breadcrumb block-title" slot="before-inner">
      <span onClick={handleBackToMain}>{t("Main")}</span>
      {categoryCode && subRoutes.map((route) => (
        <span key={route.categoryCode} onClick={() => hadnleClick(route.categoryCode)}>
          {route.categoryName}
        </span>
      ))}
    </div>
  )
}

export default memo(compose(connectCategoriesClassificator,  withTranslation())(Breadcrumb));