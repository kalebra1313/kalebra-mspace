import React, { memo } from 'react';
import { compose } from 'redux';
import { Block } from "framework7-react";
import { WithTranslation, withTranslation } from 'react-i18next';
import { formatDate, getCategory, getSubRoutes } from '../../utils';
import connectCategoriesClassificator from '../../store/connectCategoriesClassificator';
import { ICategoryClassificator } from '../../reducers/categoryReducer';
import { IProduct } from '../../reducers/productReducer';
import './index.less';

type Props = WithTranslation & {
  item: IProduct;
  categoriesClassificator:  ICategoryClassificator[]
}

const ProductCategoryInfo = ({ categoriesClassificator, item, t }: Props) => {

  if (!categoriesClassificator.length || !item) {
    return null;
  }

  const category = getCategory(categoriesClassificator, item.category);
  const subRoutes = getSubRoutes(category);
  const mainCategory = subRoutes && subRoutes[0];
  const subCategory = subRoutes && subRoutes[1];

  return (
    <Block className="category-info">
      <div className="category-info-content">
        <p><span>{t("Posted")}</span> {formatDate(item.productDate.toString())}</p>
        {mainCategory && <p><span>{t('Category')}</span> <a>{mainCategory.categoryName}</a></p>}
        {subCategory && <p><span>{t('Subcategory')}</span> <a>{subCategory.categoryName}</a></p>}
      </div>
    </Block>
  )
}

export default compose(connectCategoriesClassificator, withTranslation())(memo(ProductCategoryInfo));
