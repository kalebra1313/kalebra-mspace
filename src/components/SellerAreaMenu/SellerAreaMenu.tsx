import React, {useState} from "react"
import {AccordionContent, Icon, List, ListItem} from "framework7-react"
import {useTranslation} from "react-i18next"
import {IcOrders, IcStore} from "../../components-ui/icons"
import {IApplicationStore} from "../../store/rootReducer"
import {getProfile} from "../../selectors/profile"
import {useSelector} from "react-redux"
import classNames from "classnames"
import "./style.less"

const menu = {
  SellerArea_Products: {
    link: "/profile/seller-area/my-goods/",
    title: "My products",
    icon: "list_bullet",
  },
  SellerArea_Orders: {
    link: "/profile/seller-area/sellers-orders/",
    title: "Orders",
    icon: <IcOrders slot="media"/>,
  },
  SellerArea_ProductPromotion: {
    link: "/profile/seller-area/product-promotion/",
    title: "Product promotion",
    icon: "star",
  },
  SellerArea_PayoutSettings: {
    link: "/profile/seller-area/payout-settings/",
    title: "Payout Settings",
    icon: "gear_alt",
  },
  Switch_To_Business: {
    link: "/profile/seller-area/add-business-account/",
    title: "Switch to Business Account",
    icon: <IcStore slot="media"/>,
  },
}

const SellerAreaMenu = ({selected}: {selected?: string}) => {
  const menuItems: string[] = useSelector((state: IApplicationStore) => state.rootReducer.localConfig
    .profileMenuItems?.filter(i => Object.keys(menu).includes(i)))
  const profileType: string = useSelector((state: IApplicationStore) => getProfile(state)?.type)
  const isWithAccordion = menuItems.slice(4).filter(i => !(profileType !== "S" && i === "Switch_To_Business")).length > 1
  const {t} = useTranslation()
  const [accordionTitle, setAccordionTitle] = useState<string>(t("More").toString())
  const [accordionOpened, setAccordionOpened] = useState<boolean>(false)

  const handleAccordionOpened = (): void => {
    const more = t("More").toString()
    const less = t("Less").toString()
    setAccordionTitle(accordionTitle === more ? less : more)
    setAccordionOpened(!accordionOpened)
  }

  const renderListItem = (name: string, index: number) => {
    const itemData = menu[name]
    if (name === "Switch_To_Business" && profileType !== "S") return <div key={index}/>
    return (
      <ListItem
        key={index}
        link={itemData.link}
        title={t(itemData.title).toString()}
        className={classNames(selected === name && "active")}
      >
        {typeof itemData.icon !== "string" ? itemData.icon : <Icon slot="media" size="24" f7={itemData.icon}/>}
      </ListItem>
    )
  }

  return (
    <List className="seller-area-menu" noHairlines noHairlinesBetween={typeof selected === "string"} accordionList accordionOpposite>
      {!isWithAccordion
        ? menuItems.map((item, index) => renderListItem(item, index))
        : menuItems.slice(0, 4).map((item, index) => renderListItem(item, index))}
      {isWithAccordion && (
        <ListItem
          accordionItem
          onAccordionOpen={(): void => handleAccordionOpened()}
          onAccordionClose={(): void => handleAccordionOpened()}
          title={accordionTitle}
          noChevron
        >
          <Icon slot="media" f7={accordionOpened ? "chevron_up" : "chevron_down"} size="24"/>
          <AccordionContent>
            <List>{menuItems.slice(4).map((item, index) => renderListItem(item, index))}</List>
          </AccordionContent>
        </ListItem>
      )}
    </List>
  )
}

export default SellerAreaMenu
