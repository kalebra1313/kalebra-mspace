import React from "react";
import { Link, Icon } from "framework7-react";
import classNames from "classnames";

import "./style.less";
import { IcArrowBack } from "../../components-ui/icons";

export default (props: Link.Props): JSX.Element => {
  const { className, text, ...rest } = props;
  return (
    <Link    
    className={classNames("to-messenger-button", className)} {...rest}>
    <IcArrowBack/>
      <span>{text}</span>
    </Link>
  );
};
