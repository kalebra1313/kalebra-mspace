import React, { useRef } from "react";
import { Block, Link, Icon } from "framework7-react";
import { Price } from "./Price/index";
import {
  getCategoryNameByCategoryId,
  createThumbnailVideoURLLink,
} from "../utils";
import { compose } from "redux";
import connectCategoriesClassificator from "../store/connectCategoriesClassificator";
import { ICategoryClassificator } from "../reducers/categoryReducer";
import AddWishButton from "./AddWishButton";
import { addToWishList } from "../actions/productActions";
import { connect } from "react-redux";
import ChatWithSeller from "./ChatWithSeller";
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, {
  Pagination,Navigation,Virtual
} from 'swiper/core';
import { WithTranslation, withTranslation } from "react-i18next";
import { IcCrown } from "../components-ui/icons";
import { hasFeatureCode } from "./Catalog/CatalogItem";
import classNames from "classnames";
import { useEffect } from "react";
import 'swiper/swiper.less';

import "./Slider.less";

const defaultSliderParams: any = {
  // centeredSlides: true,
  spaceBetween: 8,
  // slidesPerView: "auto",
};

export enum SliderType {
  "top" = 0,
  "big",
  "small",
  "images",
}
SwiperCore.use([Pagination,Navigation,Virtual]);


export type SliderItem = {
  uid?: string;
  href?: string;
  name?: string;
  image?: string;
  promo?: string;
  description?: string;
  category?: string;
  categoryName?: string;
  price?: number;
  priceDiscount?: number;
  currencyCode?: string;
  onClick?(item: SliderItem): void;
  wish?: boolean;
  sellerPhone?: string;
  featureCodes?: string[];
  videoId?: string;
  videoType?: string;
  shortDescription?: string;
};

type Props = Partial<WithTranslation> & {
  className?: string;
  slides: SliderItem[];
  type?: SliderType;
  showIfEmpty?: boolean;
  categoriesClassificator?: ICategoryClassificator[];
  addToWish?(uid?: string): void;
  startChat?(uid: string): void;
  onClick?(item: SliderItem): void;
  showFeaturesHiglight?: boolean;
  videoPlayOnClick?(videoId: string, videoType: string): void;
  slideIndex?: number;
  sliderZoom?: boolean;
  changeActiveIndex?(index: number): void;
};

const getSlideBackgroundStyle = (url: string) =>
  url ? { backgroundImage: `url(${url})` } : null;

const slideItem: any = ({
  index,
  type,
  item,
  empty,
  classificator = [],
  onClickAddToWish,
  onClickStartChat,
  onClick,
  t,
  showFeaturesHiglight,
  videoPlayOnClick,
}: {
  index: number;
  type?: SliderType;
  item: SliderItem;
  empty?: boolean;
  classificator?: ICategoryClassificator[];
  onClickAddToWish?(uid: string): void;
  onClickStartChat?(uid: string): void;
  onClick?(item: SliderItem): void;
  t?: any;
  showFeaturesHiglight?: boolean;
  videoPlayOnClick?(videoUrl: string, videoType: string): void;
}) => {

  const { image, name, promo, featureCodes, videoId, videoType } = item;

  switch (type) {
    case SliderType.top:
      return (
        <SwiperSlide key={index} className="link" style={getSlideBackgroundStyle(image)}>
          <div
            onClick={() => {
              if (onClick) onClick(item);
            }}
            style={{
              position: "absolute",
              left: 0,
              right: 0,
              top: 0,
              bottom: 0,
            }}
          >
            <p className="name">{name}</p>
            <p className="promo">{promo}</p>
          </div>
        </SwiperSlide>
      );
    case SliderType.big:
      return (
        <SwiperSlide key={index} className="big-slide no-padding">
          <div
            className="link slider-link"
            onClick={() => (item.onClick ? item.onClick(item) : {})}
          >
            <div
              className="header-image"
              style={getSlideBackgroundStyle(image)}
            />
            <Block className="info">
              <p className="title">{item.name}</p>
              <span className="category">
                {item.categoryName ||
                  getCategoryNameByCategoryId(item.category, classificator)}
              </span>
              <span className="description pure-visible-xl">{item.shortDescription}
              </span>
              {!empty && (
                <Price
                  price={item.price}
                  discountedPrice={item.priceDiscount}
                  currencyCode={item.currencyCode}
                />
              )}
            </Block>
          </div>
          {!empty && (
            <AddWishButton
              active={item.wish}
              withShadow
              onClick={() => {
                onClickAddToWish(item.uid);
              }}
            />
          )}
          {!empty && (
            <ChatWithSeller
              text={t("Chat with seller")}
              onClick={() => onClickStartChat(item.uid)}
            />
          )}
        </SwiperSlide>
      );
    case SliderType.small:
      return (
        <SwiperSlide key={index} className="small-slide no-padding">
          <div
            className={classNames(
              "link",
              showFeaturesHiglight &&
                hasFeatureCode("higlight_bold", featureCodes) &&
                "feature-higlight-bold"
            )}
            onClick={() => (item.onClick ? item.onClick(item) : {})}
          >
            <div className="header-image">{image && <img src={image} style={{maxHeight: '300px'}}/>}</div>
            <Block className="info">
              <div className="title-row">
                <div className="title">{item.name}</div>
                {showFeaturesHiglight && hasFeatureCode("vip", featureCodes) && (
                  <div className="feature-icon-crow">
                    <IcCrown />
                  </div>
                )}
              </div>
              <p className="description">{item.description || item.category}</p>
              <p className="category pure-visible-xs">{item.categoryName}</p>
              {!empty && (
                <Price
                  price={item.price}
                  discountedPrice={item.priceDiscount}
                  currencyCode={item.currencyCode}
                />
              )}
            </Block>
          </div>
          {!empty && (
            <AddWishButton
              active={item.wish}
              withShadow
              onClick={() => {
                onClickAddToWish(item.uid);
              }}
            />
          )}
        </SwiperSlide>
      );
    case SliderType.images: {
      return image ? (<SwiperSlide key={index} style={getSlideBackgroundStyle(image)} />
      ) : (
        <SwiperSlide>
          <Link
            onClick={() =>
              videoPlayOnClick && videoPlayOnClick(videoId, videoType)
            }
            style={{
              backgroundColor: "#efefef",
              justifyContent: "center",
              alignItems: "center",
              display: "flex",
              width: "100%",
              height: "100%",
              flexShrink: 0,
              backgroundImage: `url(${createThumbnailVideoURLLink(
                videoId,
                videoType
              )})`,
              backgroundSize: "cover",
              backgroundRepeat: "no-repeat",
              backgroundPosition: "center",
            }}
          >
            <Icon
              material="play_circle_filled"
              size="5rem"
              style={{ color: "rgb(202, 202, 202)" }}
            />
          </Link>
        </SwiperSlide>
      );
    }
    default:
      return null;
  }
};

function emptySlides(type: SliderType): JSX.Element[] {
  const slidesCounts = type === SliderType.small ? 2 : 1;
  return [slidesCounts].map((i, index) => (
    slideItem({type, item: {}, empty: true, index})
  ));
}

let prevSlideIndex = 0;
let prevSliderZoom;

const Slider = (props) => {

 const swiperRef = useRef(null);

 const {
  className,
  slides,
  type = SliderType.top,
  showIfEmpty,
  categoriesClassificator,
  addToWish,
  startChat,
  onClick,
  videoPlayOnClick,
  t,
  showFeaturesHiglight,
  slideIndex,
  sliderZoom,
  changeActiveIndex,
  } = props;

  const handleChangeIndex = (e) => {
    changeActiveIndex && changeActiveIndex(e.activeIndex+1);
  }
  
  useEffect(() => {
    if(swiperRef.current){
      if(prevSlideIndex!==slideIndex){
        swiperRef.current.swiper.slideTo(slideIndex, 380);
      }
      if(sliderZoom!==prevSliderZoom){
        swiperRef.current.swiper.slideTo(slideIndex, 0);
      }
    }
    prevSlideIndex = slideIndex;
    prevSliderZoom = sliderZoom;
  }, [slideIndex]);

  const params = getParams(type);

  return(
    <Swiper
      ref={swiperRef}
      {...params}
      spaceBetween={sliderZoom ? 20 : 30}
      onSlideChange={handleChangeIndex}
      navigation={true}
      pagination={type === SliderType.top || type === SliderType.images}
      className={`slider ${type === SliderType.small ? " small-slider" : ""}${
        className ? ` ${className}` : ""
      }`}
    >
      {slides.length ? slides.map((item, index) => (
        slideItem({
          index,
          type,
          item,
          classificator: categoriesClassificator,
          onClickAddToWish: addToWish,
          onClickStartChat: startChat,
          onClick:onClick,
          t:t,
          showFeaturesHiglight:showFeaturesHiglight,
          videoPlayOnClick:videoPlayOnClick,
        })
      ))
      : showIfEmpty
        ? emptySlides(type)
      : null
      }
    </Swiper>
  )
}

const mapDispatchToProps = (dispatch: any) => ({
  addToWish: (uid?: string) => dispatch(addToWishList(uid)),
});

export default compose(
  withTranslation(),
  connectCategoriesClassificator,
  connect(null, mapDispatchToProps)
)(Slider) as React.ComponentType<Props>;

// Helpers
const getParams = (type?: SliderType) => {
  let params = { ...defaultSliderParams };

  if (type === SliderType.small) {
    params.centeredSlides = false;
    params.breakpoints = {
      320: {
        slidesPerView: 2.5,
        spaceBetween: 8,
      },
      768: {
        slidesPerView: 3,
      },
      1024: {
        slidesPerView: 5,
        spaceBetween: 16,
      },
    };
  } else if (type === SliderType.big) {
    params.breakpoints = {
      320: {
        slidesPerView: 1.1,
        spaceBetween: 16,
      },
      768: {
        centeredSlides: false,
        slidesPerView: 4,
        spaceBetween: 16,
      },
    };
  }
  else if(type === SliderType.top){
    params.breakpoints = {
      320: {
        slidesPerView: 1.08,
        spaceBetween: 16,
      },
      768: {
        slidesPerView: 1
      }
    }
  }
  return params;
};

