import React from "react";
import {
  Popup,
  F7Popup,
  Page,
  Navbar,
  NavRight,
  Link,
  Block,
  ListInput,
  List,
  Button,
} from "framework7-react";
import { connect } from "react-redux";
import { compose } from "redux";
import classNames from "classnames";
import { loginWithUserPassword } from "../../actions/sessionActions";
import { Profile } from "../../reducers/sessionReducer";
import { getProfile } from "../../selectors/profile";
import { IApplicationStore } from "../../store/rootReducer";
import { withTranslation, WithTranslation } from "react-i18next";

import "./style.less";

type Props = F7Popup.Props & Pick<WithTranslation, "t"> & {
  profile?: Profile;
  loading: boolean;
  error: any;
  onRegister: any;
  loginWithUserPassword?(
    username: string,
    password: string
  ): void;
}

type State = {
  username: string;
  password: string;
};

class LoginDesktopPopup extends React.Component<Props, State> {
  constructor(props: Readonly<Props>) {
    super(props);
    this.state = {
      username: '',
      password: '',
    };
  }

  private appName = process.env.APP_NAME;

  componentDidUpdate(prevProps: Props) {
    const { loading } = this.props;
    if (!loading && prevProps.loading) {
      this.$f7.preloader.hide();
      this.$f7.popup.close();
    }
  }

  login() {
    this.$f7.preloader.show();
    this.props.loginWithUserPassword(
      this.state.username,
      this.state.password
    );
  }

  render() {
    const { className, t, ...props } = this.props;

    return (
    <Popup {...props} className={classNames("login-popup", className)}>
      <Page>
        <Navbar
          noShadow
          noHairline
          title={t('Login')}
        >
          <NavRight>
            <Link popupClose>{t('Close')}</Link>
          </NavRight>
        </Navbar>
        <Block>
          <List noHairlines form>
            <ListInput
              name="username"
              label={t("E-mail or phone").toString()}
              floatingLabel
              type="text"
              placeholder=""
              clearButton
              slot="list"
              onInput={(e) => {
                this.setState({username: e.target.value});
              }}
            />
            <ListInput
              name="password"
              label={t("Password").toString()}
              floatingLabel
              type="password"
              placeholder=""
              clearButton
              slot="list"
              onInput={(e) => {
                this.setState({password: e.target.value});
              }}
            />
          </List>
          <Block className="login-form">
            <Button
              fill
              large
              round
              raised
              onClick={this.login.bind(this)}
              disabled={this.props.loading}
            >
              {t("Login")}
            </Button>

            <p className="sign-up-text">
              {t('Not on marketplace yet?', {app: this.appName})} &nbsp;
              <Link popupClose onClick={this.props.onRegister}>{t('Sign Up')}</Link>
            </p>
          </Block>
        </Block>
      </Page>
    </Popup>
    )
  }
}

const mapStateToProps = (state: IApplicationStore) => ({
  profile: getProfile(state),
  loading: state.sessionReducer.loading,
  error: state.sessionReducer.error,
});

const mapDispatchToProps = (dispatch: any) => ({
  loginWithUserPassword: (username: string, password: string) => dispatch(loginWithUserPassword(username, password)),
});

export default compose<any>(
  withTranslation(),
  connect(mapStateToProps, mapDispatchToProps)
)(LoginDesktopPopup);
