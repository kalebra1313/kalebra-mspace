import React, { createRef, PureComponent } from "react";
import { Icon } from "framework7-react";
import { Button } from "../ThemedButton";
import classNames from "classnames";
import { WithTranslation, withTranslation } from "react-i18next";
import { compose } from "redux";
import './index.less';

type Props = WithTranslation &{
  text?: string;
  icon?: string;
  className?: string;
  popoverOpen: string;
  value?: string | number;
  quantity?: number;
  itemType: string;
  onChange: (e: any) => void;
  onClick: () => void;
};

class PopowerButton extends PureComponent<Props> {

  inputRef: any = createRef();

  state: {inputValue: string | number} = {
    inputValue: ''
  }

  handleUpdate = () => {
    const {
      state: {inputValue},
      props: {onChange, quantity, itemType},
    } = this;

    const isInfinity = itemType === 'I';
    const value = (inputValue > quantity && !isInfinity) ? quantity : !inputValue ? 1 : +inputValue;

    this.setState({inputValue: value});
    onChange(value);
  }

  handleChangeValue = (e) => {
    const value = e.target.value;
    const reg = /^[0-9]+$/;

    if((value>0 && value<1000 && reg.test(value)) || value === ''){
      this.setState({ inputValue: e.target.value })
    }
  }

  componentDidUpdate(prevProps){
    const {value} = this.props;

    if(value!==prevProps.value && value === 'input'){
      prevProps.value && this.setState({inputValue: prevProps.value})
      this.inputRef && this.inputRef.current.focus();
    }
  }

  render() {
    const { text, icon, className, value, popoverOpen, onClick, t, ...rest } = this.props;
    const { inputValue } = this.state;
    const popoverType = value === 'input' ? false : popoverOpen;

    return (
      <Button
        {...rest}
        className={classNames("popower-button", !popoverType && 'type-input', className)}
        popoverOpen={popoverType}
        onClick={popoverType && onClick}
      >
        {icon && <Icon className="popower-button-left-icon" material={icon} />}
        {value === 'input' ?
          <>
            <span>{text}</span>
            <input ref={this.inputRef} type="number" value={inputValue} onChange={this.handleChangeValue} />
            <span onClick={this.handleUpdate}>{t('Update')}</span>
          </>
          :
          <>
            <span className="popower-button-text">{`${text} ${value}`}</span>
            <Icon className="popower-button-right-icon" material="expand_more" />
          </>
        }
      </Button>
    );
  }
}

export default compose(withTranslation())(PopowerButton);
