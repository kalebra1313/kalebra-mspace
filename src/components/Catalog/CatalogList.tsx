import React from 'react';
import { WithTranslation, withTranslation } from 'react-i18next';
import { compose } from 'redux';
import { IProduct } from '../../reducers/productReducer';
import { formatDate } from '../../utils';
import Price from '../Price/Price';
import { ViewDetails, WishDetails, ProductStatus } from '../ProductDetails';

type Props = WithTranslation & {
  data: IProduct[],
  simplePrice?: boolean,
  onClick:(uid) =>  void
}

const CatalogList = ({ data, simplePrice, onClick, t }: Props) => {
  return (
    <div className="catalog-list data-table data-table-init card">
      <table>
        <thead className="catalog-list-head">
          <tr>
            {/* <th className="checkbox-cell">
                <label className="checkbox">
                  <input type="checkbox" />
                  <i className="icon-checkbox"></i>
                </label>
              </th> */}
            <th className="label-cell">{t("Product")}</th>
            <th className="label-cell catalog-list-head-price">{t("Cost")}</th>
            <th className="label-cell">{t("Views")}</th>
            <th className="label-cell">{t("Likes")}</th>
            <th className="label-cell">{t("Added")}</th>
            <th className="label-cell">{t("Status")}</th>
          </tr>
        </thead>
        <tbody className="catalog-list-body">
          {data.length && data?.map((item) => (
            <tr className="catalog-list-row" key={item.uid} onClick={()=>onClick(item.uid)}>
              {/* <td className="catalog-list-row-checkbox checkbox-cell">
                  <label className="checkbox">
                    <input type="checkbox" />
                    <i className="icon-checkbox"></i>
                  </label>
                </td> */}
              <td className="catalog-list-row-media label-cell">
                <div className="catalog-list-row-media-image">
                  {item.imageThumbnailUrl1 && <img src={item.imageThumbnailUrl1} alt={item.name} />}
                </div>
                <p className="catalog-list-row-media-name">{item.name}</p>
                <div className="catalog-list-row-mob">
                  <p>{item.name}</p>
                  {item.statusValue && <ProductStatus text={item.statusValue} status={item.status}/>}
                  <Price
                    className={simplePrice ? "simple" : ""}
                    price={item.price}
                    discountedPrice={item.discountedPrice}
                    currencyCode={item.currencyCode}
                  />
                  <div className="catalog-list-row-mob-view-wish">
                    <ViewDetails count={item.viewCount} />
                    <WishDetails count={item.wishCount} />
                  </div>
                </div>
              </td>
              <td className="catalog-list-row-price label-cell">
                <Price
                  className={simplePrice ? "simple" : ""}
                  price={item.price}
                  discountedPrice={item.discountedPrice}
                  currencyCode={item.currencyCode}
                />
              </td>
              <td className="catalog-list-row-views label-cell">
                <ViewDetails count={item.viewCount} />
              </td>
              <td className="catalog-list-row-wish label-cell">
                <WishDetails count={item.wishCount} />
              </td>
              {<td className="catalog-list-row-date label-cell">{item.publishDate && formatDate(item.publishDate.toString())}</td>}
              <td className="catalog-list-row-status label-cell">
                {item.statusValue && <ProductStatus text={item.statusValue} status={item.status}/>}
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  )
}

export default compose(withTranslation())(CatalogList);
