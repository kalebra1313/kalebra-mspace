import React from 'react';
import {
  Link, F7Link
} from 'framework7-react';

import { IcShare } from '../components-ui/icons';
import './ShareButton.less';

export default (props: F7Link.Props) => (
  <Link className="share-button" href="#" iconOnly {...props}>
    {props.icon ? <props.icon/> : <IcShare/>}
  </Link>
)
