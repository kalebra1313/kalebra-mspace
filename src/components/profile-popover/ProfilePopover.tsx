import React from "react";
import {
  Popover,
  F7Popover,
  Block,
  BlockTitle,
  List,
  ListItem,
  Icon,
} from "framework7-react";
import { compose } from "redux";
import { Profile } from "../../reducers/sessionReducer";
import connectLocalConfig from "../../store/connectLocalConfig";
import { ILocalConfig } from "../../store/rootReducer";
import { Avatar } from "../Avatar";
import classNames from "classnames";

import "./style.less";
import { ProfileStatus } from "../ProfileStatus";
import { withTranslation, WithTranslation } from "react-i18next";
import {
  IcTransaction,
  IcOrders,
  IcSellerArea, IcPassport,
} from "../../components-ui/icons";

type Props = F7Popover.Props &
  Pick<WithTranslation, "t"> & {
    profile: Profile;
    localConfig?: ILocalConfig;
    onVerifyClick: any;
    onAboutClick: any;
  };

const LinkItem = ({
  title,
  link,
  icon,
}: {
  title: string;
  link: string;
  icon: JSX.Element;
}) => {
  return (
    <ListItem link={link} title={title} popoverClose>
      {icon}
    </ListItem>
  );
};

class ProfilePopover extends React.Component<Props> {

  menuItemEnabled = (item: string): boolean => {
    const { localConfig } = this.props;
    const profileMenuItems = localConfig.profileMenuItems;
    return profileMenuItems && profileMenuItems.length > 0 && profileMenuItems.includes(item);
  };

  profileVerified = (): boolean => {
    const { profile } = this.props;
    return ["SF", "MF", "BF"].includes(profile.status);
  };

  private appName = process.env.APP_NAME;

  render() {
    const {profile, className, t, onVerifyClick, onAboutClick, ...props} = this.props;
    return (
      <Popover
        {...props}
        className={classNames("profile-popover", className)}
        closeByBackdropClick
        closeByOutsideClick
      >
        <Block className="avatar-container">
          <Avatar profile={profile} />
          <BlockTitle large className="profile-name">
            {profile.person?.name} {profile.person?.surname}
          </BlockTitle>
          {profile && <ProfileStatus profile={profile} />}
        </Block>
        <List noHairlines noHairlinesBetween noChevron>
          <ul>
            {this.menuItemEnabled('SellerArea') && (
              <ListItem
                link="/profile/seller-area/my-goods/"
                title={t("Seller Area").toString()}
                popoverClose
              >
                <div
                  slot="media"
                  className="display-flex justify-content-center align-content-center"
                >
                  <IcSellerArea />
                </div>
              </ListItem>
            )}
            {this.menuItemEnabled('MyWishList') && (
              <LinkItem
                link="/wish-list/"
                title={t("My wish list").toString()}
                icon={<Icon slot="media" material="favorite_border" />}
              />
            )}
            {this.menuItemEnabled('MyWallet') && (
              <LinkItem
                link="/profile/wallet/"
                title={t("My wallet").toString()}
                icon={<Icon slot="media" material="account_balance_wallet" />}
              />
            )}
            {this.menuItemEnabled('MyOrders') && (
              <ListItem
                link="/profile/orders/"
                title={t("My Orders").toString()}
                popoverClose
              >
                <div
                  slot="media"
                  className="display-flex justify-content-center align-content-center"
                >
                  <IcOrders />
                </div>
              </ListItem>
            )}
            {this.menuItemEnabled('Transactions') && (
              <ListItem
                link="/profile/transactions/"
                title={t("Transactions").toString()}
                popoverClose
              >
                <div
                  slot="media"
                  className="display-flex justify-content-center align-content-center"
                >
                  <IcTransaction />
                </div>
              </ListItem>
            )}
            {this.menuItemEnabled('Promotions') && (
              <ListItem
                link="product-promotion/"
                title={t("Product promotion").toString()}
              >
                <Icon slot="media" f7="star" />
              </ListItem>
            )}
          </ul>
        </List>
        <List noHairlines noHairlinesBetween noChevron className="additional-menus">
          <ul>
            {this.menuItemEnabled('Verify') && this.profileVerified() && (
              <ListItem
                link="#"
                title={t("Verify Your Account").toString()}
                popoverClose
                onClick={onVerifyClick}
              >
                <div
                  slot="media"
                  className="display-flex justify-content-center align-content-center"
                >
                  <IcPassport />
                </div>
              </ListItem>
            )}
            {this.menuItemEnabled('About') && (
              <ListItem
                link="#"
                title={t("About app", {appName: this.appName}).toString()}
                popoverClose
                onClick={onAboutClick}
              >
                <Icon slot="media" f7="info_circle" />
              </ListItem>
            )}
          </ul>
        </List>
      </Popover>
    );
  }
}

export default compose<any>(
  withTranslation(),
  connectLocalConfig,
)(ProfilePopover);
