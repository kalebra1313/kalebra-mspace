import React from "react";
import {
  Block, BlockTitle, Button, Link, PageContent,
} from "framework7-react";
import { compose } from "redux";
import { withTranslation, WithTranslation } from "react-i18next";
import { createProductUrl } from "../../utils";

import "./style.less";

type Props = Pick<WithTranslation, "t"> & {
  opened?: boolean;
  productUid?: string;
  onClose?(): void;
  onStartChat?(message: string): void;
}

class StartChat extends React.Component<Props> {

  getMessage = () => {
    const { t, productUid } = this.props;
    if (!productUid) {
      return null;
    }
    const productUrl = createProductUrl(productUid);
    const formatted = `<span>${productUrl}</span>`;
    const message = t("ChatMessageProduct", { productUrl: formatted });
    return message.replaceAll("\n", "<br/>");
  };

  startChatHandle = () => {
    const { t, productUid } = this.props;
    const productUrl = createProductUrl(productUid);
    const message = t("ChatMessageProduct", { productUrl });
    this.props.onStartChat(message);
  };

  closeHandle = () => {
    this.props.onClose();
  };

  render() {
    const { t } = this.props;
    return (
      <PageContent className="start-chat-content">
        <BlockTitle medium>
          <Link
            className="close"
            onClick={this.closeHandle}
            iconMaterial="clear"
          />
          {t('Start Сhat with Seller')}
        </BlockTitle>
        <Block>
          <p>{t("You are going to open chat with Seller with the following message")}</p>
        </Block>
        <Block className="message">
          <div dangerouslySetInnerHTML={{ __html: this.getMessage() }} />
        </Block>
        <Block className="buttons">
          <Button
            fill
            large
            round
            raised
            onClick={this.startChatHandle}
          >
            {t("Start Chat")}
          </Button>
        </Block>
      </PageContent>
    )
  }
}

export default compose(
  withTranslation(),
)(StartChat);
