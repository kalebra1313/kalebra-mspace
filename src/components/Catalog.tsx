import React from 'react';
import {
  List, ListItem
} from 'framework7-react';

import { compose } from 'redux';
import connectCategories from '../store/connectCategories';
import connectFilter from '../store/connectFilter';
import { ICategory } from '../store/rootReducer';
import { IProduct } from '../reducers/productReducer';
import connectCategoriesClassificator from '../store/connectCategoriesClassificator';
import { ICategoryClassificator } from '../reducers/categoryReducer';
import { getSubcategoryNameBySubcategoryId } from '../utils';
import Price from './PriceDetails';
import AddWishButton from './AddWishButton';
import { addToWishList } from '../actions/productActions';
import { connect } from 'react-redux';

import './Catalog.less';

type Props = {
  items?: IProduct[],
  categories?: ICategory[],
  chosenCategoryId?: string,
  chosenSubcategoryId?: string,
  categoriesClassificator?: ICategoryClassificator[],
  addToWish?(uid?: string): void,
}

const getItemBackgroundStyle = (url: string) => url ? { backgroundImage: `url(${url})` } : null

const Catalog = ({ items, categoriesClassificator, addToWish }: Props) => (
  <List className="catalog" mediaList noChevron noHairlines>
    {items.map((item, index) => (
      <ListItem
        key={index}
        link={`/product-details/${item.uid}/`}
        title={item.name}
        subtitle={getSubcategoryNameBySubcategoryId(item.category, categoriesClassificator)}
        text={item.description}
        noChevron
      >
        <div slot="inner-end">
          <Price
            price={item.price}
            priceDiscount={item.discountedPrice}
            currencyCode={item.currencyCode}
          />
        </div>
        <div slot="media" className="image" style={getItemBackgroundStyle(item.imageThumbnailUrl1)}>
          <AddWishButton
            onClick={() => addToWish(item.uid)}
            withShadow
            active={item.wish}
          />
        </div>
      </ListItem>
    ))}
  </List>
)

const mapDispatchToProps = (dispatch: any) => ({
  addToWish: (uid?: string) => dispatch(addToWishList(uid))
})

export default compose<any>(
  connectFilter,
  connectCategories,
  connectCategoriesClassificator,
  connect(
    null,
    mapDispatchToProps
  )
)(Catalog)