import React, { PureComponent } from "react";
import { Icon, List, ListItem, Popover } from "framework7-react";
import classNames from "classnames";

import './index.less';

type Props = {
  text?: string;
  className?: string;
  popoverIsOpen: boolean;
  popoverQty?: number;
  selectedValue?: string | number;
  itemType?: string;
  onChange: (e: any) => void;
  onClick: () => void;
};

class PopoverList extends PureComponent<Props> {
  render() {
    const { text, className, popoverIsOpen, popoverQty, selectedValue, onChange, onClick, itemType } = this.props;
    const popoverQtyData = [];

    if(popoverQty){
      const isShowPlus = itemType === 'P' || itemType === 'I';

      for(let i=0; i<popoverQty; i++){
        if(popoverQty>10 && i===9){
          popoverQtyData.push({
            value: isShowPlus ? 'input' : i+1,
            title: `${i+1}${isShowPlus ? '+' : ''}`
          });
          break;
        }
        popoverQtyData.push({
          value: i+1,
          title: i+1
        });
      }
    }

    return (
      <Popover className={classNames("popover-list", className)} opened={popoverIsOpen}>
        <List>
          {text && <p className="popover-list-title">{text}</p>}
          {popoverQtyData.map((item, index) => (
            <ListItem title={item.title} key={index} onClick={() => {
              onChange(item.value);
              onClick();
            }}>
              {item.value === selectedValue && <Icon material="done" className="selected-icon"/>}
            </ListItem>
          ))}
        </List>
    </Popover>
    );
  }
}

export default PopoverList;
