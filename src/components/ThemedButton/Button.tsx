import React from 'react';
import { Button, F7Button } from 'framework7-react';

import './style.less';

export default (props: F7Button.Props & { children: any }) => (
  <Button {...props} />
)