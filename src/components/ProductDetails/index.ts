import DescriptionDetails from './DescriptionDetails';
import TechnicalDetails from './TechnicalDetails';
import CustomDetails from './CustomDetails';
import CategoryDetails from './CategoryDetails';
import ProductPostedBlock from './ProductPostedBlock';
import ViewDetails from './ViewDetails';
import WishDetails from './WishDetails';
import ProductStatusNotification from './ProductStatusNotification';
import ProductStatus from './ProductStatus';

export {
  TechnicalDetails, CustomDetails, DescriptionDetails, CategoryDetails,
  ProductPostedBlock, ViewDetails, WishDetails, ProductStatusNotification, ProductStatus
}