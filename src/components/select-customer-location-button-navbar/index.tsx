import React, { PureComponent } from "react";
import "./style.less";
import { Link, Icon } from "framework7-react";
import classNames from "classnames";
import locationIcon from '../../components-ui/icons/location.svg';

type Props = Link.Props & {
  locationText?: string;
};

class SelectCustomerLocationButtonNavbar extends PureComponent<Props> {
  render() {
    const { className, text, locationText, ...rest } = this.props;
    return (
      <Link
        className={classNames(
          "no-ripple",
          "select-customer-location-button-navbar",
          'header-navbar-location',
          className
        )}
        {...rest}
      >
        <img src={locationIcon} alt="location"/>
        <p className="header-navbar-location-info">
          <p>{text}</p>
          <span>{locationText}</span> 
        </p>
      </Link>
    );
  }
}

export default SelectCustomerLocationButtonNavbar;
