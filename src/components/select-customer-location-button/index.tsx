import React from "react";
import { F7ListItem, List, ListItem } from "framework7-react";
import { IcLocation } from "../../components-ui/icons";
import "./style.less";

const SelectCustomerLocationButton = (props: F7ListItem.Props) => (
  <List
    className="select-customer-location-button"
    noHairlines
    noHairlinesBetween
  >
    <ListItem link {...props}>
      {
        // @ts-ignore
        <IcLocation slot="media" />
      }
    </ListItem>
  </List>
);

export default SelectCustomerLocationButton;
