import {
  F7Popup,
  Icon,
  Link,
  List,
  ListItem,
  Navbar,
  NavLeft,
  NavRight,
  NavTitle,
  PageContent,
  Popup,
  Searchbar,
} from "framework7-react";
import React from "react";
import { withTranslation, WithTranslation } from "react-i18next";
import { connect } from "react-redux";
import { compose } from "redux";
import { IApplicationStore } from "../../store/rootReducer";
import { Country } from "../../types/commonapi";
import Flag from "react-world-flags";
import "./style.less";

type Props = F7Popup.Props &
  Pick<WithTranslation, "t"> & {
    countries: Country[];
    onCountrySelect: any;
  };

class CountrySelectPopup extends React.Component<Props> {
  render() {
    const { countries, onCountrySelect, t, ...props } = this.props;

    return (
      <Popup
        id="choose_country__popup"
        className="choose-country__popup"
        {...props}
      >
        <Navbar backLink={false} noHairline noShadow sliding>
          <NavLeft>
            <Link iconOnly onClick={() => this.$f7.popup.close()}>
              <Icon className="icon-back" />
            </Link>
          </NavLeft>
          <NavTitle>{t("Choose country")}</NavTitle>
          <NavRight>
            <Link
              searchbarEnable=".searchbar-demo-1"
              iconIos="f7:search"
              iconAurora="f7:search"
              iconMd="material:search"
            />
          </NavRight>
          <Searchbar
            className="searchbar-demo-1"
            expandable
            searchContainer=".search-list"
            searchIn=".item-title"
            disableButton={!this.$theme.aurora}
          />
        </Navbar>
        <PageContent style={{ paddingTop: 0 }}>
          <List
            className="searchbar-not-found"
            noChevron
            noHairlines
            noHairlinesBetween
          >
            <ListItem title={t("Nothing found").toString()} />
          </List>
          {/* TODO replace with VirtualList! */}
          <List
            className="search-list searchbar-found"
            noChevron
            noHairlines
            noHairlinesBetween
          >
            {countries.map((val) => (
              <ListItem
                link
                key={val.code}
                title={val.name}
                onClick={() => onCountrySelect(val)}
                popupClose
              >
                <div slot="media">
                  {" "}
                  <Flag code={val.code} height="16" />
                </div>
              </ListItem>
            ))}
          </List>
        </PageContent>
      </Popup>
    );
  }
}

const mapStateToProps = (state: IApplicationStore) => ({
  countries: state.classificatorReducer.countryClassificator,
});

export default compose<any>(
  withTranslation(),
  connect(mapStateToProps, null)
)(CountrySelectPopup);
