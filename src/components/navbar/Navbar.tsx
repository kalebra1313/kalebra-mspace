import React, { Component } from "react";
import {
  F7Navbar,
  Icon,
  Link,
  NavLeft,
  NavRight,
  Subnavbar,
} from "framework7-react";
import ToMessengerButton from "../to-messenger-button/ToMessengerButton";
import { withTranslation, WithTranslation } from "react-i18next";
import { bindActionCreators, compose } from "redux";
import { Logo } from "../Logo";
import { OpenCategoriesButton } from "../open-categories-button";
import { SearchBar } from "../search-bar";
import { ConnectedCartLink } from "../cart-link";
import MyWishButton from "../MyWishButton";
import { LanguageLink } from "../LanguageLink";
import { ProfileLink } from "../ProfileLink";
import { Profile } from "../../reducers/sessionReducer";
import { connect } from "react-redux";
import { IApplicationStore, ResizeEvent } from "../../store/rootReducer";
import "./styles.less";
import Dom7 from "dom7";
import { createUUID } from "../../utils";
import { searchProducts } from "../../actions/productActions";
import { toggleSelectCustomerLocationSheet } from "../../actions/customer-location/customerLocationActions";
import debounce from "lodash/debounce";
import { IProduct } from "../../reducers/productReducer";
import SelectCustomerLocationButton from "../select-customer-location-button";
import { getProfile } from "../../selectors/profile";
import { Country } from "../../types/commonapi";
import SelectCustomerLocationButtonNavbar from "../select-customer-location-button-navbar";
import { IcShareLarge } from "../../components-ui/icons";
import ShareButton from "../ShareButton";
import { analytics } from "../../Setup";
import connectShare, { IShareProps } from "../../store/connectShare";
import { getProductDetailsLink } from "../../actions/shareActions";


type Props = WithTranslation & IShareProps & {
  uid?: string;
  showMessengerButton?: boolean;
  onClickGoToMessenger?(): void;
  onClickLogoLink?(): void;
  onClickOpenCategoriesMenu?(): void;
  openCategoriesMenuButton?: boolean;
  onSearchbarEnable?(): void;
  onSearchbarDisable?(): void;
  onSearchInputChange?(val: string, autocomplete?: boolean): void;
  onSearchClickClear?(): void;
  showSearchbar?: boolean;
  showCartLink?: boolean;
  language?: string;
  showLanguageLink?: boolean;
  onClickLanguageLink?(): void;
  profile?: Profile;
  onClickProfileLink?(): void;
  showProfileLink?: boolean;
  findedProducts?: IProduct[];
  findProductLoading?: boolean;
  onFindedProductItemClick?(uid: string): void;
  resizeEvent?: ResizeEvent;
  onReportAdvertisementClick?(): void;
  toggleSelectCustomerLocationSheet?(open: boolean): void;
  country?: Country;
  showShare?: boolean;
} & F7Navbar.Props;

type State = {
  searchBarFocus: boolean
}

class Navbar extends Component<Props, State> {
  private _searchbarId: string;
  private _searchbar: any;

  constructor(props: any) {
    super(props);
    this._searchbarId = "_" + createUUID().replace(/-/g, "_");
    this.state = {
      searchBarFocus: false
    }
  }

  isMdRes: boolean = this.props.resizeEvent.width > 768 && this.props.resizeEvent.width <= 1024;
  isSmRes: boolean = this.props.resizeEvent.width > 567 && this.props.resizeEvent.width <= 850;;

  componentDidUpdate(prevProps) {
    const { openCategoriesMenuButton, resizeEvent } = this.props;
    this.initSearhbar();
    this.isMdRes = resizeEvent.width > 768 && resizeEvent.width <= 1024;
    this.isSmRes = resizeEvent.width > 567 && resizeEvent.width <= 850;

    if (openCategoriesMenuButton !== prevProps.openCategoriesMenuButton && this.isMdRes) {
      const navBars = this.$f7.$('.navbars .navbar-main');

      openCategoriesMenuButton ? navBars.addClass('category-opened') : navBars.removeClass('category-opened');
    }
  }

  componentWillUnmount() {
    this.$f7.off("click", this.onClickOutsideSearchbarHandler);
  }

  initSearhbar() {
    if (!this._searchbar) {
      const el = Dom7(`#${this._searchbarId}`)[0] as any;
      if (el) {
        this._searchbar = this.$f7.searchbar.create({
          el: Dom7(`#${this._searchbarId}`)[0] as any,
        });
        this.initSearchbarDisableBehavior();
      }
    }
  }

  initSearchbarDisableBehavior() {
    this.$f7.on("click", this.onClickOutsideSearchbarHandler);
  }

  onClickOutsideSearchbarHandler = (ev: any) => {
    const selector = `#${this._searchbarId}`;
    if (this._searchbar) {
      if (!Dom7(ev.target).closest(selector).length && !this._searchbar.value) {
        this._searchbar.disable();
      }
    }
  };

  renderProfileLink() {
    const {
      onClickProfileLink,
      profile,
      showProfileLink,
      resizeEvent,
    } = this.props;
    const props = { profile };
    if (resizeEvent && resizeEvent.width > 567) {
      props["href"] = "#";
      props["onClick"] = onClickProfileLink;
    }
    return showProfileLink && <ProfileLink key="profile_link" {...props} />;
  }

  renderLanguageLink() {
    const {
      showLanguageLink,
      onClickLanguageLink,
      language,
      resizeEvent,
      t,
    } = this.props;
    return (
      resizeEvent &&
      resizeEvent.width > 567 &&
      showLanguageLink && (
        <LanguageLink onClick={onClickLanguageLink} language={language} t={t} />
      )
    );
  }

  onSearchInputChange = () => {
    if (!this._searchbar.value) return;
    this.delayedQuery(this._searchbar.value);
  };

  delayedQuery = debounce(
    (q) =>
      this.props.onSearchInputChange(q, this.props.resizeEvent.width > 567),
    500
  );

  handleShareProduct = () => {
    const { item, profile } = this.props;
    analytics.shareProduct(profile, item);
    this.props.share(item.name, getProductDetailsLink(item.uid));
  };

  componentDidMount() {
    this.$f7ready((f7) => {
      const pageContent = f7.$('.page-content');

      let prevScrollpos = pageContent.scrollTop;

      pageContent.on('scroll', (e: any) => {
        if (this.props.resizeEvent.width > 1024) {
          return
        }

        const currentScrollPos: any = e.srcElement.scrollTop;

        if (prevScrollpos > currentScrollPos) {
          f7.$('.navbar-main').addClass('show');
          f7.$('.navbar-main').removeClass('hide');
        } else {
          f7.$('.navbar-main').addClass('hide');
          f7.$('.navbar-main').removeClass('show');
        }

        prevScrollpos = currentScrollPos;
      })
    })
  }

  render() {
    const {
      showMessengerButton,
      onClickGoToMessenger,
      onClickLogoLink,
      onClickOpenCategoriesMenu,
      openCategoriesMenuButton,
      onSearchClickClear,
      onSearchInputChange,
      onSearchbarDisable,
      onSearchbarEnable,
      showCartLink,
      showSearchbar,
      findedProducts,
      findProductLoading,
      onFindedProductItemClick,
      resizeEvent,
      onReportAdvertisementClick,
      toggleSelectCustomerLocationSheet,
      t,
      country,
      showShare,
      ...rest
    } = this.props;
    const { searchBarFocus } = this.state;
    const { isMdRes, isSmRes } = this;

    return (
      <>
        {isMdRes &&
          <div className={`navbar-container-delivery-top`}>
            <SelectCustomerLocationButtonNavbar
              text={t("Deliver to").toString()}
              locationText={country.name}
              onClick={() => toggleSelectCustomerLocationSheet(true)}
            />
          </div>}
        <F7Navbar
          sliding
          noShadow
          noHairline
          className={`
            ${openCategoriesMenuButton ? 'category-opened' : ''} 
            ${isMdRes ? 'delivery-is-show' : 'delivery-is-hide'} navbar-main navbar-shadow
            ${searchBarFocus ? 'search-bar-focused' : ''}
          `}
          {...rest}
        >
          <NavLeft>
            {showMessengerButton && (
              <ToMessengerButton
                text={t("to Messenger")}
                className="pure-hidden-xs"
                onClick={onClickGoToMessenger}
              />
            )}

            <Link
              className="pure-hidden-xs navbar-logo-content"
              style={{ flex: "0 0 auto" }}
              onClick={onClickLogoLink}
            >
              <Logo full />
            </Link>
            <div className="vertical-line"></div>
          </NavLeft>
          <div className="navbar-center">
            <div>
              <OpenCategoriesButton
                className="pure-hidden-xs"
                text={t("Categories")}
                onClick={onClickOpenCategoriesMenu}
                opened={openCategoriesMenuButton}
              />
            </div>
            <div className="search-container">
              {showSearchbar && (
                <SearchBar
                  id={this._searchbarId}
                  noShadow
                  noHairline
                  backdrop
                  customSearch
                  onSearchbarEnable={() => {
                    onSearchbarEnable();
                    this.setState({ searchBarFocus: true })
                  }}
                  onSearchbarDisable={() => {
                    onSearchbarDisable();
                    this.setState({ searchBarFocus: false })
                  }}
                  onChange={this.onSearchInputChange}
                  onClickClear={onSearchClickClear}
                  findedProducts={findedProducts}
                  onFindedProductItemClick={(uid) => {
                    this._searchbar.disable();
                    onFindedProductItemClick(uid);
                  }}
                  autocomplete={
                    (findProductLoading ||
                      (findedProducts && findedProducts.length > 0)) &&
                    this._searchbar &&
                    this._searchbar.enabled &&
                    resizeEvent.width > 567
                  }
                  preloader={findProductLoading}
                  isIconShow={isSmRes}
                  searchBarFocus={searchBarFocus}
                />
              )}
            </div>
            {resizeEvent.width > 1024 &&
              <div className="navbar-container-location-web">
                <SelectCustomerLocationButtonNavbar
                  text={t("Deliver to").toString()}
                  locationText={country.name}
                  onClick={() => toggleSelectCustomerLocationSheet(true)}
                />
              </div>
            }
          </div>
          <NavRight>
            {(searchBarFocus && isSmRes) ?
              <span
                className="search-bar-cancel"
                onClick={() => {
                  onSearchbarDisable();
                  this.setState({ searchBarFocus: false })
                }}
              >
                {t("Cancel")}
              </span>
              :
              <>
                {this.renderLanguageLink()}
                {resizeEvent.width < 567 && showShare && (
                  <div className="nav-right-card-share">
                    <ShareButton
                      onClick={this.handleShareProduct}
                      icon={IcShareLarge}
                    />
                  </div>
                )}
                <div className="nav-right-card-wish">
                  {resizeEvent.width > 1024 ?
                    <>
                      <MyWishButton />
                      <ConnectedCartLink />
                    </>
                    : showCartLink ? <ConnectedCartLink /> : <MyWishButton />
                  }
                </div>
                {this.renderProfileLink()}
              </>
            }
          </NavRight>
          {
            country && resizeEvent.width <= 768 && (
              <Subnavbar className="no-shadow no-hairline">
                <SelectCustomerLocationButton
                  title={t("Deliver to {{country}}", {
                    country: country.name,
                  }).toString()}
                  onClick={() => toggleSelectCustomerLocationSheet(true)}
                />
              </Subnavbar>
            )
          }
        </F7Navbar >
      </>
    );
  }
}

const mapStateToProps = (state: IApplicationStore) => ({
  resizeEvent: state.rootReducer.resizeEvent,
  /* TODO: for localization or move to actions? */
  country:
    state.customerLocationReducer.country || getProfile({ ...state }).country,
});

const mapDispatchToProps = (dispatch: any) =>
  bindActionCreators(
    {
      toggleSelectCustomerLocationSheet,
    },
    dispatch
  );

const mapStateToPropsDependsOfRoute = (
  state: IApplicationStore,
  ownProps: Props & WithTranslation
) => {
  const { profile: { uid } } = ownProps;
  const {
    rootReducer: { resizeEvent },
    productReducer: { productDetails },
  } = state;
  const {
    cartReducer: { items },
  } = state;

  const isMobile = resizeEvent.width < 567;
  const isCartHasItems = items.length > 0;

  const item = state.productReducer.products.filter(
    (item) => item.uid === uid
  )[0];

  if (isMobile) {
  } else {
    ownProps.showSearchbar = true;
  }

  if (isCartHasItems) {
    ownProps.showCartLink = true;
  }

  return {
    ...ownProps,
    item: productDetails,
  };
};

const dependsOfRoutesConnector = connect(mapStateToPropsDependsOfRoute);
const mapDispatchToPropsSearchBehaviorConnector = (dispatch: any) =>
  bindActionCreators(
    {
      onSearchInputChange: (val: string, autocomplete = false) => {
        return searchProducts(
          {
            name: val,
          },
          autocomplete
        );
      },
    },
    dispatch
  );

export default compose<React.FC<Props>>(
  withTranslation(),
  connectShare,
  connect(mapStateToProps, mapDispatchToProps),
  connect(null, mapDispatchToPropsSearchBehaviorConnector),
  dependsOfRoutesConnector,
)(Navbar);
